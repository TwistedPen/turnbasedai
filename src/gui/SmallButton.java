package gui;

import gui.sprites.Art;
import gui.sprites.Screen;
import client.MouseButtons;

public class SmallButton extends Button {

	private int h;
	private int w;

	public SmallButton(ButtonID Id, int buttonImageIndex, int x, int y) {
		super(Id, buttonImageIndex, x, y);
		
		this.w = 24;
        this.h = 24;
	}
	
	 @Override
	    public void update(MouseButtons mouseButtons) {

	        int mx = mouseButtons.getX() / Gui.SCALE;
	        int my = mouseButtons.getY() / Gui.SCALE;
	        isPressed = false;
	        if (mx >= x && my >= y && mx < (x + w) && my < (y + h)) {
	            if (mouseButtons.isRelased(1)) {
	                postClick();
	            } else if (mouseButtons.isDown(1)) {
	                isPressed = true;
	            }
	        }

	        if (performClick) {
	            if (listeners != null) {
	                for (ButtonListener listener : listeners) {
	                    listener.buttonPressed(this);
	                }
	            }
	            performClick = false;
	        }
	    }
	 
	  @Override
	    public void render(Screen screen) {

	        if (isPressed) {
	            screen.blit(Art.small_buttons[ix][iy * 2 + 1], x, y);
	        } else {
	            screen.blit(Art.small_buttons[ix][iy * 2 + 0], x, y);
	        }
	    }
	
}
