package message.data;

import java.io.Serializable;

public class PlanetUpdateData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6057494652564276453L;
	
	private int location;
	private int colonizedBy;
	
	public PlanetUpdateData(int location, int colonizedBy) {
		this.location = location;
		this.colonizedBy = colonizedBy;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public int getColonizedBy() {
		return colonizedBy;
	}

	public void setColonizedBy(int colonizedBy) {
		this.colonizedBy = colonizedBy;
	}

	@Override
	public String toString() {
		return "PlanetUpdateData [location=" + location + ", colonizedBy="
				+ colonizedBy + "]";
	}
}
