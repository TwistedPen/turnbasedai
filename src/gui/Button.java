package gui;
import gui.sprites.Art;
import gui.sprites.Screen;

import java.util.ArrayList;
import java.util.List;

import client.MouseButtons;

public class Button extends GuiComponent {

    protected List<ButtonListener> listeners;

    protected boolean isPressed;

    protected final int x;
    protected final int y;
    private final int w;
    private final int h;

    protected final ButtonID id;

    protected int ix;

    protected int iy;
    protected boolean performClick = false;

    public Button(ButtonID Id, int buttonImageIndex, int x, int y) {
        this.id = Id;
        this.x = x;
        this.y = y;
        this.w = 128;
        this.h = 24;
        this.ix = buttonImageIndex % 2;
        this.iy = buttonImageIndex / 2;
    }

    @Override
    public void update(MouseButtons mouseButtons) {
        super.update(mouseButtons);

        int mx = mouseButtons.getX() / Gui.SCALE;
        int my = mouseButtons.getY() / Gui.SCALE;
        isPressed = false;
        if (mx >= x && my >= y && mx < (x + w) && my < (y + h)) {
            if (mouseButtons.isRelased(1)) {
                postClick();
            } else if (mouseButtons.isDown(1)) {
                isPressed = true;
            }
        }

        if (performClick) {
            if (listeners != null) {
                for (ButtonListener listener : listeners) {
                    listener.buttonPressed(this);
                }
            }
            performClick = false;
        }
    }

    public void postClick() {
        performClick = true;
    }

    @Override
    public void render(Screen screen) {

        if (isPressed) {
            screen.blit(Art.buttons[ix][iy * 2 + 1], x, y);
        } else {
            screen.blit(Art.buttons[ix][iy * 2 + 0], x, y);
        }
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void addListener(ButtonListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<ButtonListener>();
        }
        listeners.add(listener);
    }
    
    public boolean removeListener(ButtonListener listener) {
        if (listeners != null) {
           return listeners.remove(listener);
        }
        return false;
    }

    public ButtonID getId() {
        return id;
    }
    
}

