package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;

public class ScoreScreen extends GuiMenu {

    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
	public static String text = "";
	private int[] playerArray;
	private int[] scoreArray;
	private Button leave;
	private int playerId;
	private int turn;
	int leftBP = screenW - 128-64;
	
    public ScoreScreen(int[] playerArray, int[] scoreArray,int playerID, int turnNumber) {
        super();
        
        this.playerArray = playerArray;
        this.scoreArray = scoreArray;
        this.playerId = playerID;
        this.turn = turnNumber;
        leave = addButton(new Button(ButtonID.LEAVE_GAME_SCORE_ID, 6, leftBP, 80));
        
    }
    
    @Override
    public void render(Screen screen) {

        screen.clear(0);
        
        screen.fill(54, 80, leftBP-100, screenH - 184, 0xff808080);
        
        
        int startList = 95;
        
        Font.draw(screen, "Game ended after " + turn + " turns" , 64, 60);
        Font.draw(screen, "Player:", 64, startList);
        Font.draw(screen, "Score", 64+150, startList);
        
        for(int i = 0; i < playerArray.length; i++){
            if(playerArray[i] ==  playerId)
                Font.draw(screen, playerArray[i] + " (you)", 64, startList + ((i+1)*20));
            else 
                Font.draw(screen, playerArray[i] + "", 64, startList + ((i+1)*20));
		}
        
        for(int i = 0; i < scoreArray.length; i++){
			Font.draw(screen, "" + scoreArray[i], 64+150, startList + ((i+1)*20));
		}
        
        super.render(screen);
    }
	
	@Override
	public void keyPressed(KeyEvent e) {
	    if (e.getKeyChar() == KeyEvent.VK_ENTER) {
            leave.postClick();
        }

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}
