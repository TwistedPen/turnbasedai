package gui;

import gui.sprites.Art;
import gui.sprites.Screen;

import java.util.ArrayList;
import java.util.List;

import client.MouseButtons;

public class DropDown extends GuiComponent implements DDItemListener{
    
    protected List<DropDownListener> listeners;
    protected List<DDItem> ddItems = new ArrayList<DDItem>();

    protected boolean isPressed;
    
    protected final int x;
    protected final int y;
    private final int w;
    private final int h;
    private int itemH;

    
    private int index = 0;
    private String chosenItem;
    
    protected boolean isDroppedDown = false;
    
    protected boolean performClick = false;
    
    public DropDown(int x, int y, String[] list) {
        this.x = x;
        this.y = y;
        this.w = 64;
        this.h = 12;
        
        chosenItem = list[0];
        
        for(int i = 0; i < list.length ; i++){
           addItem(new DDItem(list[i], x,y+ h + i*h, this,i));  
        }
        
        this.itemH = ddItems.size();
        
    }
    
    public void addItem(DDItem item){
        item.addListener(this);
        ddItems.add(item);
    }
    
    public void postClick() {
        performClick = true;
    }
    
    public void setIsDroppedDown(boolean isDD){
        this.isDroppedDown = isDD;
    }
    
    @Override
    public void update(MouseButtons mouseButtons) {
        super.update(mouseButtons);
     
        int mx = mouseButtons.getX() / Gui.SCALE;
        int my = mouseButtons.getY() / Gui.SCALE;
        isPressed = false;
        
        if (mx >= x && my >= y && mx < (x + w) && my < (y + h)) {
            if ( !isDroppedDown){
                isDroppedDown = true;
            }
        }else if(mx >= x && my >= y && mx < (x + w) && my < (y + h*(itemH+1)) && isDroppedDown){
            
        }else{
            if (isDroppedDown){
                isDroppedDown = false;
            }
            
        }
            
        
        if (performClick) {
            if (listeners != null) {
                for (DropDownListener listener : listeners) {
                    listener.drop(this);
                }
            }
            performClick = false;
        }
        
        for (DDItem ddi : ddItems) {
            ddi.update(mouseButtons);
        }
        
    }
    
    @Override
    public void render(Screen screen) {
        
        if(isDroppedDown){
            screen.blit(Art.DD_bg[0][0], x, y);
            Font.draw(screen, ddItems.get(index).getItem(), x+2, y+2);
            for(DDItem item : ddItems){
                item.render(screen);
            }
            
        } else{
            screen.blit(Art.DD_bg[0][0], x, y);
            Font.draw(screen, ddItems.get(index).getItem(), x+2, y+2);
        }
           
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void addListener(DropDownListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<DropDownListener>();
        }
        listeners.add(listener);
    }
    
    
    public String getChosenItem() {
        return chosenItem;
    }

    public void itemSelected(DDItem ddItem) {
        
        this.index = ddItem.getIndex();
        chosenItem = ddItem.getItem();
        
    }
    
}
