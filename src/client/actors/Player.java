package client.actors;

import message.Constant;
import gui.Gui;
import gui.sprites.Art;
import gui.sprites.Screen;
import cell.Cell;
import cell.PlanetCell;
import cell.WormHoleCell;

public class Player extends Actor{

	private int actorID = Constant.PLAYER_ACTIVE.getId();
	public int tProductionR = 0;
	public int ships = 0;
	public int nPlanets = 0;
	
	public PlanetCell[] planets;
	
	public Gui gui;

	public Player(Gui g) {
	    super();
		this.gui = g;
	}
	
	@Override
    public int getID() {
        
        return actorID;
    }

	public void addPlanet(PlanetCell p) {
		planets[nPlanets] = p;
		nPlanets++;
	}

	public void render(Screen screen) {
	    
		if (selectedCell != null && selectedCell.getPlayerID() == this.getPlayerID() && !gui.choosePlanet) {	
			
		    screen.colorBlit(Art.cellTile[0][0], selectedCell.getXpos(), selectedCell.getYpos(), 0xf0000fff);
			
			for(Cell c :selectedCell.getNeighbours()){
			    
				screen.colorBlit(Art.cellTile[0][0], c.getXpos(), c.getYpos(), 0xfffff000);
			
			}
			
			if(selectedCell.getID() == 2){
				WormHoleCell whc = (WormHoleCell) selectedCell;
				screen.colorBlit(Art.cellTile[0][0], whc.getConnectedTo().getXpos(), whc.getConnectedTo().getYpos(), 0xfffff000);
				
			}
			
		} else if(selectedCell != null && gui.choosePlanet){
		    screen.colorBlit(Art.cellTile[0][0], selectedCell.getXpos(), selectedCell.getYpos(), 0xf0000fff);
		}

	}

}
