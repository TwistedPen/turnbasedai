package message;

import java.io.Serializable;

public class MessagePlayer extends MessageGame implements Serializable {
	
	/**
     * 
     */
    private static final long serialVersionUID = -3566901494513336787L;
    private int playerId;

	public MessagePlayer(int messageId, int clientId, int gameId, int playerId) {
		super(messageId, clientId, gameId);
		this.playerId = playerId;
	}

	public int getPlayerId() {
		return playerId;
	}

    public boolean verify(int clientId) {
        return super.verify(clientId);
    }
    
    public boolean verify(int clientId, int gameId) {
        return super.verify(clientId, gameId);
    }
    
    public boolean verify(int clientId, int gameId, int playerId) {
       return (verify(clientId, gameId) && (playerId == this.playerId));
    }
    
	@Override
	public String toString() {
		return super.toString() + "\n" + "[playerId=" + playerId + "]";
	}
}
