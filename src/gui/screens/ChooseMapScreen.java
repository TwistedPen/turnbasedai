package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import message.data.GameData;


public class ChooseMapScreen extends GuiMenu {
    
    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
	private Button ChooseMapButton;
	public static String mapId = "";
	public ArrayList<GameData> mapList = new ArrayList<GameData>();
	
	int leftBP = screenW - 128-64;
	
	public ChooseMapScreen() {
		super();
		mapId = "";
		ChooseMapButton = addButton(new Button(ButtonID.PERFORM_CHOOSE_MAP_ID, 3, leftBP, 80));
		addButton(new Button(ButtonID.CANCEL_ID, 4, leftBP, screenH - 64 - 64));
        
	}

	@Override
	public void render(Screen screen) {
		
		screen.clear(0);
		
		screen.fill(54, 80, leftBP-100, screenH - 184, 0xff808080);
		
		Font.draw(screen, "Maps", 64, 60);
		
		if(mapList.size() == 0){
		    
			Font.draw(screen, "No maps present", 64, 90);
			
		} else {
			
			Font.draw(screen, "Enter map ID to join:", 64, screenH-64-64);
			Font.draw(screen, mapId + "-",64 + Font.getStringWidth("Enter map ID to join:") + 8, screenH-64-64);
			
			for(int i = 0; i < mapList.size(); i++){
				Font.draw(screen, i + " - height: " + mapList.get(i).getMapHeight() + 
				        " width: " + mapList.get(i).getMapWidth() +
				        " Amount of planets:  " + (mapList.get(i).getHomePlanetLocations().length + mapList.get(i).getPlanets().length) + 
				        " Amount of wormholes: " + mapList.get(i).getWormHoles().length
				        , 64, 90 + (i*20));
			}
		}
		super.render(screen);
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER && mapId.length() > 0) {
			ChooseMapButton.postClick();
		}
	}

	public void keyReleased(KeyEvent arg0) {
	}

	public void keyTyped(KeyEvent e) {

		if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE && mapId.length() > 0) {
			mapId = mapId.substring(0, mapId.length() - 1);
		} else if (Font.letters.indexOf(Character.toUpperCase(e.getKeyChar())) >= 0) {
			mapId += e.getKeyChar();
		}
	}

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}
