package client.actors.AIFunctions;

import client.Map;
import client.actors.AI;
import cell.PlanetCell;

public class Conquest extends Quest{

    private PlanetCell goal = null;
    
    public Conquest(PlanetCell pc, int playerID){
        this.goal = pc;
        this.playerID = playerID;
        
        this.requiredShips = goal.getRequirement();
        
    }
    
    public PlanetCell getGoal() {
        
        return goal;
    }
    
    @Override
    public void update(Map map){
        super.update(map);
        
        goal = (PlanetCell) map.getCell(goal.getX(),goal.getY());
        
        if(goal.isClaimed()){
            if(AI.DEBUG)
                System.out.println("mission done");
            if(goal.getClaimedBy() == playerID)
                succes = true;
         failure = true;   
        }
        
        if(failure || succes){
            for(Fleet f : fleets){
                if(f != null);
                f.assignQuest(null);
            }
            fleets.clear();
        }
        
    }

    @Override
    public int compareTo(Object o) {
        
        if(o instanceof Conquest){
            if(this.priorities < ((Conquest) o).priorities)
                return -1;
            else if(this.priorities > ((Conquest) o).priorities)
                return 1;
        }
        
        return 0;
    }

    
    
}
