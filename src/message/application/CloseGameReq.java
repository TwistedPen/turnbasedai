package message.application;

import java.io.Serializable;

import message.Constant;
import message.MessageGame;

public class CloseGameReq extends MessageGame implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 554959433906902247L;

	public CloseGameReq(int messageId, int clientId, int gameId) {
		super(Constant.REQ_CLOSE_GAME.getId(), clientId, gameId);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
}
