package server.map;

import message.data.GameData;

public class Map {
	
	private GameData mapData;

	public Map(GameData mapData) {
		this.mapData = mapData;
	}

	public GameData getMapData() {
		return mapData;
	}

	public void setMap(GameData mapData) {
		this.mapData = mapData;
	}
	
	public int getPlayerCount() {
		return this.getMapData().getPlayerCount();
	}
}
