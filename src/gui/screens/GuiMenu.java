package gui.screens;

import gui.Button;
import gui.ButtonListener;
import gui.DropDown;
import gui.DropDownListener;
import gui.GuiComponent;
import gui.sprites.Screen;

import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;


import client.MouseButtons;

public abstract class GuiMenu extends GuiComponent implements ButtonListener, KeyListener, DropDownListener {

    public List<Button> buttons = new ArrayList<Button>();
    public List<DropDown> DD = new ArrayList<DropDown>();

    public Button addButton(Button button) {
        buttons.add(button);
        button.addListener(this);
        return button;
    }
    
    public DropDown addDropDown(DropDown dd) {
        DD.add(dd);
        dd.addListener(this);
        return dd;
    }

    @Override
    public void render(Screen screen) {
        super.render(screen);

        for (Button button : buttons) {
            button.render(screen);
        }
        
        for (DropDown dd : DD) {
            dd.render(screen);
        }
    }

    @Override
    public void update(MouseButtons mouseButtons) {
        super.update(mouseButtons);

        for (Button button : buttons) {
            button.update(mouseButtons);
        }
        
        for (DropDown dd : DD) {
            dd.update(mouseButtons);
        }
    }

    public void addButtonListener(ButtonListener listener) {
        for (Button button : buttons) {
            button.addListener(listener);
        }
    }
    
    public void addDropDown(DropDownListener listener){
        for (DropDown dropd : DD) {
            dropd.addListener(listener);
        }
    }

    public void buttonPressed(Button button) {
    }

}
