package message.data;

import java.io.Serializable;

public class WormHoleData implements Serializable {
	
    /**
     * 
     */
    private static final long serialVersionUID = 4590524841282918444L;
    
    private int location;
	private int destination;
	
	public WormHoleData(int location, int destination) {
		this.location = location;
		this.destination = destination;
	}
	
	public WormHoleData(WormHoleData WHD){
	    this.location = WHD.location;
        this.destination = WHD.destination;
	}

	public int getLocation() {
		return location;
	}

	public int getDestination() {
		return destination;
	}
	
	@Override
	public String toString() {
		return "WormHoleData [location=" + location + ", destination=" + destination + "]";
	}
}
