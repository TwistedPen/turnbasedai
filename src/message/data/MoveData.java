package message.data;

import java.io.Serializable;

public class MoveData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6270169814840170246L;
	
	private int currentLocation;
	private int fleetSize;
	private int moveCommand;
	
	public MoveData(int currentLocation, int fleetSize, int moveCommand) {
		this.currentLocation = currentLocation;
		this.fleetSize = fleetSize;
		this.moveCommand = moveCommand;
	}

	public int getCurrentLocation() {
		return currentLocation;
	}

	public int getFleetSize() {
		return fleetSize;
	}
	
	public void setFleetSize(int fleetSize) {
	    this.fleetSize = fleetSize;
	}

	public int getMoveCommand() {
		return moveCommand;
	}

	@Override
	public String toString() {
		return "MoveData [currentLocation=" + currentLocation + ", fleetSize="
				+ fleetSize + ", moveCommand=" + moveCommand + "]";
	}
}
