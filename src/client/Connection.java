package client;

import java.util.*;
import java.net.*;
import java.io.*;
import message.*;
import message.application.*;

public class Connection implements Runnable {

	private int clientID = 0;
	private boolean init = false;
	private boolean isRunning = true;
	private boolean isConnected = false;
	private ArrayList<Message> fromServer;
	private String serverName = "";
	private int portNumber = 1024;
	private Socket serverConnection = null;
	private ObjectOutputStream output = null;
	private ObjectInputStream input = null;

	public boolean isConnected() {
		return isConnected;
	}
	
	public void setClientID(int ID) {
		clientID = ID;
	}

	public synchronized boolean checkUpdate() {
		if (fromServer.size() > 0)
			return true;
		return false;
	}

	public synchronized Message getUpdate() {
		Message msg = fromServer.get(0);
		fromServer.remove(0);
		return msg;
	}

	public int getClientID() {
		return clientID;
	}

	public synchronized void sendUpdate(Message resp) {
		try {
			output.writeObject(resp);
			/* See: http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6525563 */
			output.reset();

		} catch (UnknownHostException e) {

		} catch (IOException e) {

		}
	}

	public void setServerName(String name) {
		serverName = name;
	}

	public void setPortNumber(int num) {
		portNumber = num;
	}

	public void shutdown() {

		disconnect();
		isRunning = false;

	}

	public void disconnect() {
		try {
			if (isConnected && init) {
				ClientShutDownInd shutdown = new ClientShutDownInd(clientID);
				sendUpdate(shutdown);
				fromServer.clear();
				output.close();
				input.close();
				serverConnection.close();
				isConnected = false;
				init = false;
			}
		} catch (UnknownHostException e) {

		} catch (IOException e) {

		}
	}

	public synchronized void establishConnection() {
		isConnected = true;
	}
	
	public synchronized void abortConnection() {
	    isConnected = false;
	}

	public void run() {

		try {
			
			fromServer = new ArrayList<Message>();
			
			while (isRunning) {

				try { Thread.sleep(500); } catch (InterruptedException e) {}
				
				if (isConnected && !init) {
					init = true;
					JoinServerReq joinServerReq = new JoinServerReq();
					serverConnection = new Socket(serverName,portNumber);
					output = new ObjectOutputStream(serverConnection.getOutputStream());
					input = new ObjectInputStream(serverConnection.getInputStream());
					output.writeObject(joinServerReq);
					output.reset();
				}
				
				while (isConnected && init) {

					fromServer.add((Message)input.readObject());

				}
			}
			
		} catch (UnknownHostException e) {
			disconnect();
		} catch (IOException e) {
			disconnect();
		} catch (ClassNotFoundException e) {

		}

	}
}
