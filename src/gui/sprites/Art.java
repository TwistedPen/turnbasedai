package gui.sprites;

import client.Client;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Art {
	public static Bitmap[][] cellTile = cut("/cell.png",52,60);
	public static Bitmap[][] buttons = cut("/buttons.png", 128, 24);
	public static Bitmap[][] small_buttons = cut("/small_buttons.png", 24, 24);
	public static Bitmap[][] font = cut("/gamfont_blue.png", 8, 8);
	public static Bitmap[][] ships = cut("/Ship.png",52,60);
	public static Bitmap wormHole = load("/WormHole.png");
	public static Bitmap[][] debris = cut("/debris1.png",52,60);
	public static Bitmap[][] DD_bg = cut("/dd_bg_2.png",64,12);
	public static Bitmap bg_game = load("/Background_Game.png");
	public static Bitmap[][] Planets = cut("/Planet2.png",52,60);
	
	

	private static Bitmap[][] cut(String string, int w, int h) {
	    return cut(string, w, h, 0, 0);
	}

	private static Bitmap[][] cut(String string, int w, int h, int bx, int by) {
	    try {
	        BufferedImage bi = ImageIO.read(Client.class.getResource(string));
	        
	        int xTiles = (bi.getWidth() - bx) / w;
	        int yTiles = (bi.getHeight() - by) / h;

	        Bitmap[][] result = new Bitmap[xTiles][yTiles];

	        for (int x = 0; x < xTiles; x++) {
	            for (int y = 0; y < yTiles; y++) {
	                result[x][y] = new Bitmap(w, h);
	                bi.getRGB(bx + x * w, by + y * h, w, h, result[x][y].pixels, 0, w);
	            }
	        }

	        return result;
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return null;
	}


	private static Bitmap load(String string) {
	    try {
	        BufferedImage bi = ImageIO.read(Client.class.getResource(string));

	        int w = bi.getWidth();
	        int h = bi.getHeight();

	        Bitmap result = new Bitmap(w, h);
	        bi.getRGB(0, 0, w, h, result.pixels, 0, w);

	        return result;
	    } catch (IOException e) {
	        e.printStackTrace();
	    }

	    return null;
	}

}