package gui;

import gui.sprites.Art;
import gui.sprites.Screen;

import java.util.ArrayList;
import java.util.List;

import client.MouseButtons;

public class DDItem extends GuiComponent {

    protected List<DDItemListener> listeners;
    
    protected boolean mouseOver;
    
    protected final int x;
    protected final int y;
    private final int w;
    private final int h;
    
    private DropDown dropDown;
    
    private int indexItem;
    private String item;

    protected boolean performClick = false;

    public DDItem(String item, int x, int y, DropDown dropDown, int indexInList) {
        
        this.dropDown = dropDown;
        this.x = x;
        this.y = y;
        this.w = 64;
        this.h = 12;
        this.indexItem = indexInList;
        
        int maxNumLetters = (w-2-2)/8;
        
        //if the string is longer than the dropdown box minus the border of it the short it down
        if(Font.getStringWidth(item) > (w-2-2)){
            item = item.substring(0, maxNumLetters -1);
            this.item = item + "..";
        }else 
            this.item = item;
    }

    @Override
    public void update(MouseButtons mouseButtons) {
        super.update(mouseButtons);

        int mx = mouseButtons.getX() / Gui.SCALE;
        int my = mouseButtons.getY() / Gui.SCALE;
        mouseOver = false;
        if (mx >= x && my >= y && mx < (x + w) && my < (y + h)) {
            mouseOver = true;
            if (mouseButtons.isRelased(1)) {
                dropDown.itemSelected(this);
                dropDown.setIsDroppedDown(false);
                postClick();
            }
        }

        if (performClick) {
            if (listeners != null) {
                for (DDItemListener listener : listeners) {
                    listener.itemSelected(this);
                }
            }
            performClick = false;
        }

    }
        

    public void postClick() {
        performClick = true;
    }

    @Override
    public void render(Screen screen) {

        if (mouseOver) {
            screen.blit(Art.DD_bg[1][0], x, y);
            Font.draw(screen, item, x+2, y+2);
        } else {
            screen.blit(Art.DD_bg[0][0], x, y);
            Font.draw(screen, item, x+2, y+2);
        }
    }

    public boolean isPressed() {
        return mouseOver;
    }

    public void addListener(DDItemListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<DDItemListener>();
        }
        listeners.add(listener);
    }

    public String getItem() {
        return item;
    }
    
    public int getIndex(){
        return indexItem;
    }
    
}
