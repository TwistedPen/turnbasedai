package gui;

public interface ButtonListener {

    public void buttonPressed(Button button);
}
