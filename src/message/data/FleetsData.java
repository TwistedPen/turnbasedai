package message.data;

import java.io.Serializable;
import java.util.Arrays;

public class FleetsData implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 2182419169053258851L;
    
    private int playerId;
	private int[] locations;
	private int[] fleetSizes;
	
	public FleetsData(int playerId, int[] locations, int[] fleetSizes) {
		this.playerId = playerId;
		this.locations = locations;
		this.fleetSizes = fleetSizes;
	}

	public FleetsData(FleetsData fleetsData) {
	    this.playerId = fleetsData.playerId;
	    
	    if(fleetsData.locations != null ){
	        this.locations = new int[fleetsData.locations.length];
	        System.arraycopy(fleetsData.locations, 0, this.locations, 0, fleetsData.locations.length);
	    } else
	        this.locations = new int[]{};
	    
	    if(fleetsData.fleetSizes != null){
	        this.fleetSizes = new int[fleetsData.fleetSizes.length];
	        System.arraycopy(fleetsData.fleetSizes, 0, this.fleetSizes, 0, fleetsData.fleetSizes.length);
	    } else
	        this.fleetSizes = new int[]{};
    }

    public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public int[] getLocations() {
		return locations;
	}

	public void setLocations(int[] locations) {
	    this.locations = locations;
	}

	public int[] getFleetSizes() {
		return fleetSizes;
	}

	public void setFleetSizes(int[] fleetSizes) {
		this.fleetSizes = fleetSizes;
	}
	
	@Override
	public String toString() {
		
		return "FleetData [playerId=" + playerId + ", " + "\n" + "locations="
				+ Arrays.toString(locations) + "\n" + "fleetSizes="
				+ Arrays.toString(fleetSizes) + "]";
	}
}
