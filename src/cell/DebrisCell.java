package cell;

import gui.sprites.Art;
import gui.sprites.Screen;

public class DebrisCell extends Cell {

	public DebrisCell(){
		super();
		this.setID(3);
	}

	@Override
	public void render(Screen screen) {
		super.render(screen);
	    
		if(this.isVisible()){
			
			screen.blit(Art.cellTile[0][0], this.getXpos(), this.getYpos());
			screen.blit(Art.debris[0][0], this.getXpos(), this.getYpos());
		
		} else{
			
			screen.blit(Art.cellTile[1][0], this.getXpos(), this.getYpos());
			screen.blit(Art.debris[0][0], this.getXpos(), this.getYpos());
		}
	}
}
