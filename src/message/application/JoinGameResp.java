package message.application;

import java.io.Serializable;

import message.Constant;
import message.MessagePlayer;

public class JoinGameResp extends MessagePlayer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3031368604856952613L;
	
	private int response;

	public JoinGameResp(int clientId, int gameId, int playerId,
			int response) {
		super(Constant.RESP_JOIN_GAME.getId(), clientId, gameId, playerId);
		this.response = response;
	}

	public int getResponse() {
		return response;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "JoinGameResp [response=" + response + "]";
	}
}
