package server.server;

import message.application.ClientShutDownInd;
import message.application.CloseGameReq;
import message.application.JoinGameReq;
import message.application.JoinServerReq;
import message.application.NewGameReq;
import server.client.Client;

abstract class ServerState {
    
    abstract void handleNewClientConnection(ServerAdmin serverAdmin, Client client, int connectionCounter);
    abstract void handleJoinServerReq(ServerAdmin serverAdmin, Client client, JoinServerReq req);
    abstract void handleNewGameReq(ServerAdmin serverAdmin, Client client, NewGameReq req);
    abstract void handleJoinGameReq(ServerAdmin serverAdmin, Client client, JoinGameReq req);

    public void handleCloseGameReq(ServerAdmin serverAdmin, Client client, CloseGameReq req) {
        serverAdmin.serviceCloseGameReq(client, req);
    }
    
    public void handleClientShutDownInd(ServerAdmin serverAdmin, Client client, ClientShutDownInd ind) {
        serverAdmin.serviceClientShutDownInd(client, ind);
    }
}

/* TODO: 
 * verify client id matches id in message.
 * verify message should be received from client with this state 
 */
class RunningState extends ServerState {
    
    public void handleNewClientConnection(ServerAdmin serverAdmin, Client client, int connectionCounter) {
        serverAdmin.serviceAddNewClientConnection(client);
        (new Thread(client, "Client " + connectionCounter++)).start();
    }
    
    public void handleJoinServerReq(ServerAdmin serverAdmin, Client client, JoinServerReq req) {
        serverAdmin.serviceJoinServerReq(client, req);
    }
    
    public void handleNewGameReq(ServerAdmin serverAdmin, Client client, NewGameReq req) {
        serverAdmin.serviceNewGameReq(client, req);        
    }
    
    public void handleJoinGameReq(ServerAdmin serverAdmin, Client client, JoinGameReq req) {
        serverAdmin.serviceJoinGameReq(client, req);
    }
}

/* TODO: no new connections, no join server, no new games, no join game */
class ShutDownState extends ServerState {
    
    public void handleNewClientConnection(ServerAdmin serverAdmin, Client client, int connectionCounter) {
        serverAdmin.serviceAddNewClientConnection(client);
        (new Thread(client, "Client " + connectionCounter++)).start();
    }
    
    public void handleJoinServerReq(ServerAdmin serverAdmin, Client client, JoinServerReq req) {
        serverAdmin.serviceJoinServerReq(client, req);
    }
    
    public void handleNewGameReq(ServerAdmin serverAdmin, Client client, NewGameReq req) {
        serverAdmin.serviceNewGameReq(client, req);
    }
    
    public void handleJoinGameReq(ServerAdmin serverAdmin, Client client, JoinGameReq req) {
        serverAdmin.serviceJoinGameReq(client, req);
    }
}
