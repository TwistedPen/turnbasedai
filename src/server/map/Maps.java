package server.map;

import java.util.Random;

import constants.GameConstant;
import server.server.ServerAdmin;
import message.Constant;
import message.data.GameData;
import message.data.WormHoleData;
import message.data.PlanetData;
import message.data.FleetsData;

/* 
 * NOTE: As long as we are not modifying the maps it is not necessary to synchronize 
 * The singleton implementation is due to Pugh 
 */

public class Maps {

	private Map[][] maps;
	
	private static class MapsHolder{
		public static final Maps INSTANCE = new Maps();
	}
	
	private Maps() {
		
		maps = new Map[GameConstant.MAX_ACTIVE_PLAYER_COUNT.getValue()][];
		
		maps[2] = new Map[] {
				new Map(
						new GameData(2, 20, 30, 40, 50, 400000, new WormHoleData[] {
								new WormHoleData(1, 10), new WormHoleData(20, 30),
								new WormHoleData(14, 17) }, new int[] { 1, 2, 3, 4,
								5 }, new FleetsData[] {new FleetsData(1,new int[]{},new int[]{}),new FleetsData(2,new int[]{},new int[]{})},
				
						new PlanetData[] {
								new PlanetData(55, Constant.NONE.getId(), 6, 5, 4,
										2, 3),
								new PlanetData(67, Constant.NONE.getId(), 10, 8, 5,
										3, 4),
								new PlanetData(76, Constant.NONE.getId(), 10, 8, 5,
										3, 4) }, new int[] { 55, 67 }, new int[] {})),
										
				new Map(new GameData(2, 10, 4, 40, 50, 400000, new WormHoleData[] {
						new WormHoleData(2, 8) },
						
						new int[] { 1, 4, 5, 7, 9, 10, 11, 12, 13, 16, 18, 19,
				                    20, 21, 22, 23, 25, 26, 30, 31, 32, 33, 35, 37, 39 },
						
						new FleetsData[] {new FleetsData(1,new int[]{},new int[]{}),new FleetsData(2,new int[]{},new int[]{})},

						new PlanetData[] {
								new PlanetData(34, Constant.NONE.getId(), 2, 2, 100,
										1, 5),
								new PlanetData(0, Constant.NONE.getId(), 0, 0, 50,
										1, 2),
								new PlanetData(3, Constant.NONE.getId(), 0, 0, 2,
										1, 1),
								new PlanetData(29, Constant.NONE.getId(), 2, 0, 2,
										1, 2) }, new int[] { 0, 3 },
						new int[] {})),
										
				new Map(new GameData(2, 40, 46, 66, 30, 300000, new WormHoleData[] {
						new WormHoleData(1, 10), new WormHoleData(20, 30),
						new WormHoleData(14, 17) }, new int[] { 1, 2, 3, 4, 5 },
						new FleetsData[] {new FleetsData(1,new int[]{},new int[]{}),new FleetsData(2,new int[]{},new int[]{})},
		
						new PlanetData[] {
								new PlanetData(100, Constant.NONE.getId(), 6, 5, 4,
										2, 3),
								new PlanetData(89, Constant.NONE.getId(), 10, 8, 5,
										3, 4),
								new PlanetData(55, Constant.NONE.getId(), 10, 8, 5,
										3, 4) }, new int[] { 100, 55 },
						new int[] {})), 
				 new Map(new GameData(2, 12, 4, 66, 30, 300000, new WormHoleData[] {
		                new WormHoleData(14, 22), }, 
		                new int[] {},
		                new FleetsData[] {new FleetsData(1,new int[]{},new int[]{}),new FleetsData(2,new int[]{},new int[]{})},
		        
		                new PlanetData[] {
		                new PlanetData(16, Constant.NONE.getId(), 1, 1, 10,
		                                 10, 3),
		                new PlanetData(21, Constant.NONE.getId(), 2, 2, 5,
		                                 3, 3),
		                new PlanetData(30, Constant.NONE.getId(), 1, 1, 1,
		                                 10, 6) }, new int[] { 16, 30},
		                new int[] {}))};
						
		maps[3] = new Map[] {
		  
		        new Map(
                        new GameData(3, 24, 20, 40, 50, 400000, new WormHoleData[] {
                                new WormHoleData(34, 167), new WormHoleData(50, 167),
                                new WormHoleData(66, 412), new WormHoleData(303, 412),
                                new WormHoleData(268, 425), new WormHoleData(441, 425),
                                new WormHoleData(263, 178), new WormHoleData(380, 201),
                                new WormHoleData(361, 226) },
                                
                        new int[] { 25, 26, 27, 40, 41, 42, 49, 52, 58, 59, 64, 67, 73, 74, 81, 83, 89, 90, 99, 105, 107,
                                    113, 142, 143, 144, 166, 168, 177, 189, 193, 203, 213, 217, 221, 222, 225, 229, 236,
                                    242, 245, 254, 260, 264, 266, 269, 270, 271, 276, 278, 283, 285, 286, 287, 291, 301, 
                                    302, 307, 313, 315, 330, 332, 337, 340, 354, 357, 360, 364, 377, 381, 384, 389, 401,
                                    406, 413, 416, 417, 419, 424, 432, 433, 434, 435, 436, 437, 440, 443, 449, 450, 451,
                                    452, 453, 454, 455, 464, 465, 466 },
                        
                        new FleetsData[]{new FleetsData(1,new int[]{},new int[]{}),new FleetsData(2,new int[]{},new int[]{}),
                                        new FleetsData(3,new int[]{},new int[]{})},

                        new PlanetData[] {
                                new PlanetData(76, Constant.NONE.getId(), 2, 2, 6, 3, 3),       // BASIC PLANETS
                                new PlanetData(87, Constant.NONE.getId(), 2, 2, 6, 3, 3),       // Production: 6 ships (each 3rd round)
                                new PlanetData(100, Constant.NONE.getId(), 2, 2, 6, 3, 3),      // Requirements: 2 req, 2 cost
                                new PlanetData(112, Constant.NONE.getId(), 2, 2, 6, 3, 3),      // Vision: 3
                                new PlanetData(151, Constant.NONE.getId(), 2, 2, 6, 3, 3),
                                new PlanetData(157, Constant.NONE.getId(), 2, 2, 6, 3, 3),
                                new PlanetData(298, Constant.NONE.getId(), 2, 2, 6, 3, 3),
                                new PlanetData(394, Constant.NONE.getId(), 2, 2, 6, 3, 3),
                                new PlanetData(395, Constant.NONE.getId(), 2, 2, 6, 3, 3),
                                
                                new PlanetData(75, Constant.NONE.getId(), 4, 2, 6, 2, 2),       // BASIC EXPANSIONS / GATES
                                new PlanetData(88, Constant.NONE.getId(), 4, 2, 6, 2, 2),       // Production: 6 ships (each 2nd round)
                                new PlanetData(289, Constant.NONE.getId(), 4, 2, 6, 2, 2),      // Requirements: 4 req, 2 cost
                                new PlanetData(309, Constant.NONE.getId(), 4, 2, 6, 2, 2),      // Vision: 2
                                new PlanetData(407, Constant.NONE.getId(), 4, 2, 6, 2, 2),
                                new PlanetData(418, Constant.NONE.getId(), 4, 2, 6, 2, 2),
                                       
                                new PlanetData(202, Constant.NONE.getId(), 6, 4, 6, 1, 3),      // THIRD EXPANSIONS / MIDDLE
                                new PlanetData(215, Constant.NONE.getId(), 6, 4, 6, 1, 3),      // Production: 6 ships (per round)
                                new PlanetData(387, Constant.NONE.getId(), 6, 4, 6, 1, 3),      // Requirements: 6 req, 4 cost
                                new PlanetData(403, Constant.NONE.getId(), 6, 4, 6, 1, 3),      // Vision: 3

                                new PlanetData(82, Constant.NONE.getId(), 18, 18, 20, 2, 4),    // RICH PLANETS
                                new PlanetData(246, Constant.NONE.getId(), 18, 18, 20, 2, 4),   // Production: 20 ships (each 2nd round)
                                new PlanetData(277, Constant.NONE.getId(), 18, 18, 20, 2, 4),   // Requirements: 18 req, 18 cost
                                new PlanetData(311, Constant.NONE.getId(), 18, 18, 20, 2, 4),   // Vision: 4
                                new PlanetData(336, Constant.NONE.getId(), 18, 18, 20, 2, 4),
                                new PlanetData(358, Constant.NONE.getId(), 18, 18, 20, 2, 4),}
                        
                                , new int[] { 151, 157, 298 }, new int[] {})),
                };
		
		maps[4] = new Map[] {
				new Map(
						new GameData(4, 10, 8, 40, 50, 400000, new WormHoleData[] {
								new WormHoleData(12, 4), new WormHoleData(17, 9),
								new WormHoleData(52, 49), new WormHoleData(57, 44) },
								
						new int[] { 21,22,26,27,61,62,66,67 },
						
						new FleetsData[]{new FleetsData(1,new int[]{},new int[]{}),new FleetsData(2,new int[]{},new int[]{}),
						                new FleetsData(3,new int[]{},new int[]{}),new FleetsData(4,new int[]{},new int[]{})},

						new PlanetData[] {
								new PlanetData(11, Constant.NONE.getId(), 2, 1, 4,	// HOME PLANETS
										4, 2),										// Production: 4 ships (each 4th round)
								new PlanetData(16, Constant.NONE.getId(), 2, 1, 4,	// Requirements: 0 req, 0 cost
										4, 2),										// Vision: 2
								new PlanetData(51, Constant.NONE.getId(), 2, 1, 4,
										4, 2),
								new PlanetData(56, Constant.NONE.getId(), 2, 1, 4,
										4, 2),				
								new PlanetData(20, Constant.NONE.getId(), 2, 1, 6,	// NATURAL EXPANSIONS
										6, 1),										// Production: 6 ships (each 6th round)
								new PlanetData(25, Constant.NONE.getId(), 2, 1, 6,	// Requirements: 2 req, 1 cost
										6, 1),										// Vision: 1
								new PlanetData(60, Constant.NONE.getId(), 2, 1, 6,
										6, 1),
								new PlanetData(65, Constant.NONE.getId(), 2, 1, 6,
										6, 1),
								new PlanetData(33, Constant.NONE.getId(), 6, 4, 12,	// THIRD EXPANSION
										6, 3),										// Production: 12 ships (each 6th round)
								new PlanetData(38, Constant.NONE.getId(), 6, 4, 12,	// Requirements: 6 req, 4 cost
										6, 3),										// Vision: 3
								new PlanetData(73, Constant.NONE.getId(), 6, 4, 12,
										6, 3),
								new PlanetData(78, Constant.NONE.getId(), 6, 4, 12,
										6, 3) }, new int[] { 11, 16, 51, 56 }, new int[] {})),
				};
	}
	
	

	public static Maps getInstance() {
		return MapsHolder.INSTANCE;
	}
	
	public Map[][] getMaps() {
		return maps;
	}
	
	/* 
	 * NOTE: The same map index may be returned several times - we can't produce truly unique AND truly random numbers 
	 *       Should allow maps with a higher player count to be returned by modifying the number of homeplanets 
	 */
	
	public Map[] getRandomMapsWithPlayerCount(int playerCount) {

		Random r = new Random();
		
		Map[] allMaps = maps[playerCount];
		Map[] options = new Map[ServerAdmin.MAP_OPTIONS_COUNT];
		
		for (int i = 0; i < ServerAdmin.MAP_OPTIONS_COUNT; i++) {
			options[i] = allMaps[r.nextInt(allMaps.length)];
		}
		
		return options;
	}
	
	public Map[] getMapsWithPlayerCount(int playerCount) {

	    /* 
	     * This function transforms all existing maps (with greater or
	     * equal playerCount) into a map fitted to the amount of players.
	     * This means overwriting fleetsData as well as playerCount, or else
	     * the clients will respond in a bad manner !!
	     */
	    
	    int size = 0;
	    for (int i = playerCount; i < GameConstant.MAX_ACTIVE_PLAYER_COUNT.getValue(); i++)
	        if (maps[i] != null)
	            size += maps[i].length;

        FleetsData[] fleets = new FleetsData[playerCount];
        for (int i = 1; i <= playerCount; i++)
            fleets[i-1] = new FleetsData(i,new int[]{},new int[]{});
        
        int index = 0;
	    Map[] result = new Map[size];
	    for (int i = playerCount; i < GameConstant.MAX_ACTIVE_PLAYER_COUNT.getValue(); i++)
	        if (maps[i] != null)
	            for (int j = 0; j < maps[i].length; j++) {
	                GameData temp = new GameData(maps[i][j].getMapData());
	                temp.setPlayerCount(playerCount);
	                temp.setFleets(fleets);
	                result[index] = new Map(temp);
	                index++;
	            }
	    
        return result;
        
    }
}
