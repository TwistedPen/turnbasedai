package server.server;

import java.net.Socket;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import server.map.*;

import server.client.Client;

import constants.GameConstant;
import server.game.Game;
import server.map.Maps;

import message.Constant;
import message.Message;
import message.application.ClientShutDownInd;
import message.application.CloseGameReq;
import message.application.GameListReq;
import message.application.GameListResp;
import message.application.JoinGameReq;
import message.application.JoinServerReq;
import message.application.JoinServerResp;
import message.application.NewGameReq;
import message.application.NewGameResp;
import message.application.ServerShutDownInd;
import message.data.GameListData;

/* Contains the current state of the server */
public class ServerAdmin {

    /* Maximum number of clients the server can handle */
    private static final int MAX_CLIENTS_COUNT = 1000;
    /* Maximum number of games the server can handle */
    private static final int MAX_GAMES_COUNT = 100;
    /* Number of map options supplied per game */
    public static final int MAP_OPTIONS_COUNT = 5;

    /*
     * When a client joins the first id on this list is chosen - 
     * when the client is removed the id is returned to the list.
     */
    private final LinkedList<Integer> availableClientIds;
    /*
     * When a game is added an id is chosen from this list - 
     * when the game is removed the id is returned to the list.
     */
    private final LinkedList<Integer> availableGameIds;

    private final ConcurrentHashMap<Integer, Client> clients;
    private final ConcurrentHashMap<Socket, Client> newClientConnections;
    
    private final ConcurrentHashMap<Integer, Game> games;

    /* Contents of list of games published to clients */
    private final TreeMap<Integer, GameListData> gameList;

    private final ReentrantReadWriteLock stateLock;
    private final Lock stateLockRead;  /* Lock when calling methods depending on current state */
    private final Lock stateLockWrite; /* Lock when updating current state */
    
    private ServerState currentState;
    private final ServerState running;
    private final ServerState shutDown;

    public ServerAdmin() {

        /* ID = 0 is never used */
        this.clients = new ConcurrentHashMap<Integer, Client>(MAX_CLIENTS_COUNT);
        this.newClientConnections = new ConcurrentHashMap<Socket, Client>();
        this.games = new ConcurrentHashMap<Integer, Game>(MAX_GAMES_COUNT);
        this.gameList = new TreeMap<Integer, GameListData>();
        this.availableClientIds = new LinkedList<Integer>();
        this.availableGameIds = new LinkedList<Integer>();

        /* ID = 0/NONE is never used */
        for (int i = 1; i <= MAX_CLIENTS_COUNT; i++) {
            availableClientIds.add(i);
        }

        /* ID = 0/NONE is never used */
        for (int i = 1; i <= MAX_GAMES_COUNT; i++) {
            availableGameIds.add(i);
        }

        stateLock = new ReentrantReadWriteLock();
        stateLockRead = stateLock.readLock();
        stateLockWrite = stateLock.writeLock();
        running = new RunningState();
        shutDown = new ShutDownState();
        currentState = running;
    }
    
    public void handleNewClientConnection(Client newClient, int connectionCounter) {
        try {
            stateLockRead.lock();
            currentState.handleNewClientConnection(this, newClient, connectionCounter);
        } finally {
            stateLockRead.unlock();
        }
    }
    
    public void serviceAddNewClientConnection(Client newClient) {
        newClientConnections.put(newClient.getSocket(), newClient);
    }
    
    /* Returns none - if there are no available ids */
    /* Absolutely the first thing to be done when a client is added */
    private int getClientId() {

        synchronized (availableClientIds) {
            if (availableClientIds.isEmpty()) {
                return Constant.NONE.getId();
            } else {
                return availableClientIds.removeFirst();
            }
        }
    }

    public void handleJoinServerReq(Client client, JoinServerReq req) {
        try {
            stateLockRead.lock();
            currentState.handleJoinServerReq(this, client, req);
        } finally {
            stateLockRead.unlock();
        }
    }
    
    public void serviceJoinServerReq(Client client, JoinServerReq req) {

        int currClientId = getClientId();
        
        if (currClientId == Constant.NONE.getId()) {
            client.sendMessage(new JoinServerResp(currClientId, Constant.TOO_MANY_CLIENTS.getId()));
            /*
             * Client should be allowed to send more join requests - other clients may shut down.
             */
            return;
        } else {
            client.sendMessage(new JoinServerResp(currClientId, Constant.OK.getId()));
            clients.put(currClientId, client);
            client.setId(currClientId);
            client.sendMessage(getGameListMessage());
        }
    }

    /* Returns none - if there are no available ids */
    /* Absolutely the first thing to be done when a game is added */
    private int getGameId() {

        synchronized (availableGameIds) {
            if (availableGameIds.isEmpty()) {
                return Constant.NONE.getId();
            } else {
                return availableGameIds.removeFirst();
            }
        }
    }

    /*
     * In the event that the server can refuse a new game for other reasons than
     * too many games - it makes sense that the message is handled here.
     */
    public void handleNewGameReq(Client client, NewGameReq req) {
        try {
            stateLockRead.lock();
            currentState.handleNewGameReq(this, client, req);
        } finally {
            stateLockRead.unlock();
        }
    }
    
    public void serviceNewGameReq(Client client, NewGameReq req) {

        int clientId = client.getId();
        int playerCount = req.getPlayerCount();

        /* Player count may be too small or too large */
        if (playerCount < 2 || playerCount > GameConstant.MAX_ACTIVE_PLAYER_COUNT.getValue()) {
            client.sendMessage(new NewGameResp(clientId, Constant.NONE.getId(), Constant.REQUESTED_PLAYER_COUNT_ERROR.getId()));
        } else {

            int newGameId = getGameId();

            /* Server has too many games */
            if (newGameId == Constant.NONE.getId()) {
                client.sendMessage(new NewGameResp(clientId, Constant.NONE.getId(), Constant.TOO_MANY_GAMES.getId()));
            } else {

                Map currentMaps[] = Maps.getInstance().getMapsWithPlayerCount(req.getPlayerCount());

                /* No suitable maps were available */
                if (currentMaps == null) {
                    client.sendMessage(new NewGameResp(clientId, Constant.NONE.getId(), Constant.NO_MAPS_AVAILABLE.getId()));
                } else {
                    Game newGame = new Game(this, newGameId, req.getPlayerCount(), currentMaps);
                    games.put(newGameId, newGame);
                    client.sendMessage(new NewGameResp(clientId, newGameId, Constant.OK.getId()));
                    gameList.put(newGame.getId(), newGame.getGameData());
                }
            }
        }
    }
    
    public void handleJoinGameReq(Client client, JoinGameReq req) {
        try {
            stateLockRead.lock();
            currentState.handleJoinGameReq(this, client, req);
        } finally {
            stateLockRead.unlock();
        }
    }
    
    public void serviceJoinGameReq(Client client, JoinGameReq req) {
        int gameId = req.getGameId();
        
        synchronized(games) {
            games.get(gameId).handleJoinGameRequest(client, req);
        }
    }
    
    public void handleGameListReq(Client client, GameListReq req) {
        client.sendMessage(this.getGameListMessage());
    }
    
    public void handleCloseGameReq(Client client, CloseGameReq received) {
        try {
            stateLockRead.lock();
            currentState.handleCloseGameReq(this, client, received);
        } finally {
            stateLockRead.unlock();
        }
    }
    
    public void serviceCloseGameReq(Client client, CloseGameReq req) {
        int gameId = req.getGameId();
        
        synchronized(games) {
            (games.get(gameId)).serviceCloseGameReq(client, req);
        }
    }

    /* Should be called when the client has shut down */
    public void deregisterClient(int clientId) {
        clients.remove(clientId);
        synchronized (availableClientIds) {
            availableClientIds.add(clientId);
        }
    }

    /* Should be called by game just before it is shut down */
    public void deregisterGame(int gameId) {
        games.remove(gameId);
        synchronized (availableGameIds) {
            availableGameIds.add(gameId);
        }
    }

    public Game getGame(int gameId) {
        return games.get(gameId);
    }

    /*
     * The updated gamelist should be sent to all registered clients after this
     * operation is done
     */
    public void updateGameListMessage(int gameId, int currPlayerCount, int currSpectatorCount, int maxPlayerCount, int currentState) {

        synchronized (gameList) {
             GameListData current = gameList.put(gameId, new GameListData(gameId, currPlayerCount, currSpectatorCount, maxPlayerCount, currentState));
        }
    }

    public GameListResp getGameListMessage() {

        synchronized (gameList) {
            return new GameListResp(gameList.values().toArray(new GameListData[gameList.values().size()]));
        }
    }

    /* Shut down the entire server */
    public void shutDown() {

        Message output = new ServerShutDownInd();

        /*
         * Each client should call deregister client as part of its shutdown
         * procedure
         */
        synchronized (clients) {
            for (Client client : clients.values()) {
                if (client != null) {
                    client.sendMessage(output);
                    client.shutDown();
                }
            }
        }

        /*
         * Each game should call deregister game as part of its shutdown
         * procedure
         */
        synchronized (games) {
            for (Game game : games.values()) {
                if (game != null) {
//                    game.sendMessage(output);
//                    game.shutDown();
                }
            }
        }
    }

    public void serviceClientShutDownInd(Client client, ClientShutDownInd ind) {
        /* Set client to shut down state */
    }
}