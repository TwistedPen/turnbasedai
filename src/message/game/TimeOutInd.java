package message.game;

import java.io.Serializable;
import message.Constant;
import message.MessagePlayer;

public class TimeOutInd extends MessagePlayer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 457630225687258758L;

	public TimeOutInd(int clientId, int gameId, int playerId) {
		super(Constant.IND_TIMEOUT.getId(), clientId, gameId, playerId);
	}
}
