package client.actors;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import cell.Cell;
import cell.PlanetCell;
import client.Client;
import client.Map;
import client.MoveCommand;
import client.actors.AIFunctions.Conquest;
import client.actors.AIFunctions.Fleet;
import client.actors.AIFunctions.Path;
import client.actors.AIFunctions.Quest;
import client.actors.AIFunctions.mapCalc;

import message.Constant;
import message.Message;
import message.data.FleetsData;
import message.data.GameData;
import message.data.MoveData;
import message.data.PlanetData;
import message.data.WormHoleData;
import message.game.ChooseHomePlanetReq;
import message.game.ChooseHomePlanetResp;
import message.game.ChooseMapReq;
import message.game.ChooseMapResp;
import message.game.GameStateInd;
import message.game.MoveReq;
import message.game.MoveResp;

public class AI extends Actor implements Runnable{
    
    private int actorID = Constant.PLAYER_INACTIVE.getId();
    
    public static final boolean DEBUG = false;
    
    private BlockingQueue<Message> bQueue = new LinkedBlockingQueue<Message>();
    
    private Client client;
    private volatile boolean active;
    private Map map;
    private Random random = null;
    private GameData dataMap;
    private Path[][] shortestPaths;
    
    public ArrayList<MoveCommand> AICommands = new ArrayList<MoveCommand>();
    public ArrayList<ArrayList<Cell>> AIPaths = new ArrayList<ArrayList<Cell>>();
    public PriorityQueue<Quest> Quests = new PriorityQueue<Quest>();
    public ArrayList<Fleet> serverFleetState = new ArrayList<Fleet>();
    public ArrayList<Fleet> aiFleetState = new ArrayList<Fleet>();
    
    public AI(Client c){
        super();
        this.client = c;
        active = true;
    }
    
    @Override
    public int getID() {
        return actorID;
    }
    

    @Override
    public void run() {
        Message newMessage = null;
        
        while(active){
            
            while(newMessage == null){
                try {
                    newMessage = bQueue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            
            if(DEBUG)
                System.out.println("Recieved " + Constant.values()[newMessage.getMessageId()] + " from client");
            
            switch (Constant.values()[newMessage.getMessageId()]) {
            
            case IND_GAME_STATE: {
               
                GameStateInd ind = (GameStateInd) newMessage;
                dataMap = new GameData(ind.getGameData());
                this.setPlayerID(ind.getPlayerId());
                
                // Map construction/translation;
                
                map = new Map(dataMap.getMapWidth(),dataMap.getMapHeight());
                
                for (WormHoleData worm : dataMap.getWormHoles()){
                    map.setWormhole(worm.getLocation(),worm.getDestination(),false);
                }

                for (int debris : dataMap.getDebrisLocations()){
                    map.setDebris(debris,false);
                }
                
                    
                for (PlanetData planet : dataMap.getPlanets()){
                    map.setPlanet(planet.getColonizedBy(),  // Planet Owner
                            planet.getLocation(),           
                            planet.getExpendFleetSize(),    // Cost
                            planet.getRequiredFleetSize(),  // Requirement
                            planet.getProductionFleetSize(),// Production
                            planet.getProductionRate(),
                            planet.getVisionRange(),false);
                }
                map.setHomePlanets(dataMap.getHomePlanetLocations());
                
                serverFleetState = new ArrayList<Fleet>();
                for (FleetsData fleet : dataMap.getFleets()) {
                    int playerID = fleet.getPlayerId();
                    int[] size = fleet.getFleetSizes();
                    int[] location = fleet.getLocations();
                    if(size != null){
                        for (int i = 0; i < size.length; i++){
                            map.setShip(playerID, location[i], size[i]);
                        }
                        if(fleet.getPlayerId() == ind.getPlayerId())
                            for (int i = 0; i < size.length; i++){
                                serverFleetState.add(new Fleet(location[i],size[i]));
                            }
                            
                    }
                }
                
               AIPaths = new ArrayList<ArrayList<Cell>>();
                if (ind.getTurnNumber() > 0){
                    
                    FleetsData[] fleets = dataMap.getFleets();
                    PlanetData[] planets = dataMap.getPlanets();
                    WormHoleData[] wormholes = dataMap.getWormHoles();
                            
                    
                    if (ind.getTurnNumber() == 1) {
                        //iterates over each player
                        for(int player = 0; player < fleets.length; player++){

                            if(fleets[player].getPlayerId() == ind.getPlayerId()){

                                int[] fleetLoc = fleets[player].getLocations();



                                shortestPaths = new Path[dataMap.getMapHeight()*dataMap.getMapWidth()][dataMap.getMapHeight()*dataMap.getMapWidth()];
                                for (int i = 0; i < planets.length; i++)
                                    if (planets[i].getColonizedBy() == 0) {
                                        shortestPaths[fleetLoc[0]][planets[i].getLocation()] = new Path(mapCalc.Astar(map.getCell(fleetLoc[0] % map.getWidth(), fleetLoc[0] / map.getWidth()),
                                                map.getCell(planets[i].getLocation() % map.getWidth(), planets[i].getLocation() / map.getWidth()) ,map.getWidth(),map.getHeight()),getPlayerID());
                                        for (int j = 0; j < wormholes.length; j++) {
                                            shortestPaths[wormholes[j].getDestination()][planets[i].getLocation()] = new Path(mapCalc.Astar(map.getCell(wormholes[j].getDestination() % map.getWidth(), wormholes[j].getDestination() / map.getWidth()),
                                                    map.getCell(planets[i].getLocation() % map.getWidth(), planets[i].getLocation() / map.getWidth()) ,map.getWidth(),map.getHeight()),getPlayerID());
                                            shortestPaths[fleetLoc[0]][wormholes[j].getLocation()] = new Path(mapCalc.Astar(map.getCell(fleetLoc[0] % map.getWidth(), fleetLoc[0] / map.getWidth()),
                                                    map.getCell(wormholes[j].getLocation() % map.getWidth(), wormholes[j].getLocation() / map.getWidth()) ,map.getWidth(),map.getHeight()),getPlayerID());
                                            if (shortestPaths[fleetLoc[0]][wormholes[j].getLocation()].getShortestPath() != null && shortestPaths[wormholes[j].getDestination()][planets[i].getLocation()].getShortestPath() != null)
                                                if (shortestPaths[fleetLoc[0]][planets[i].getLocation()].getDistance() > (shortestPaths[wormholes[j].getDestination()][planets[i].getLocation()].getDistance()
                                                        + shortestPaths[fleetLoc[0]][wormholes[j].getLocation()].getDistance() + 1))
                                                    shortestPaths[fleetLoc[0]][planets[i].getLocation()] = new Path(shortestPaths[fleetLoc[0]][wormholes[j].getLocation()],shortestPaths[wormholes[j].getDestination()][planets[i].getLocation()],getPlayerID());
                                        }
                                    }
                                break; //initial shotest path calculation ends
                            }
                        }
                    }
                                        
                    //check if there are new fleets to be made or if existing fleets have been destroyed
                    if(!aiFleetState.isEmpty())
                        updateStates(serverFleetState, aiFleetState);
                    else
                        for(Fleet f: serverFleetState){
                            aiFleetState.add(new Fleet(f.getLocation(),f.getSize()));
                        }
                        
                    
                    // Updates all quests and combine fleets that are one same quest and on same location
                    if(DEBUG)
                        System.out.println("updated " + Quests.size() + " Quests");
                    for(Quest q : Quests){
                        ArrayList<Fleet> f = q.getAssignedFleets();
                        for(int i = 0; i < f.size(); i++){
                            for(int j = 0; j < f.size(); j++){
                                if(i != j){
                                    if(f.get(i).getLocation() == f.get(j).getLocation()){
                                        f.get(i).setSize(f.get(i).getSize() + f.get(j).getSize());
                                        f.get(j).setSize(0);
                                    }
                                        
                                }
                            }
                        }
                        q.update(map);
                    }
                    
                    //removes all succeeded or failed quest
                    Iterator<Quest> IteQ = Quests.iterator();
                    while(IteQ.hasNext()){
                        Quest current = IteQ.next();
                        if(current.isFailure() || current.isSucces()){
                            IteQ.remove();
                            if(DEBUG)
                                System.out.println("removed Quest");
                        }
                    }
                    
                    //Assign ships not on quest to a existing quest
                    for(Quest quest : Quests){
                        while(!quest.hasEnoughShips()){
                            if(!assignFleetToQuest(quest,wormholes))
                                break;
                        }
                    }
                    
                    // if there still are ships not assigned to quest. create a new quest and assign ships to it.
                    // should continue until all fleets are assigned to a quest
                    while(!findFleetsNotOnQuest().isEmpty()){
                        Quest newQuest = createNewQuest();
                        if(newQuest != null)
                            assignFleetToQuest(newQuest,wormholes);
                        else 
                            break;
                    }
                    
                    ArrayList<Fleet> noQuestFleets = findFleetsNotOnQuest();
                    if(!noQuestFleets.isEmpty())
                        for(Fleet f : noQuestFleets)
                            f.assignQuest(Quests.peek());

                    for(Quest quest : Quests){

                        for(Fleet fleet : quest.getAssignedFleets()){
                            
                                ArrayList<Cell> stackPath = new ArrayList<Cell>();
                                
                                Cell[] path = getShortestAvailablePath(map.getCell(fleet.getLocation()), ((Conquest)quest).getGoal(), wormholes);
                                if(path == null)
                                    quest.failQuest();
                                else{
                                    if(DEBUG)
                                        System.out.println("start: " + fleet.getLocationX(map.getWidth()) + " : " + fleet.getLocationY(map.getWidth()) +
                                            " "+ fleet.getSize() +" goal:" + path[0].getX() + " : " + path[0].getY());

                                    if (path[0] != null && path.length > 0) {
                                        AICommands.add(new MoveCommand(fleet.getLocationX(map.getWidth()), fleet.getLocationY(map.getWidth()), 
                                                fleet.getSize(), 
                                                path[0].getX() , path[0].getY()));

                                        updateFleetState(fleet,path[0]);
                                    }
                                    for (Cell c : path)
                                        stackPath.add(c);

                                    AIPaths.trimToSize();
                                    AIPaths.add(stackPath);

                                
                            }
                        }
                        
                    }
                    
                    if(DEBUG)
                        System.out.println("send meessage to server with " + AICommands.size() + " moves");
                    
                    MoveData[] MD = new MoveData[AICommands.size()];
                    ArrayList<MoveCommand> temp = new ArrayList<MoveCommand>();
                    for(int i = 0; i < MD.length; i++){                        
                        temp.add(new MoveCommand(AICommands.get(i)));
                        MD[i] = new MoveData(AICommands.get(i).getCurrentLocationX() + map.getWidth()*AICommands.get(i).getCurrentLocationY(),
                                AICommands.get(i).getShips(),
                                AICommands.get(i).getDestinationX() + map.getWidth()*AICommands.get(i).getDestinationY());
                        
                    }
                    client.recievePathsFromAI(AIPaths);
                    client.recieveCommandsFromAI(temp);
                    AICommands.clear();
                    client.sendMessageToServer(new MoveReq(Client.server.getClientID(), client.getGameID(), client.getActor().getPlayerID(), client.turnNumber, MD));
                    
                }
                break;
            }
            
            case REQ_CHOOSE_MAP: {
                
                ChooseMapReq req = (ChooseMapReq) newMessage;
                if(req.getTimeOut() != Constant.IND_TIMEOUT.getId()){
                    GameData[] maps = req.getMaps();

                    //find the map with the highest number of planets
                    int mapChoice = 0 , choiceNumPlanets = 0;
                    for(int index = 0; index < maps.length; index++){

                        int numPlanets = maps[index].getPlanets().length;
                        if(numPlanets > choiceNumPlanets){
                            choiceNumPlanets = numPlanets;
                            mapChoice = index;
                        }
                    }

                    //return a message to the server with REQ_CHOOSE_MAP.
                    client.sendMessageToServer(new ChooseMapResp(Client.server.getClientID(),client.getGameID(),client.getActor().getPlayerID(),mapChoice));
                }
               
                break;
            }
            
            case REQ_CHOOSE_HOME_PLANET: {

                ChooseHomePlanetReq req = (ChooseHomePlanetReq) newMessage;

                if(req.getTimeOut() != Constant.IND_TIMEOUT.getId()){

                    // from the list of planets find the one with max of production/production rate
                    int[] planetLocations = req.getAvailableHomePlanetLocations();
                    int planetChoice = 0;
                    double maxPlanetValue = 0;

                    for(int index = 0; index < planetLocations.length; index++){
                        PlanetCell planet = (PlanetCell) map.getCell(planetLocations[index] % map.getWidth(), planetLocations[index] / map.getWidth());
                        double planetValue = planet.getProduction()/planet.getProductionRate();
                        if(planetValue > maxPlanetValue){
                            maxPlanetValue = planetValue;
                            planetChoice = index;
                        }
                    }
                    
                    client.sendMessageToServer(new ChooseHomePlanetResp(Client.server.getClientID(),client.getGameID(),client.getActor().getPlayerID(),planetLocations[planetChoice]));
                    
                }

                break;
            }
            
            case RESP_MOVE: {
                
                MoveResp resp = (MoveResp) newMessage;
                if(DEBUG){
                    System.out.println("Recieve illegal move responds " + resp.getResponse() + " with the following responses:");
                    for(int mResp : resp.getResponses())
                        System.out.println(mResp);
                }
                break;
            }
            
            case IND_WAIT: {
                
                break;
            }
            
            case IND_GAME_ENDED: {
                active = false;
                break;
            }
            }
            
            newMessage = null;
        }
    }

    private void updateFleetState(Fleet fleet, Cell destinationCell) {
        
        fleet.setLocation(destinationCell.getX() + destinationCell.getY()*map.getWidth());
        
    }

    private Quest createNewQuest() {
        
        // find nearest planet that is not part of a quest. create new  
        ArrayList<Fleet> fleet = findFleetsNotOnQuest();
        
        for(PlanetCell pd : getAvailPlanetsSortedByDistance(map.getCell(fleet.get(0).getLocation()), dataMap.getPlanets())){
            
            boolean isQuest = false;
            //Checks if the planet is part of a quest
            for(Quest q : Quests){
                if(((Conquest)q).getGoal().equal(pd))
                    isQuest = true;
            }
            
            if(!isQuest){
                Quest newQuest = new Conquest(pd, client.getActor().getPlayerID());
                newQuest.setPriorities(getNextPriorities());
                Quests.add(newQuest);
                if(DEBUG){
                    System.out.println("Created new quest for " + ((Conquest)newQuest).getGoal().getX() + " : "+ ((Conquest)newQuest).getGoal().getY());
                }
                
                return newQuest;
            }
        }
        return null;
        
    }

    private int getNextPriorities() {
        return Quests.size();
    }
    
    private boolean assignFleetToQuest(Quest quest,WormHoleData[] whd) {
        
        //First find nearest fleet
        ArrayList<Fleet> fleet = findFleetsNotOnQuest();
        
        if(fleet.isEmpty())
            return false;
        int nearestFleet = 1000;
        int nearestFleetIndex = 0;
        for(int i = 0; i < fleet.size(); i++){
            Cell[] path = getShortestAvailablePath(map.getCell(fleet.get(i).getLocation()), (Cell)((Conquest)quest).getGoal(), whd);
            if(path != null){
                int tempDistance = path.length;
                if(tempDistance < nearestFleet){
                    nearestFleet = tempDistance;
                    nearestFleetIndex = i;
                }
            }
        }
        if(fleet.get(nearestFleetIndex).getSize() < quest.getMissingShips())
            fleet.get(nearestFleetIndex).assignQuest(quest);
        else{
            splitFleet(fleet.get(nearestFleetIndex),quest.getMissingShips());
            fleet.get(nearestFleetIndex).assignQuest(quest);
        }
            
        
        if(DEBUG)
            System.out.println("Assigned ships to the quest for "+ ((Conquest)quest).getGoal().getX() + " : "+ ((Conquest)quest).getGoal().getY());
        
        return true;
        
    }

    private void splitFleet(Fleet fleet, int missingShips) {
        int newFleetSize = fleet.getSize() - missingShips;
        fleet.setSize(missingShips);
        aiFleetState.add(new Fleet(fleet.getLocation(),newFleetSize));
        
    }

    private ArrayList<Fleet> findFleetsNotOnQuest() {
        ArrayList<Fleet> result = new ArrayList<Fleet>();
        
        for(Fleet f : aiFleetState){
            if(!f.isOnQuest())
                result.add(f);
            
        }
        
        return result;
        
    }
    
    public ArrayList<Cell[]> continueConquest(Conquest q, WormHoleData[] whd){
        ArrayList<Cell[]> paths = new ArrayList<Cell[]>();
        
        for(Fleet f : q.getAssignedFleets()){
            if(f.getSize() > 0){
                Cell[] path = getShortestAvailablePath(map.getCell(f.getLocation()), q.getGoal(), whd);
                if(path == null)
                    q.failQuest();
                paths.add(path);
            }
            
        }
        
        return paths;
           
    }

    @SuppressWarnings("unused")
    private void updateStates(ArrayList<Fleet> server, ArrayList<Fleet> client) {
         
        ArrayList<Fleet> fleetsToAdd = new ArrayList<Fleet>();
        boolean[] clientCheck = new boolean[client.size()];
        // check that if fleets have been destroyed or new fleets are to be generated
        for(int i = 0; i < server.size(); i++){
            ArrayList<Fleet> fleetInstances = new ArrayList<Fleet>(); // if there are multiple fleets in the same location
            int serverFleet = server.get(i).getSize();
            int clientFleet = 0;
            int lowestPiroritizedQuest = 0;
            for(int j = 0; j < client.size(); j++){
                if(server.get(i).getLocation() == client.get(j).getLocation()){
                    clientCheck[j] = true;
                    clientFleet = clientFleet + client.get(j).getSize();
                    fleetInstances.add(client.get(j));
                    if(client.get(j).getQuest() != null){
                        if(client.get(j).getQuest().getPriorities() > lowestPiroritizedQuest)
                            lowestPiroritizedQuest = client.get(j).getQuest().getPriorities(); 
                    }
                }
            }
            if(fleetInstances.isEmpty())
                fleetsToAdd.add(new Fleet(server.get(i).getLocation(),server.get(i).getSize()));
            else if(clientFleet > 0 && clientFleet < serverFleet){
                int newFleetSize = serverFleet - clientFleet;
                    for(int j = 0; j < fleetInstances.size(); j++){
                        fleetInstances.get(j).setSize(newFleetSize);
                        break;
                    }

                
            } else if(clientFleet > 0 && clientFleet > serverFleet){
                int reducingShip =  clientFleet - serverFleet;
                while(reducingShip > 0){
                    for(int j = 0; j < fleetInstances.size(); j++){
                        if(fleetInstances.get(j).getQuest().getPriorities() == lowestPiroritizedQuest){
                            if(reducingShip >= fleetInstances.get(j).getSize()){
                                reducingShip = reducingShip - fleetInstances.get(j).getSize();
                                fleetInstances.get(j).setSize(0);
                            }else{
                                fleetInstances.get(j).setSize(fleetInstances.get(j).getSize() - reducingShip);
                                reducingShip = 0;
                            }
                        }
                    }
                    lowestPiroritizedQuest--; 

                }
            }
        }

        //destroys the fleet that are not in the server state
        for(int i = clientCheck.length-1; i >= 0; i--){
            Fleet f = client.get(i);
            if(f.getSize() == 0 || clientCheck[i] == false){
                f.removeFromQuest();
                client.remove(i);
            }
        }

        for(Fleet f : fleetsToAdd){
            client.add(f);
        }

    }

    private Cell[] getShortestAvailablePath(Cell start, Cell goal, WormHoleData[] wormholes) {
        
        if (shortestPaths[start.getX()+start.getY()*map.getWidth()][goal.getX()+goal.getY()*map.getWidth()] != null) {
            if (shortestPaths[start.getX()+start.getY()*map.getWidth()][goal.getX()+goal.getY()*map.getWidth()].getShortestPath() != null)
                if (shortestPaths[start.getX()+start.getY()*map.getWidth()][goal.getX()+goal.getY()*map.getWidth()].hasPlanetInPath())
                    shortestPaths[start.getX()+start.getY()*map.getWidth()][goal.getX()+goal.getY()*map.getWidth()] = new Path(mapCalc.Astar(start, goal, map.getWidth(), map.getHeight()), getPlayerID());

        } else {
            shortestPaths[start.getX()+start.getY()*map.getWidth()][goal.getX()+goal.getY()*map.getWidth()] = new Path(mapCalc.Astar(start, goal, map.getWidth(), map.getHeight()),getPlayerID());

            for (int j = 0; j < wormholes.length; j++) {
                if (shortestPaths[wormholes[j].getDestination()][goal.getX()+goal.getY()*map.getWidth()] == null)
                    shortestPaths[wormholes[j].getDestination()][goal.getX()+goal.getY()*map.getWidth()] = new Path(mapCalc.Astar(map.getCell(wormholes[j].getDestination() % map.getWidth(), wormholes[j].getDestination() / map.getWidth()),
                            map.getCell(goal.getX(), goal.getY()) ,map.getWidth(),map.getHeight()),getPlayerID());
                if (shortestPaths[start.getX()+start.getY()*map.getWidth()][wormholes[j].getLocation()] == null)
                    shortestPaths[start.getX()+start.getY()*map.getWidth()][wormholes[j].getLocation()] = new Path(mapCalc.Astar(start, map.getCell(wormholes[j].getLocation() % map.getWidth()
                            , wormholes[j].getLocation() / map.getWidth()) ,map.getWidth(),map.getHeight()),getPlayerID());
                if (shortestPaths[start.getX()+start.getY()*map.getWidth()][wormholes[j].getLocation()].getShortestPath() != null && shortestPaths[wormholes[j].getDestination()][goal.getX()+goal.getY()*map.getWidth()].getShortestPath() != null)
                    if (shortestPaths[start.getX()+start.getY()*map.getWidth()][goal.getX()+goal.getY()*map.getWidth()].getDistance() > (shortestPaths[wormholes[j].getDestination()][goal.getX()+goal.getY()*map.getWidth()].getDistance()
                            + shortestPaths[start.getX()+start.getY()*map.getWidth()][wormholes[j].getLocation()].getDistance() + 1))
                        shortestPaths[start.getX()+start.getY()*map.getWidth()][goal.getX()+goal.getY()*map.getWidth()] = new Path(shortestPaths[start.getX()+start.getY()*map.getWidth()][wormholes[j].getLocation()],shortestPaths[wormholes[j].getDestination()][goal.getX()+goal.getY()*map.getWidth()],getPlayerID());
                    

            }
        }
        return shortestPaths[start.getX()+start.getY()*map.getWidth()][goal.getX()+goal.getY()*map.getWidth()].getShortestPath();
    }

    private ArrayList<PlanetCell> getAvailPlanetsSortedByDistance(Cell c, PlanetData[] planetDatas){
        
        int randIndex;
        if (random == null)
            random = new Random(2520*c.getPlayerID()*c.getX()*c.getY()*c.getPlayerID()+144000*c.getX()*c.getY());
        
        ArrayList<Double> distances = new ArrayList<Double>();
        ArrayList<PlanetCell> result = new ArrayList<PlanetCell>();

        for (int i = 0; i < planetDatas.length; i++) {
            if (!planetDatas[i].isClaimed()) {
                
                randIndex = 0;
                double dist = mapCalc.wrapDistance(c, map.getCell(planetDatas[i].getLocation() % map.getWidth(), 
                        planetDatas[i].getLocation() / map.getWidth()), map.getWidth(), map.getHeight());
                
                if (distances.isEmpty()) {
                    distances.add(dist);
                    result.add((PlanetCell)map.getCell(planetDatas[i].getLocation() % map.getWidth(), 
                            planetDatas[i].getLocation() / map.getWidth()));
                    
                } else
                    for (int j = 0; j < distances.size(); j++) {
                        if (distances.get(j) == dist) {
                            randIndex++;
                        } else if (distances.get(j) > dist) {
                            if (randIndex > 0)
                                randIndex = j-random.nextInt(randIndex);
                            else
                                randIndex = j;

                            distances.add(randIndex, dist);
                            result.add(randIndex,(PlanetCell)map.getCell(planetDatas[i].getLocation() % map.getWidth(), 
                                    planetDatas[i].getLocation() / map.getWidth()));
                            break;
                        
                        } 
                        
                        if (j == distances.size()-1) {
                            if (randIndex > 0) {
                                randIndex = random.nextInt(randIndex);
                            }
                            if (randIndex == 0) {
                                distances.add(dist);
                                result.add((PlanetCell)map.getCell(planetDatas[i].getLocation() % map.getWidth(), 
                                        planetDatas[i].getLocation() / map.getWidth()));
                                break;
                            } else {
                                distances.add((j-randIndex)+1, dist);
                                result.add((j-randIndex)+1,(PlanetCell)map.getCell(planetDatas[i].getLocation() % map.getWidth(), 
                                        planetDatas[i].getLocation() / map.getWidth()));
                                break;
                            }

                        }
                    }
            }
        }
        
        if (DEBUG) {
            System.out.println("");
            for (int i = 0; i < distances.size(); i++) {
                System.out.print("["+result.get(i).getX()+";"+result.get(i).getY()+"], ");
            }
        }
        
        return result;
        
    }
    
    public void addMessage(Message msg){
        try {
            bQueue.put(msg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    
}