package client.actors.AIFunctions;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Stack;

import cell.Cell;
import cell.PlanetCell;

public class mapCalc {
    
    private static int epsilon = 0; //bounded relaxtion. used in A*
    
    /**
     * Using A* algorithm with implemented with bounded relaxation that is activated with setting espilon > 0. With bounded relaxation
     * A* returns a sub-optimal solution but in return is faster
     * 
     * @param start cell of the path
     * @param goal cell of the path
     * @return a list of cells the order from the start cell to the goal cell
     */
    
    static public Cell[] Astar(Cell start, Cell goal, int width, int height){
        
        ArrayList<Cell> visitedNote = new ArrayList<Cell>();
        PriorityQueue<Cell> frontier = new PriorityQueue<Cell>();
        
        start.setParent(null);
        start.setgScore(0);
        start.setfScore(start.getgScore() + (1 + epsilon)* heuristic(start, goal, width, height));

        frontier.add(start);

        while (!frontier.isEmpty()) {

            Cell current = frontier.remove();  

            if (current.equals(goal)) {
                //return array of the path
                return path(current);
            }

            visitedNote.add(current);

            Cell[] neighbours = current.getNeighbours();

            for (Cell c : neighbours) {
                
                //checks if the cell is a planet and if that the planet is not claimed by others
                boolean claimed = false;
                if(c instanceof PlanetCell)
                    claimed = ((((PlanetCell) c).getPlayerID() != start.getPlayerID()) && ((PlanetCell) c).isClaimed());
                
                if(c.getID() != 3 && !claimed) {
                    double tempScore = current.getgScore() + heuristic(current, c, width, height);

                    if (visitedNote.contains(c)) {

                        if (c.getgScore() <= tempScore) {
                            continue;
                        }
                    }

                    else if (!frontier.contains(c)) {

                        c.setParent(current);
                        c.setgScore(tempScore);
                        c.setfScore(c.getgScore() + (1 + epsilon) * heuristic(c, goal, width, height));
                        frontier.add(c);
                    }
                }
            }


        }

        return null;
        
    }
    
    static private Cell[] path(Cell goal) {
        
        Stack<Cell> tempList = new Stack<Cell>();
        Cell parent = new Cell();
        parent = goal;
        
        while (parent != null){
            
            tempList.push(parent);
            
            Cell t = parent;
            
            parent = t.getParent();
            t.setParent(null);
            
        }
        if(tempList.size() > 1)
            tempList.pop();
        Cell[] result = new Cell[tempList.size()];
        int index = 0;
        while (!tempList.isEmpty()) {
            result[index] = tempList.pop();
            index++;
        }
        
        return result;
    }
    
    static private double heuristic(Cell c1, Cell c2, int width, int height) {

        return wrapDistance(c1, c2, width, height);
    }
       
       
    static private double axToCubeX(int x, int y){
             
        return x-(y-(y&1))/2;
    }
    
    static private double axToCubeY(int x, int y){
        return y;
    }
    
    static private double axToCubeZ(double x, double y){
        return -x-y;
    }
    
    static public double directDistance(int x1, int y1, int x2, int y2){
        
        double newX1 = x1+0.5*(y1&1);
        double newX2 = x2+0.5*(y2&1);
        
        return Math.sqrt(Math.pow(Math.abs(newX1 + newX2),2) + Math.pow(Math.abs(y1 + y2),2));
        
        
    }
    
    static public double manhattanDistance(int x1, int y1, int x2, int y2){
        
        double cubeX1 = axToCubeX(x1,y1);
        double cubeY1 = axToCubeY(x1,y1);
        double cubeZ1 = axToCubeZ(cubeX1,cubeY1);
        
        double cubeX2 = axToCubeX(x2,y2);
        double cubeY2 = axToCubeY(x2,y2);
        double cubeZ2 = axToCubeZ(cubeX2,cubeY2);

        return (Math.abs(cubeX1 - cubeX2) + Math.abs(cubeY1 - cubeY2) + Math.abs(cubeZ1 - cubeZ2)) / 2L;

       }
       
       /** calculates the distance in a map with wrapping. calculates for 4 different direction. For the path that does not use
        * wrapping, the path that crosses wrap along the x-axes, the y-axes or both.
        * 
        * @param c1 is the start cell
        * @param c2 is the end cell
        * @param width  of the map
        * @param height of the map 
        * @return the distance in double
        */
       
    static  public double wrapDistance(Cell c1, Cell c2, int width, int height){
           
           double d1 = manhattanDistance(c1.getX(),c1.getY(),c2.getX(),c2.getY());
           double d2 = manhattanDistance(c1.getX() + width,c1.getY(),c2.getX(),c2.getY());
           double d3 = manhattanDistance(c1.getX(),c1.getY(),c2.getX(),c2.getY() + height);
           double d4 = manhattanDistance(c1.getX() + width,c1.getY(),c2.getX(),c2.getY() + height);
           
           return Math.min(Math.min(d1 , d2), Math.min(d3,d4));

       }
       
    static public double distanceCell(Cell c1, Cell c2){
           return manhattanDistance(c1.getX(),c1.getY(),c2.getX(),c2.getY());
       }
}
