package message.game;

import java.io.Serializable;

import message.Constant;
import message.MessagePlayer;
import message.data.GameData;

public class GameStateInd extends MessagePlayer implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -8594169994379922362L;
    
    int turnNumber;
	private GameData gameData;
	
	public GameStateInd(int clientId, int gameId, int playerId, int turnNumber, GameData gameData) {
		
		super(Constant.IND_GAME_STATE.getId(), clientId, gameId, playerId);
		this.gameData = gameData;
		this.turnNumber = turnNumber;
	}

	public int getTurnNumber() {
	    return this.turnNumber;
	}
	
	public GameData getGameData() {
		return this.gameData;
	}
	
	public void setGameData(GameData gameData) {
		this.gameData = gameData;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "GameStateInd [turnNumber= " + turnNumber + "\n" + "gameData=" + gameData.toString() + "]";
	}
}
