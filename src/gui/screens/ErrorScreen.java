package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;

import message.Constant;

public class ErrorScreen extends GuiMenu {
	
    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
	private static Constant error;
	
	@SuppressWarnings("static-access")
    public ErrorScreen(Constant resp) {
		super();
		
		this.error = resp;
		addButton(new Button(ButtonID.CANCEL_ID, 6, (screenW-128)/2, screenH/4+120));
	}
	
	@Override
    public void render(Screen screen) {

        screen.clear(0);
        Font.draw(screen, error.getMessage(), (screenW-Font.getStringWidth(error.getMessage()))/2, screenH/4);
        
        super.render(screen);
    }
	

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}
