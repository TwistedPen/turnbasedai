package message;

import java.io.Serializable;

public class MessageGame extends MessageClient implements Serializable {
	
	/**
     * 
     */
    private static final long serialVersionUID = -6781289381876080840L;
    private final int gameId;
	
	public MessageGame(int messageId, int clientId, int gameId) {
		super(messageId, clientId);
		this.gameId = gameId;
	}

	public int getGameId() {
		return gameId;
	}
	
    public boolean verify(int clientId) {
        return super.verify(clientId);
    }
    
    public boolean verify(int clientId, int gameId) {
        return (verify(clientId) && (gameId == this.gameId));
    }
    
	public boolean verify(int clientId, int gameId, int playerId) {
	   return verify(clientId, gameId);
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "[gameId=" + gameId + "]";
	}
}

