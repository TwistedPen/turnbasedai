package hexagonLayoutManager;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;

public class HexagonLayout implements LayoutManager {

	/*
	 * 
	 */
	public static final float edgeFactor = 0.25f;
	private int rows;
	private int columns;
	private final int spacing;
	private final boolean indentFirst;
	private final boolean verticeNorth;

	private Dimension preferredLayoutSize;

	public HexagonLayout(int rows, int columns, int spacing,
			boolean indentFirst, boolean verticeNorth) {

		if (rows <= 0 || columns <= 0) {
			throw (new IllegalArgumentException(
					"The number of rows and columns must be greater than 0"));
		}

		this.spacing = spacing;
		preferredLayoutSize = new Dimension(1000, 700);
		this.rows = rows;
		this.columns = columns;
		this.indentFirst = indentFirst;
		this.verticeNorth = verticeNorth;
	}

	public void layoutContainer(Container parent) {

		int componentCount = parent.getComponentCount();

		float parentMaxX = ((float) parent.getWidth()) - 1.0f;
		float parentMaxY = ((float) parent.getHeight()) - 1.0f;

		int boxMaxX = 0;
		int boxMaxY = 0;

		/* Divide the entire layout into boxes */
		if (verticeNorth) {
			boxMaxX = (int) (parentMaxX / (((float) columns) + 0.5f));
			boxMaxY = (int) (parentMaxY / (((float) (rows - 1)) * (1.0f - edgeFactor) + 1.0));
		} else {
			boxMaxX = (int) (parentMaxX / (((float) (columns - 1)) * (1.0f - edgeFactor) + 1.0));
			boxMaxY = (int) (parentMaxY / (((float) rows) + 0.5f));
		}

		int componentWidth = (boxMaxX + 1) - (spacing * 2);

		if (componentWidth < 0) {
			componentWidth = 0;
		} else if ((!verticeNorth) && ((componentWidth % 2) == 0)) {
			componentWidth -= 1;
		}

		int componentHeight = (boxMaxY + 1) - (spacing * 2);

		if (componentHeight < 0) {
			componentHeight = 0;
		} else if (verticeNorth && ((componentHeight % 2) == 0)) {
			componentHeight -= 1;
		}

		int currentX = spacing;
		int currentY = spacing;

		int currentComponent = 0;
		Component[] components = parent.getComponents();
		boolean indentCurrent = indentFirst ? true : false;

		int xDelta = 0;
		int yDelta = 0;

		if (verticeNorth) {
			xDelta = boxMaxX;
			yDelta = ((int) (boxMaxY - ((((float) boxMaxY) * edgeFactor))));
		} else {
			xDelta = ((int) (boxMaxX - ((((float) boxMaxX) * edgeFactor))));
			yDelta = boxMaxY;
		}

		if (xDelta < 0) {
			xDelta = 0;
		}

		if (yDelta < 0) {
			yDelta = 0;
		}

		if (verticeNorth) {
			
			for (int currentRow = 0; currentRow < rows; currentRow++) {

				currentX = spacing;
				
				if (indentCurrent) {
					currentX += ((int) (((float) boxMaxX) * 0.5f));
				}

				for (int currentColumn = 0; currentColumn < columns; currentColumn++) {

					if (currentComponent <= componentCount) {
						components[currentComponent].setBounds(currentX, currentY, componentWidth, componentHeight);
						currentComponent++;
					}

					currentX += xDelta;
				}
				indentCurrent = !indentCurrent;
				currentY += yDelta;
			}
		} else {

			for (int currentColumn = 0; currentColumn < columns; currentColumn++) {

				currentY = spacing;
				
				if (indentCurrent) {
					currentY += ((int) (((float) boxMaxY) * 0.5f));
				} 

				for (int currentRow = 0; currentRow < rows; currentRow++) {

					currentComponent = currentColumn + (currentRow * columns);

					if (currentComponent <= componentCount) {
						components[currentComponent].setBounds(currentX,
								currentY, componentWidth, componentHeight);
					}

					currentY += yDelta;
				}
				indentCurrent = !indentCurrent;
				currentX += xDelta;
			}
		}
	}

	public Dimension preferredLayoutSize(Container parent) {
		return preferredLayoutSize;
	}

	public void removeLayoutComponent(Component comp) {
	}

	public void addLayoutComponent(String name, Component comp) {
	}

	public int getRows() {
		return columns;
	}

	public int getColumns() {
		return columns;
	}

	public Dimension minimumLayoutSize(Container arg0) {
		return null;
	}
}
