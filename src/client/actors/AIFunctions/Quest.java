package client.actors.AIFunctions;

import java.util.ArrayList;
import java.util.Iterator;

import client.Map;

public abstract class Quest implements Comparable<Object>{

    
    protected boolean succes = false;
    protected boolean failure = false;
    protected int requiredShips = 0;
    protected int playerID = 0;
    protected int priorities = 0;
    protected ArrayList<Fleet> fleets = new ArrayList<Fleet>();
    
    public boolean isFailure() {
        return failure;
    }
    
    public boolean isSucces() {
        return succes;
    }
    
    public int getRequiredShips() {
        return requiredShips;
    }
    
    public int getPriorities() {
        return priorities;
    }
    
    public void setPriorities(int priorities) {
        this.priorities = priorities;
    }
    
    public void setRequiredShips(int requiredShips) {
        this.requiredShips = requiredShips;
    }
    
    public void setFleets(ArrayList<Fleet> fleets) {
        this.fleets = fleets;
    }
    
    public ArrayList<Fleet> getAssignedFleets(){
        return fleets;
    }
    
    public void addFleet(Fleet f){
        fleets.add(f);
    }
    
    public Fleet getFleetAt(int location){
    
        for(Fleet f : fleets){
            if(location == f.getLocation());
            return f;
        }
        
        return null;
    }
    
    public boolean removeFleetAt(int location){
        
        int i = 0;
        for(Fleet f : fleets){
            if(location == f.getLocation())
                fleets.remove(i);
                
            i++;
        }
        
        return false;
    }
    
    public void removeFleet(Fleet f){
        fleets.remove(f);
    }
    
    public void update(Map map){
        
        Iterator<Fleet> IteF = fleets.iterator();
        while(IteF.hasNext()){
            Fleet f = IteF.next();
            if(f.getSize() == 0)
                IteF.remove();
        }
        
    }
    
    public int getMissingShips() {

        int assignedShips = 0;
        for(Fleet f : fleets){
            assignedShips = assignedShips + f.getSize();
        }
        if(assignedShips <= requiredShips)
            return requiredShips - assignedShips;
        
        return 0;
    }
    
    public boolean hasEnoughShips() {
        
        int assignedShips = 0;
        for(Fleet f : fleets){
            assignedShips = assignedShips + f.getSize();
        }
        if(assignedShips >= requiredShips)
            return true;
        return false;
    }

    public void failQuest() {
        failure = true;
    }
    
}

