package message.application;

import java.io.Serializable;

import message.Constant;
import message.Message;

public class ErrorInd extends Message implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7185776902332278092L;
	
	private int response;
	private Message inResponseTo;

	public ErrorInd(int response, Message inResponseTo) {
		super(Constant.IND_ERROR.getId());
		this.response = response;
		this.inResponseTo = inResponseTo;
	}

	public int getResponse() {
		return response;
	}
	
	public Message getInResponseTo() {
		return inResponseTo;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "ErrorInd [response=" + response + ", inResponseTo="
				+ inResponseTo.toString() + "]";
	}
}
