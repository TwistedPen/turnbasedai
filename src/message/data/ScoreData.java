package message.data;

import java.io.Serializable;

public class ScoreData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6018231465639986023L;
	
	private int playerId;
	private int playerScore;
	
	public ScoreData(int playerId, int playerScore) {
		this.playerId = playerId;
		this.playerScore = playerScore;
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public int getPlayerScore() {
		return playerScore;
	}

	public void setPlayerScore(int playerScore) {
		this.playerScore = playerScore;
	}

	@Override
	public String toString() {
		return "ScoreData [playerId=" + playerId + ", playerScore=" + playerScore + "]";
	}
}
