package message.game;

import java.io.Serializable;

import message.Constant;
import message.MessagePlayer;
import message.data.FleetsUpdateData;
import message.data.PlanetUpdateData;

public class GameStateUpdateInd extends MessagePlayer implements Serializable {
		
	/**
     * 
     */
    private static final long serialVersionUID = 2860747548429609490L;
    
    private int turnNumber;
	private FleetsUpdateData[] fleetsUpdates;
	private PlanetUpdateData[] planetUpdates;
	
	public GameStateUpdateInd(int clientId, int gameId, int playerId, int turnNumber, FleetsUpdateData[] fleetsUpdates, PlanetUpdateData[] planetUpdates) {
		super(Constant.IND_GAME_STATE_UPDATE.getId(), clientId, gameId, playerId);
		this.fleetsUpdates = fleetsUpdates;
		this.planetUpdates = planetUpdates;
	}

	public int getTurnNumber() {
	    return this.turnNumber;
	}
	
	public FleetsUpdateData[] getFleetsUpdates() {
		return fleetsUpdates;
	}

	public void setFleetsUpdates(FleetsUpdateData[] fleetsUpdates) {
		this.fleetsUpdates = fleetsUpdates;
	}

	public PlanetUpdateData[] getPlanetUpdates() {
		return planetUpdates;
	}

	public void setPlanetsUpdates(PlanetUpdateData[] planetUpdates) {
		this.planetUpdates = planetUpdates;
	}

	@Override
	public String toString() {
		
		String fleetsStr = "";
		
		for (FleetsUpdateData data : fleetsUpdates) {
			fleetsStr += data.toString() + "\n";
		}
		
		String planetsStr = "";
		
		for (PlanetUpdateData data : planetUpdates) {
			planetsStr += data.toString() + "\n";
		}
		
		return super.toString() + "\n" + "GameStateUpdateInd [turnNumber= " + turnNumber + " fleetsUpdates="
				+ "\n" + fleetsStr + ", " + "\n" + "planetsUpdates=" + "\n"
				+ planetsStr + "]";
	}
}