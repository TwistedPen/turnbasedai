package message;

import java.io.Serializable;

public class Message implements Serializable {
	
	/* 3 types of messages:
	 * REQ - always waits for a RESP
	 * RESP - always follows a REQ
	 * IND - never waits for a RESP
	 */
	
    /* see: http://www.ibm.com/developerworks/java/library/j-5things1/index.html make note in report re.: security implications of using serialization */
	
	/* NOTE: specifying the serialVersionUID avoids the risk of different compilers generating it differently */
	
	/**
     * 
     */
    private static final long serialVersionUID = 8970242692782595128L;
    private final int messageId;
	
	public Message(int messageId) {
		this.messageId = messageId;
	}

	public int getMessageId() {
		return messageId;
	}

	public boolean verify(int clientId) {
	    return true;
	}
	
	public boolean verify(int clientId, int gameId) {
	    return true;
	}
	   
	public boolean verify(int clientId, int gameId, int playerId) {
	    return true;
	}
	
	@Override
	public String toString() {
		return "Message\n[messageId=" + messageId + "]";
	}
}
