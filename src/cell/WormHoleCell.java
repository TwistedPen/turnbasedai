package cell;

import gui.sprites.Art;
import gui.sprites.Screen;

public class WormHoleCell extends Cell {

	private Cell connectedTo = this;

	public WormHoleCell(){
		super();
		this.setID(2);
	}
	
	public WormHoleCell(WormHoleCell cell) {
	    super();
	    this.setID(cell.getID());
	    this.setConnection(new Cell(cell.getConnectedTo()));
	    this.setfScore(cell.getfScore());
	    this.setgScore(cell.getgScore());
	    this.setID(cell.getID());
	    this.setPlayerID(cell.getPlayerID());
	    this.setX(cell.getX());
	    this.setY(cell.getY());
	    this.setXpos(cell.getXpos());
	    this.setYpos(cell.getYpos());
	}
	
	public void setConnection(Cell whc){
		this.connectedTo = whc;
	}
	
	public Cell getConnectedTo() {
		return connectedTo;
	}

	@Override
	public void render(Screen screen) {
		super.render(screen);
	    
		if(this.isVisible()){
			
			screen.blit(Art.cellTile[0][0], this.getXpos() , this.getYpos());
			screen.blit(Art.wormHole, this.getXpos() , this.getYpos());
			
			if(getShip() == 1)
                screen.blit(Art.ships[0][this.getPlayerID()], getXpos(), getYpos());
            else if(getShip() == 2)
                screen.blit(Art.ships[1][this.getPlayerID()], getXpos(), getYpos());
            else if(getShip() == 3)
                screen.blit(Art.ships[2][this.getPlayerID()], getXpos(), getYpos());
            else if(getShip() > 3)
                screen.blit(Art.ships[3][this.getPlayerID()], getXpos(), getYpos());
			
		} else{
			
			screen.blit(Art.cellTile[1][0], this.getXpos() , this.getYpos());
			screen.blit(Art.wormHole, this.getXpos() , this.getYpos());
			
			
		}
	}
		

}
