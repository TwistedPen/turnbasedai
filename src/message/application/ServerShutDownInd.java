package message.application;

import java.io.Serializable;

import message.Constant;
import message.Message;

/* Sent if and when the server is closed down */

public class ServerShutDownInd extends Message implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1635138248879115946L;

	public ServerShutDownInd() {
		super(Constant.IND_SERVER_SHUTDOWN.getId());
	}
}
