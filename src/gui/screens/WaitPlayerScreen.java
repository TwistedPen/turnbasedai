package gui.screens;

import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;

import message.Constant;

import client.MouseButtons;

public class WaitPlayerScreen extends GuiMenu {

    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
	private int waitType = 0;
	private int playersWaiting = 0;
	
    public WaitPlayerScreen() {
        super();
        
        this.waitType = Constant.WAIT_FOR_PLAYER_TO_CHOOSE_MAP.getId();
        
    }
    
    public WaitPlayerScreen(int i) {
        super();
        
        this.waitType = Constant.WAIT_FOR_PLAYERS.getId();
        this.playersWaiting = i;
        
    }
   
    @Override
    public void update(MouseButtons mousebuttons) {
    	
    }
    
    @Override
    public void render(Screen screen) {
        super.render(screen);
        screen.clear(0);
        String s = "";
        if(waitType == Constant.WAIT_FOR_PLAYERS.getId()){
            s = "Waiting for " + playersWaiting + " players to join";
            
            if (playersWaiting == 1000)
                Font.draw(screen, "Waiting for players to join", (screenW-Font.getStringWidth("Waiting for players to join"))/2, screenH/4);
            else
                Font.draw(screen, s,(screenW-Font.getStringWidth(s))/2, screenH/4);
        } else if (waitType == Constant.WAIT_FOR_PLAYER_TO_CHOOSE_MAP.getId()){
            
            s = Constant.WAIT_FOR_PLAYER_TO_CHOOSE_MAP.getMessage();
            Font.draw(screen, s,(screenW-Font.getStringWidth(s))/2, screenH/4);
            
        }
        
    }

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}
