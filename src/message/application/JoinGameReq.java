package message.application;

import java.io.Serializable;

import message.Constant;
import message.MessageGame;

public class JoinGameReq extends MessageGame implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2466775862970500844L;
	/* See Constant:
    PLAYER_ACTIVE("ACTIVE") or
    PLAYER_SPECTATOR("SPECTATOR") */
	private int playerType;
	
	public JoinGameReq(int clientId, int gameId, int playerType) {
		super(Constant.REQ_JOIN_GAME.getId(), clientId, gameId);
		this.playerType = playerType;
	}

	public int getPlayerType() {
		return playerType;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "JoinGameReq" + "\n" + "[playerType=" + playerType + "]";
	}
}
