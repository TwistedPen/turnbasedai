package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Gui;
import gui.sprites.Art;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;

public class StartMenuScreen extends GuiMenu {
    
    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
    
	public StartMenuScreen() {
        super();
        
        addButton(new Button(ButtonID.JOIN_SERVER_ID, 3, (screenW - 128) / 2, ((screenH * 2) / 4) + 40));
        addButton(new Button(ButtonID.EXIT_GAME_ID, 1, (screenW - 128) / 2, ((screenH * 2) / 4) + 80));
	}
	
	public void render(Screen screen){
		
		screen.clear(0);
		
		screen.blit(Art.bg_game, 0, 0);
		
		super.render(screen);
		
	}
	
	 @Override
	 public void buttonPressed(Button button) {
	 }


	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}
