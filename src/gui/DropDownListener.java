package gui;

public interface DropDownListener {

    public void drop(DropDown dropDown);
}
