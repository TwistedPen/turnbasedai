package hexagonLayoutManager;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class HexTest {

	static boolean verticeNorth = false;
	static JFrame f = new JFrame();
	static final ArrayList<HexagonButton> buttons = new ArrayList<HexagonButton>();
	static JPanel j = new JPanel();

	public static void createAndShowGui() {
	    
	    if (SwingUtilities.isEventDispatchThread()) {
	        System.out.println("On the EventQueue dispatch thread");
	    }
	
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		j.setLayout(new HexagonLayout(4, 6, 2, false, verticeNorth));
		f.add(j);
		
		ActionListener listener = new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			    HexagonButton a = (HexagonButton) e.getSource();
			    a.setBackground(Color.CYAN);
			}
		};
		
		
		
		for (int i = 0; i < 24; i++) {

		    HexagonButton b = new HexagonButton(i, verticeNorth, HexagonLayout.edgeFactor);
		    buttons.add(b);
		    b.addActionListener(listener);
		    j.add(b);
		}
		
		/*
		Causes this Window to be sized to fit the preferred size and layouts of its subcomponents. 
		The resulting width and height of the window are automatically enlarged if 
		either of dimensions is less than the minimum size as specified by the previous call to the setMinimumSize method. 
		*/
	    f.pack();
		f.setVisible(true);
	    
}
	
	
    public static void main(String[] args) {
    	
        //HexTest.createAndShowGui();
    	SwingUtilities.invokeLater(new Runnable() {
    	    public void run() {
    	        HexTest.createAndShowGui();
    	    }
    	});
    }
    }
