package server.game;

import server.client.Client;
import message.Message;
import message.game.ChooseHomePlanetResp;
import message.game.ChooseMapResp;
import message.game.LeaveGameInd;
import message.game.MoveReq;

/* One instance per player in the game 
 * Communicates with the client and the game */
public class Player {
	
    private final Game game;
	private final Client client;
	private final int playerId;
	private int playerType;  /* Spectator, active */
	private PlayerState state;
    private int joinedAsNo;
	
	public Player(Game game, Client client, int playerId, int playerType, int joinedAsNo) {
	    this.game = game;
	    this.client = client;
		this.playerId = playerId;
		this.playerType = playerType;
		this.state = new Waiting();
		this.joinedAsNo = joinedAsNo;
	}
	
	public int getPlayerType() {
	    return this.playerType;
	}
	
	public int getClientId() {
	    return this.client.getId();
	}
	
	public int getGameId() {
	    return this.game.getId();
	}
	
	public int getPlayerId() {
	    return this.playerId;
	}
	
	public void registerWithClient() {
	    client.addPlayer(this);
	}
	
	public void deregisterFromClient() {
	    client.removePlayer(this);
	}
	
	public synchronized void handleChooseMapResp(ChooseMapResp resp) {
	    state.handleChooseMapResp(game, this, resp);
	}

    public synchronized void handleChooseHomePlanetResp(ChooseHomePlanetResp resp) {
        state.handleChooseHomePlanetResp(game, this, resp);
    }

    public synchronized void handleMoveReq(MoveReq req) {
        state.handleMoveReq(game, this, req);
    }

    public synchronized void handleLeaveGameInd(LeaveGameInd ind) {
        state.handleLeaveGameInd(game, this, ind);
    }
    
    public synchronized void sendMessage(Message message) {
        state.sendMessage(client, message);
    }
    
    public synchronized void setState(PlayerState newState) {
        state = newState;
    }
}