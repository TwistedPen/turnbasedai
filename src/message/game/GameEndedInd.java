package message.game;

import java.io.Serializable;

import message.Constant;
import message.MessageGame;
import message.data.ScoreData;

public class GameEndedInd extends MessageGame implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8359858880331812956L;
	
	private ScoreData[] scores;
	
	public GameEndedInd(int clientId, int gameId, ScoreData[] scores) {
		super(Constant.IND_GAME_ENDED.getId(), clientId, gameId);
		this.scores = scores;
	}

	public ScoreData[] getScoresData() {
		return scores;
	}
	
	@Override
	public String toString() {
		
		String scoreStr = "";
		
		for (ScoreData data : scores) {
			scoreStr += data.toString() + "\n";
		}
		
		return super.toString() + "\n" + "GameEndedInd " + "\n" + 
				"[scores=" + "\n" + scoreStr + "]";
	}
}
