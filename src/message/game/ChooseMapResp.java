package message.game;

import java.io.Serializable;

import message.Constant;
import message.MessagePlayer;

public class ChooseMapResp extends MessagePlayer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8530579325714198969L;
	
	private int mapNo;
	
	public ChooseMapResp(int clientId, int gameId, int playerId, int mapNo) {
		super(Constant.RESP_CHOOSE_MAP.getId(), clientId, gameId, playerId);
		this.mapNo = mapNo;
	}

	public int getMapNo() {
		return mapNo;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "ChooseMapResp [mapNo=" + mapNo + "]";
	}
}
