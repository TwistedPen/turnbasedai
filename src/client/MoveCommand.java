package client;

public class MoveCommand {
	
	private int currentLocationX;
	private int currentLocationY;
	private int ships;
	private int destinationX;
	private int destinationY;
	private int action;
	
	public MoveCommand(int cLX, int cLY, int ships, int dX, int dY){
        this.currentLocationX = cLX;
        this.currentLocationY = cLY;
        this.ships = ships;
        this.destinationX = dX;
        this.destinationY = dY;
    }
    
    public MoveCommand(int cLX, int cLY, int ships,int dX, int dY, int action){
        this.currentLocationX = cLX;
        this.currentLocationY = cLY;
        this.ships = ships;
        this.destinationX = dX;
        this.destinationY = dY;
        this.action = action;
    }
    
    public MoveCommand(MoveCommand MC){
        this.currentLocationX = MC.getCurrentLocationX();
        this.currentLocationY = MC.getCurrentLocationY();
        this.ships = MC.getShips();
        this.destinationX = MC.getDestinationX();
        this.destinationY = MC.getDestinationY();
        this.action = MC.getAction();
    }
	
	public int getCurrentLocationX() {
        return currentLocationX;
    }

    public void setCurrentLocationX(int currentLocationX) {
        this.currentLocationX = currentLocationX;
    }

    public int getCurrentLocationY() {
        return currentLocationY;
    }

    public void setCurrentLocationY(int currentLocationY) {
        this.currentLocationY = currentLocationY;
    }

    public int getShips() {
        return ships;
    }

    public void setShips(int ships) {
        this.ships = ships;
    }

    public int getDestinationX() {
        return destinationX;
    }

    public void setDestinationX(int destinationX) {
        this.destinationX = destinationX;
    }

    public int getDestinationY() {
        return destinationY;
    }

    public void setDestinationY(int destinationY) {
        this.destinationY = destinationY;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

}
