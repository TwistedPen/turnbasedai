package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;

public class ChangePortScreen extends GuiMenu {

    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
    private Button changePortButton;
    public static String portNum = "";

    public ChangePortScreen() {
        super();

        changePortButton = addButton(new Button(ButtonID.PERFORM_CHANGE_PORT_ID, 6, (screenW - 128)/2, screenH / 4 + 120));
        addButton(new Button(ButtonID.CANCEL_ID, 4, (screenW - 128)/2, screenH / 4 + 160));
    }

    @Override
    public void render(Screen screen) {

        screen.clear(0);
        Font.draw(screen, "Enter port number:", (screenW - Font.getStringWidth("Enter port number:"))/2, screenH / 4);
        Font.draw(screen, portNum + "-", (screenW - Font.getStringWidth(portNum + "-"))/2, screenH / 4 + 20);

        super.render(screen);
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_ENTER && portNum.length() > 0) {
            changePortButton.postClick();
        }
    }

    public void keyReleased(KeyEvent arg0) {
    }

    public void keyTyped(KeyEvent e) {

        if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE && portNum.length() > 0) {
            portNum = portNum.substring(0, portNum.length() - 1);
        } else if (Font.letters.indexOf(Character.toUpperCase(e.getKeyChar())) >= 0) {
            char key = e.getKeyChar();
            if((key == KeyEvent.VK_0 || key == KeyEvent.VK_1 || key == KeyEvent.VK_2 || key == KeyEvent.VK_3 || 
                    key == KeyEvent.VK_4 || key == KeyEvent.VK_5 || key == KeyEvent.VK_6 || key == KeyEvent.VK_7 || 
                    key == KeyEvent.VK_8 || key == KeyEvent.VK_9) && portNum.length() < 5)
                portNum += e.getKeyChar();
        }
    }

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub

    }

}
