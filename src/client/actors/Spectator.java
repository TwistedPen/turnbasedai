package client.actors;

import message.Constant;

public class Spectator extends Actor {
    
    private int actorID = Constant.PLAYER_SPECTATOR.getId();

    @Override
    public int getID() {
        return actorID;
    }

}
