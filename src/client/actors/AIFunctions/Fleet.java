package client.actors.AIFunctions;

public class Fleet {
    
    private int location = 0;
    private int size = 0;
    private Quest currentQuest = null;
    
    public Fleet(int location, int size){
        this.location = location;
        this.size = size;
        
    }
    
    public void assignQuest(Quest q){
        this.currentQuest = q;
        if(q != null)
            q.addFleet(this);
    }
    
    public Quest getQuest(){
        return currentQuest;
    }
    
    public boolean isOnQuest(){
        if(currentQuest != null)
            return true;
        return false;
    }
    
    public void removeFromQuest(){
        if(currentQuest != null)
            currentQuest.removeFleet(this);
    }
    
    public int getLocation() {
        return location;
    }
    
    public int getLocationX(int mapWidth) {
        return location % mapWidth;
    }
    
    public int getLocationY(int mapWidth) {
        return location / mapWidth;
    } 
    
    public void setLocation(int location) {
        this.location = location;
    }
    
    public int getSize() {
        return size;
    }
    
    public void setSize(int size) {
        this.size = size;
    }
    
    public boolean equal(Fleet f){
        if(this.location == f.location && this.size == f.size)
            return true;
        return false;
    }
}
