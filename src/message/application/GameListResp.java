package message.application;

import java.io.Serializable;

import message.Constant;
import message.Message;
import message.data.GameListData;

public class GameListResp extends Message implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2963368633947178162L;
    
    private GameListData[] games;

    public GameListResp(GameListData[] games)  {
        super(Constant.RESP_GAME_LIST.getId());
        this.games = games;
    }

    public GameListData[] getGames() {
        return games;
    }
    
    @Override
    public String toString() {
        
        String gameStr = "";
        
        for (GameListData data : games) {
            gameStr += data.toString() + "\n";
        }
        
        
        return super.toString() + "\n" + "GameListResp " + "\n" + 
                "[games=" + "\n" + gameStr + "]";
    }
}