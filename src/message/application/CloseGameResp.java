package message.application;

import java.io.Serializable;
import message.Constant;
import message.MessageGame;

public class CloseGameResp extends MessageGame implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1072042692338691165L;
	
	private int response;
	
	public CloseGameResp(int clientId, int gameId, int response) {
		super(Constant.RESP_CLOSE_GAME.getId(), clientId, gameId);
		this.response = response;
	}
	
	public int getResponse() {
		return response;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\n" + "CloseGameResp [response=" + response
				+ "]";
	}
}
