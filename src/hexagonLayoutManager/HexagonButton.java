package hexagonLayoutManager;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.JButton;

public class HexagonButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1572406077380152822L;
	
	private final int id;
	private Polygon hexagonShape;
	private final boolean verticeNorth;
	private final float edgeFactor;

	public HexagonButton(int id, boolean verticeNorth, float edgeFactor) {
		this.id = id;
		this.verticeNorth = verticeNorth;
		this.edgeFactor = edgeFactor;
		this.setContentAreaFilled(false);
		hexagonShape = new Polygon();
	}

	/* http://mathworld.wolfram.com/Hexagon.html */
	private void calculateHexagon() {
		
		this.hexagonShape.reset();
		
		int maxX = super.getWidth() - 1;
		int maxY = super.getHeight() - 1;
		
		int centerX = maxX / 2;
		int centerY = maxY / 2;
		
		int boxToVerticeDist = 0;
		int verticeToBoxDist = 0;
		
		if (verticeNorth) {
			boxToVerticeDist = (int) (((float) maxY) * edgeFactor);

			if (boxToVerticeDist > centerY) {
				boxToVerticeDist = centerY;
			}

			verticeToBoxDist = maxY - boxToVerticeDist;

		} else {
			boxToVerticeDist = (int) (((float) maxX) * edgeFactor);

			if (boxToVerticeDist > centerX) {
				boxToVerticeDist = centerX;
			}

			verticeToBoxDist = maxX - boxToVerticeDist;
		}
		
		if (verticeToBoxDist < boxToVerticeDist) {
			verticeToBoxDist = boxToVerticeDist;
		}

		if (verticeNorth) {
			hexagonShape.addPoint(centerX, 0);
			hexagonShape.addPoint(maxX, boxToVerticeDist);

			hexagonShape.addPoint(maxX, verticeToBoxDist);
			hexagonShape.addPoint(centerX, maxY);
			
			hexagonShape.addPoint(0, verticeToBoxDist);
			hexagonShape.addPoint(0, boxToVerticeDist);
		} else {
			hexagonShape.addPoint(0, centerY);
			hexagonShape.addPoint(boxToVerticeDist, maxY);

			hexagonShape.addPoint(verticeToBoxDist, maxY);
			hexagonShape.addPoint(maxX, centerY);
			
			hexagonShape.addPoint(verticeToBoxDist, 0);
			hexagonShape.addPoint(boxToVerticeDist, 0);
		}
	}

	public int getId() {
		return this.id;
	}
	
	public boolean contains(Point p) {
		return hexagonShape.contains(p);
	}

	public boolean contains(int x, int y) {
		return hexagonShape.contains(x, y);
	}

	public void setSize(Dimension d) {
		super.setSize(d);
		calculateHexagon();
	}

	public void setSize(int w, int h) {
		super.setSize(w, h);
		calculateHexagon();
	}

	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		calculateHexagon();
	}

	public void setBounds(Rectangle r) {
		super.setBounds(r);
		calculateHexagon();
	}

	protected void processMouseEvent(MouseEvent e) {
		if (contains(e.getPoint())) {
			super.processMouseEvent(e);
		}
	}

	protected void paintComponent(Graphics g) {
		g.setColor(getBackground());
		g.drawPolygon(hexagonShape);
		g.fillPolygon(hexagonShape);
	}

	protected void paintBorder(Graphics g) {
		
	    g.setColor(Color.BLACK);
	    g.drawPolygon(hexagonShape.xpoints, hexagonShape.ypoints, hexagonShape.npoints); 
	}
}
