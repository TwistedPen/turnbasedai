package message.game;

import message.Constant;
import message.MessagePlayer;
import message.data.MoveData;

public class MoveReq extends MessagePlayer {
	
	/**
     * 
     */
    private static final long serialVersionUID = -941724561632926908L;
    
    private int turnNumber;
	private MoveData moves[];
	
	public MoveReq(int clientId, int gameId, int playerId, int turnNumber, MoveData moves[]) {
		super(Constant.REQ_MOVE.getId(), clientId, gameId, playerId);
		this.turnNumber = turnNumber;
		this.moves = moves;
	}

	public int getTurnNumber() {
		return turnNumber;
	}

	public void setMoves(MoveData[] moves) {
		this.moves = moves;
	}
	
	public MoveData[] getMoves() {
		return moves;
	}

	@Override
	public String toString() {
		
		String moveStr = "";
		
		for (MoveData data : moves) {
			moveStr += data.toString() + "\n";
		}
		
		return super.toString() + "MoveReq [turnNumber=" + turnNumber + ", " + "\n" + "Moves=" + "\n" + 
				moveStr + "\n" + "]";
	}
}
