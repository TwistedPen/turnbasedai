package gui;

import gui.sprites.Art;
import gui.sprites.Screen;


public class Font {
    public static String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ���   " + "0123456789-.!?/%$\\=*+,;:()&#\"'";

    public static int getStringWidth(String s) {
        return s.length() * 8;
    }

    private Font() {
    }

    public static void draw(Screen screen, String msg, int x, int y) {
    	
        msg = msg.toUpperCase();
        int length = msg.length();
        for (int i = 0; i < length; i++) {
            int c = letters.indexOf(msg.charAt(i));
            if (c < 0) continue;
            screen.blit(Art.font[c % 32][c / 32], x, y);
            x += 8;
        }
    }
}
