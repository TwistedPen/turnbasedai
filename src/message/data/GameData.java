package message.data;

import java.io.Serializable;
import java.util.Arrays;

public class GameData implements Serializable {
	
    /**
     * 
     */
    private static final long serialVersionUID = -8284839148701432496L;
    
    private int playerCount;
	private int mapWidth;
	private int mapHeight;
	private int chooseTimeout;
	private int turnTimeout;
	private int gameTimeout;

	private WormHoleData[] wormHoles;
	private int[] debrisLocations;
	private FleetsData[] fleets;
	private PlanetData[] planets;
	private int[] homePlanetLocations;
	private int[] emptyLocations;

	public GameData(int playerCount, int mapWidth, int mapHeight,
			int chooseTimeout, int turnTimeout, int gameTimeout,
			WormHoleData[] wormHoles, int[] debrisLocations,
			FleetsData[] fleets, PlanetData[] planets, int[] homePlanetLocations, int[] emptyLocations) {
		
		this.playerCount = playerCount;
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		this.chooseTimeout = chooseTimeout;
		this.turnTimeout = turnTimeout;
		this.gameTimeout = gameTimeout;
		this.wormHoles = wormHoles;
		this.debrisLocations = debrisLocations;
		this.fleets = fleets;
		this.planets = planets;
		this.homePlanetLocations = homePlanetLocations;
		this.emptyLocations = emptyLocations;
	}
	
	public GameData(GameData GD){
	    this.playerCount = GD.getPlayerCount();
        this.mapWidth = GD.getMapWidth();
        this.mapHeight = GD.getMapHeight();
        this.chooseTimeout = GD.chooseTimeout;
        this.turnTimeout = GD.turnTimeout;
        this.gameTimeout = GD.gameTimeout;
        
        if(GD.getWormHoles() != null){
            this.wormHoles = new WormHoleData[GD.getWormHoles().length];
            for(int i = 0; i < GD.getWormHoles().length; i++){
                this.wormHoles[i] = new WormHoleData(GD.getWormHoles()[i]);
            }
        } else 
            this.wormHoles = new WormHoleData[]{};
        
        this.debrisLocations = GD.debrisLocations;
        
        if(GD.getFleets() != null){
            this.fleets = new FleetsData[GD.getFleets().length];
            for(int i = 0; i < GD.getFleets().length; i++){
                this.fleets[i] = new FleetsData(GD.getFleets()[i]);
            }
        } else 
            this.fleets = new FleetsData[]{};
        
        if(GD.getPlanets() != null){
            this.planets = new PlanetData[GD.getPlanets().length];
            for(int i = 0; i < GD.getPlanets().length; i++){
                this.planets[i] = new PlanetData(GD.getPlanets()[i]);
            }
        }
        
        this.homePlanetLocations = GD.getHomePlanetLocations();
        this.emptyLocations = GD.getEmptyLocations();
	}
	
	public void setHomePlanetOwner(int location,int player) {
	    int k = 0;
	    int[] newHome = new int[homePlanetLocations.length-1];
	    for (int i = 0; i < homePlanetLocations.length; i++)
	        if (homePlanetLocations[i] == location) {
	            for (PlanetData planet : planets)
	                if (planet.getLocation() == location) {
	                    planet.setColonizedBy(player);
	                    break;
	                }
	        } else {
	            if (k == homePlanetLocations.length-1) {
	                /* Assign the last planet automatically due to error */
	                for (PlanetData planet : planets)
	                    if (planet.getLocation() == homePlanetLocations[k]) {
	                        planet.setColonizedBy(player);
	                        break;
	                    }
	            } else {
	                newHome[k] = homePlanetLocations[i];
	                k++;
	            }
	        }
	    homePlanetLocations = newHome;
	}
	
	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public int getMapWidth() {
		return mapWidth;
	}

	public void setMapWidth(int mapWidth) {
		this.mapWidth = mapWidth;
	}

	public int getMapHeight() {
		return mapHeight;
	}

	public void setMapHeight(int mapHeight) {
		this.mapHeight = mapHeight;
	}

	public int getChooseTimeout() {
		return chooseTimeout;
	}

	public void setChooseTimeout(int chooseTimeout) {
		this.chooseTimeout = chooseTimeout;
	}

	public int getTurnTimeout() {
		return turnTimeout;
	}

	public void setTurnTimeout(int turnTimeout) {
		this.turnTimeout = turnTimeout;
	}

	public int getGameTimeout() {
		return gameTimeout;
	}

	public void setGameTimeout(int gameTimeout) {
		this.gameTimeout = gameTimeout;
	}

	public WormHoleData[] getWormHoles() {
		return wormHoles;
	}

	public void setWormHoles(WormHoleData[] wormHoles) {
		this.wormHoles = wormHoles;
	}

	public int[] getDebrisLocations() {
		return debrisLocations;
	}

	public void setDebrisLocations(int[] debrisLocations) {
		this.debrisLocations = debrisLocations;
	}

	public FleetsData[] getFleets() {
		return fleets;
	}

	public void setFleets(FleetsData[] fleets) {
		this.fleets = fleets;
	}

	public PlanetData[] getPlanets() {
		return planets;
	}

	public void setPlanets(PlanetData[] planets) {
		this.planets = planets;
	}
	
	public int[] getHomePlanetLocations() {
		return homePlanetLocations;
	}

	public void setHomePlanetLocations(int[] homePlanetLocations) {
		this.homePlanetLocations = homePlanetLocations;
	}

	public int[] getEmptyLocations() {
		return emptyLocations;
	}

	public void setEmptyLocations(int[] emptyLocations) {
		this.emptyLocations = emptyLocations;
	}

	@Override
	public String toString() {
		
		String wormStr = "";

		for (WormHoleData data : wormHoles) {
			wormStr += data.toString() + "\n";
		}

		String fleetsStr = "";

		for (FleetsData data : fleets) {
			fleetsStr += data.toString() + "\n";
		}

		String planetsStr = "";

		for (PlanetData data : planets) {
			planetsStr += data.toString() + "\n";
		}
		
		return "GameData [playerCount=" + playerCount + ", mapWidth="
				+ mapWidth + ", mapHeight=" + mapHeight + ", chooseTimeout="
				+ chooseTimeout + ", turnTimeout=" + turnTimeout
				+ ", gameTimeout=" + gameTimeout + ", wormHoles="
				+ "\n" + wormStr + ", debrisLocations="
				+ Arrays.toString(debrisLocations) + ", fleets="
				+ "\n" + fleetsStr + ", planets="
				+ "\n" + planetsStr + "\n" + ", homePlanetLocations="
				+ Arrays.toString(homePlanetLocations) + ", emptyLocations="
				+ Arrays.toString(emptyLocations) + "]";
	}
}
