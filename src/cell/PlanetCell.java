package cell;

import gui.sprites.Art;
import gui.sprites.Screen;

public class PlanetCell extends Cell {
	
	private int claimedBy = 0;
	private int requirement = 0; //Ships required to colonize
	private int cost = 0; //cost to claim a planet
	private int production = 0; //number of ships deployed every x turn 
	private int productionRate = 0; // the amount of x turn before new ships are deployed
	private int vision = 0; //range of view
	private boolean isHomePlanet;
	
	
	public PlanetCell(int req, int c, int p, int pr, int v, boolean isHomePlanet){
		super();
		this.requirement = req;
		this.cost = c;
		this.production = p;
		this.productionRate = pr;
		this.vision = v;
		this.setHomePlanet(isHomePlanet);
		this.setID(1);
		
		
	}
	
	public PlanetCell(PlanetCell cell) {
	    super();
	    this.requirement = cell.getRequirement();
	    this.cost = cell.getCost();
	    this.production = cell.getProduction();
	    this.productionRate = cell.getProductionRate();
	    this.vision = cell.getVision();
	    this.setHomePlanet(cell.isHomePlanet());
	    this.setClaimedBy(cell.getClaimedBy());
	    this.setID(cell.getID());
	    this.setfScore(cell.getfScore());
	    this.setgScore(cell.getgScore());
	    this.setID(cell.getID());
	    this.setPlayerID(cell.getPlayerID());
	    this.setX(cell.getX());
	    this.setY(cell.getY());
	    this.setXpos(cell.getXpos());
	    this.setYpos(cell.getYpos());
	}

    public int getClaimedBy() {
        return claimedBy;
    }
    
    public boolean isClaimed(){
        if(claimedBy != 0)
            return true;
        return false;
    }

    public void setClaimedBy(int claimedBy) {
        this.claimedBy = claimedBy;
    }

    public int getRequirement() {
        return requirement;
    }

    public void setRequirement(int requirement) {
        this.requirement = requirement;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getProduction() {
        return production;
    }

    public void setProduction(int production) {
        this.production = production;
    }

    public int getProductionRate() {
        return productionRate;
    }

    public void setProductionRate(int productionRate) {
        this.productionRate = productionRate;
    }

    public int getVision() {
        return vision;
    }

    public void setVision(int vision) {
        this.vision = vision;
    }
	
    @Override
	public void render(Screen screen){
		super.render(screen);
		
		if(this.isVisible()){
		    
		    // draws the tile
			screen.blit(Art.cellTile[0][0], this.getXpos(), this.getYpos());
			
			//draws the planet
			screen.blit(Art.Planets[claimedBy == 0 ? 0 : 1][claimedBy], this.getXpos(), this.getYpos());
            
			//draws the ships
			if(getShip() == 1)
                screen.blit(Art.ships[0][getPlayerID()], getXpos(), getYpos());
            else if(getShip() == 2)
                screen.blit(Art.ships[1][getPlayerID()], getXpos(), getYpos());
            else if(getShip() == 3)
                screen.blit(Art.ships[2][getPlayerID()], getXpos(), getYpos());
            else if(getShip() > 3)
                screen.blit(Art.ships[3][getPlayerID()], getXpos(), getYpos());
			
		} else{

		    //draws the tile
		    screen.blit(Art.cellTile[1][0], this.getXpos(), this.getYpos());
		    
		    //draws the planet
		    screen.blit(Art.Planets[claimedBy == 0 ? 0 : 1][claimedBy], this.getXpos(), this.getYpos());
		}

	}

	public void renderHomePlanets(Screen screen){

	    screen.colorBlit(Art.cellTile[0][0], this.getXpos(), this.getYpos(), 0xff00C90D); // find the correct color
		if(claimedBy == 0)
			screen.blit(Art.Planets[0][6], this.getXpos(), this.getYpos());
		else
			screen.blit(Art.Planets[1][6], this.getXpos(), this.getYpos());
	
}

    public boolean isHomePlanet() {
        return isHomePlanet;
    }

    public void setHomePlanet(boolean isHomePlanet) {
        this.isHomePlanet = isHomePlanet;
    }

    public boolean equal(PlanetCell planet){
        if(this.getX() == planet.getX() && this.getY() == planet.getY())
            return true;
        return false;
    }

}
