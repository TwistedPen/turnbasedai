package message.game;

import java.io.Serializable;
import message.Constant;
import message.MessagePlayer;

public class WaitInd extends MessagePlayer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7176339829407893938L;

	public WaitInd(int clientId, int gameId, int playerId) {
		super(Constant.IND_WAIT.getId(), clientId, gameId, playerId);
	}
}
