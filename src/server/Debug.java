package server;

import message.Constant;
import message.Message;
import message.application.ClientShutDownInd;
import message.application.CloseGameReq;
import message.application.CloseGameResp;
import message.application.ErrorInd;
import message.application.GameListReq;
import message.application.GameListResp;
import message.application.JoinGameReq;
import message.application.JoinGameResp;
import message.application.JoinServerReq;
import message.application.JoinServerResp;
import message.application.NewGameReq;
import message.application.NewGameResp;
import message.application.ServerShutDownInd;
import message.game.ChooseHomePlanetReq;
import message.game.ChooseHomePlanetResp;
import message.game.ChooseMapReq;
import message.game.ChooseMapResp;
import message.game.GameEndedInd;
import message.game.GameStateInd;
import message.game.GameStateUpdateInd;
import message.game.LeaveGameInd;
import message.game.MoveReq;
import message.game.MoveResp;
import message.game.TimeOutInd;
import message.game.TurnEndedInd;
import message.game.TurnStartedInd;
import message.game.WaitInd;

public class Debug {
    
    public static void print(Message message) {
        
        String output = null;

        switch (Constant.values()[message.getMessageId()]) {

        case REQ_CHOOSE_MAP: {
            output = ((ChooseMapReq) message).toString();
            break;
        }
        case RESP_CHOOSE_MAP: /* MessagePlayer */
        {            
            output = ((ChooseMapResp) message).toString();
            break;
        }
        case REQ_CHOOSE_HOME_PLANET: {
            output = ((ChooseHomePlanetReq) message).toString();
            break;
        }
        case RESP_CHOOSE_HOME_PLANET: /* MessagePlayer */
        {            
            output = ((ChooseHomePlanetResp) message).toString();
            break;
        }
        case REQ_CLOSE_GAME: /* MessageGame */
        {
            output = ((CloseGameReq) message).toString();
            break;
        }
        case RESP_CLOSE_GAME: /* MessageGame */
        {
            output = ((CloseGameResp) message).toString();
            break;
        }
        case IND_LEAVE_GAME: /* MessagePlayer */
        {
            output = ((LeaveGameInd) message).toString();
            break;
        }
        case REQ_MOVE: /* MessagePlayer */
        {
            output = ((MoveReq) message).toString();
            break;
        }
        case RESP_MOVE: /* MessagePlayer */
        {
            output = ((MoveResp) message).toString();
            break;
        }

        case REQ_JOIN_SERVER: {
            output = ((JoinServerReq) message).toString();
            break;
        }

        case RESP_JOIN_SERVER: {
            output = ((JoinServerResp) message).toString();
            break;
        }

        case REQ_NEW_GAME: {
            output = ((NewGameReq) message).toString();
            break;
        }

        case RESP_NEW_GAME: {
            output = ((NewGameResp) message).toString();
            break;
        }

        case REQ_JOIN_GAME: /* MessageGame */
        {
            output = ((JoinGameReq) message).toString();
            break;
        }

        case RESP_JOIN_GAME: /* MessageGame */
        {
            output = ((JoinGameResp) message).toString();
            break;
        }

        case IND_GAME_ENDED: 
        {
            output = ((GameEndedInd) message).toString();
            break;
        }

        case IND_GAME_STATE: 
        {
            output = ((GameStateInd) message).toString();
            break;
        }

        case IND_GAME_STATE_UPDATE: 
        {
            output = ((GameStateUpdateInd) message).toString();
            break;
        }
        
        case RESP_GAME_LIST: 
        {
            output = ((GameListResp) message).toString();
            break;
        }
        
        case REQ_GAME_LIST: 
        {
            output = ((GameListReq) message).toString();
            break;
        }

        case IND_TIMEOUT: 
        {
            output = ((TimeOutInd) message).toString();
            break;
        }
        case IND_TURN_ENDED: 
        {
            output = ((TurnEndedInd) message).toString();
            break;
        }
        case IND_TURN_STARTED: 
        {
            output = ((TurnStartedInd) message).toString();
            break;
        }
        case IND_WAIT: 
        {
            output = ((WaitInd) message).toString();
            break;
        }
        case IND_CLIENT_SHUTDOWN: {
            output = ((ClientShutDownInd) message).toString();
            break;
        }

        case IND_SERVER_SHUTDOWN: {
            output = ((ServerShutDownInd) message).toString();
            break;
        }

        case IND_ERROR: {
            output = ((ErrorInd) message).toString();
            break;
        }

        default: {
            output = "unknown message: ";
            output += message.toString();
        }
        }
        System.out.println(output);
    }
}