package server.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

import server.client.Client;

/* Starts a server.
 * The server listens for connections and adds each connection as a client if this is possible. */

/* NOTE: Server MUST be started before any clients are started - 
 *       optional command line argument may be used to specify whether server is local as well a port number.
 *       Usages: Server local (local server on unspecified port) 
 *               Server local 1024 (local server on specified port) 
 *               Server (iff connected to a network it is a networked server on unspecified port) 
 *               Server 1024 (iff connected to a network it is a networked server on unspecified port) */

/* 
 * Well-known ports end at 1023 - so server should pick port number s.t. 1023 < portNumber <= 65535
 * https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt 
 * http://tools.ietf.org/html/rfc6335 
 */

public class Server {
    
    private ServerAdmin serverAdmin;
    private ConnectionHandler connectionHandler;
    private CommandReader commandReader;

    /* Waits for connections from clients */
	private class ConnectionHandler implements Runnable {
	    
	    private final int MIN_PORT = 1024;
	    private final int MAX_PORT = 65535;
	    private String[] arguments;
	    private int connectionCounter = 0;
	    /* This is made volatile in order to avoid synchronizing access */
	    private volatile boolean running = true;
	    ServerSocket serverSocket = null;
	    
	    public ConnectionHandler(String[] arguments) {
	        this.arguments = arguments;
	    }
	    
	    public boolean initialize() {
	        
	        boolean local = false;
            int portNumber = MIN_PORT;
            InetAddress localAddress = null;

            if (arguments.length >= 1) { /* Command line arguments present */
                
                /* First command line argument is either a port number or the string local */
                try {
                    portNumber = Integer.parseInt(arguments[0]);
                } catch (NumberFormatException e) {
                    if (arguments[0].compareToIgnoreCase("local") == 0) {
                        local = true;
                    }
                }

                /* Second command line (if present) must be a port number */
                if (local && (arguments.length >= 2)) {
                    try {
                        portNumber = Integer.parseInt(arguments[1]);
                    } catch (NumberFormatException e) {
                    }
                }

                if (portNumber < MIN_PORT || portNumber > MAX_PORT) {
                    portNumber = MIN_PORT;
                }
            }

            try {
                localAddress = InetAddress.getLocalHost();
                if ("127.0.0.1".equals(localAddress.getHostAddress().toString())) {
                    local = true; /* Not connected to any network */
                }
            } catch (UnknownHostException e) {
                running = false;
            }

            int attemptsCount = 1;

            /* Attempt creating a server socket on a port - until an available port has been found or all options have been exhausted */
            while (running && (serverSocket == null) && (attemptsCount <= (MAX_PORT - MIN_PORT))) {
                try {
                    if (local) {
                        serverSocket = new ServerSocket(portNumber, 50, InetAddress.getByName(null));
                    } else {
                        serverSocket = new ServerSocket(portNumber, 50, localAddress);
                    }
                } catch (IOException e) {
                    portNumber++;

                    if (portNumber > MAX_PORT) {
                        portNumber = MIN_PORT;
                    }
                }
            }

            if (!running || (serverSocket == null)) {
                System.err.println("Server NOT running: could not listen on any port.");
                return false;
            }

            System.out.println("Server running:");

            System.out.println("host name: \t" + serverSocket.getInetAddress().getCanonicalHostName());
            System.out.println("ip address: \t" + serverSocket.getInetAddress().getHostAddress());
            System.out.println("port:\t\t" + serverSocket.getLocalPort());
            return true;
	    }
	    
	    /* Clients can no longer connect to the server */
        public void shutDown() {
            running = false;
            if (serverSocket != null) {
                if (!serverSocket.isClosed()) {
                    try {
                        serverSocket.close();
                    } catch (IOException e) {
                        System.err.println("Could not close server socket");
                    }
                }
            }
        }

        public void run() {
            
            while (running) {
                try {
                    /* Create a new thread for each client */
                    Client newClient = new Client(serverAdmin, serverSocket.accept());
                    serverAdmin.handleNewClientConnection(newClient, connectionCounter++);
                }
                catch (SocketException e) {
                    shutDown();
                }
                catch (IOException e) {
                    System.err.println("Server could not accept new connection.");
                    shutDown();
                }
            }
            System.out.println("Connection handler shut down");
        }
    }
	
	/* Reads commands from standard in */
    private class CommandReader implements Runnable {

        private Server server;
        private String command;
        private Scanner consoleInput = null;
        /* This is made volatile in order to avoid synchronizing access */
        private volatile boolean running = true;

        public CommandReader(Server server) {
            this.server = server;
            consoleInput = new Scanner(System.in);
        }
        
        public void shutDown() {
            running = false;
            if (consoleInput != null) {
                consoleInput.close();
            }
        }

        public void run() {
            
            System.out.println("Enter shut down to close server.");

            while (running) {
                command = consoleInput.nextLine();

                if (command.equalsIgnoreCase("shut down")) {
                    server.shutDown();
                    System.out.println("Server shutting down.");
                } else {
                    System.out.println("Command not recognised.");
                }
            }
            System.out.println("Console reader shut down");
        }
    }
    
    public Server() {
        serverAdmin = new ServerAdmin();
    }
    
    public void startServer(String[] arguments) {

        connectionHandler = new ConnectionHandler(arguments);
        boolean connectionHandlerInitialized = connectionHandler.initialize();
        
        if (connectionHandlerInitialized) {
            (new Thread(connectionHandler, "ConnectionHandler")).start();
            commandReader = new CommandReader(this);
            commandReader.run();
        }
    }
    
    public void shutDown() {
        if (commandReader != null) {
            commandReader.shutDown();
        }
        
        if (connectionHandler!= null) {
            connectionHandler.shutDown();
        }
    }
    
	public static void main(String[] args) {
	    Server server = new Server();
	    server.startServer(args);
	}
}
