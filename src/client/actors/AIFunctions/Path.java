package client.actors.AIFunctions;
import cell.Cell;
import cell.PlanetCell;
import cell.WormHoleCell;

public class Path {

    private int distance;
    private boolean planetPath;
    private Cell[] shortestPath;
    
    public Path (Path path1, Path path2, int playerID) {
        /* SPECIFICALLY DESIGNED FOR WORMHOLES */
        if (path1.getShortestPath() == null || path2.getShortestPath() == null) {
            shortestPath = null;
            return;
        }
        this.distance = path1.getDistance() + path2.getDistance() + 1;
        Cell[] cells1 = path1.getShortestPath();
        Cell[] cells2 = path2.getShortestPath();
        this.planetPath = path1.hasPlanetInPath() || path2.hasPlanetInPath();
        this.shortestPath = new Cell[distance];
        for (int i = 0; i < cells1.length; i++)
            if (cells1[i] instanceof PlanetCell)
                this.shortestPath[i] = (Cell)(new PlanetCell((PlanetCell)cells1[i]));
            else if (cells1[i] instanceof WormHoleCell)
                this.shortestPath[i] = (Cell)(new WormHoleCell((WormHoleCell)cells1[i]));
            else
                this.shortestPath[i] = new Cell(cells1[i]);
        this.shortestPath[cells1.length] = ((WormHoleCell)cells1[cells1.length-1]).getConnectedTo();
        if (this.shortestPath[cells1.length] instanceof PlanetCell)
            if (((PlanetCell)this.shortestPath[cells1.length]).getClaimedBy() != playerID)
                this.planetPath = true;
        for (int i = 1; i <= cells2.length; i++)
            if (cells2[i-1] instanceof PlanetCell)
                this.shortestPath[i+cells1.length] = (Cell)(new PlanetCell((PlanetCell)cells2[i-1]));
            else if (cells2[i-1] instanceof WormHoleCell)
                this.shortestPath[i+cells1.length] = (Cell)(new WormHoleCell((WormHoleCell)cells2[i-1]));
            else
                this.shortestPath[i+cells1.length] = new Cell(cells2[i-1]);
    }
    
    public Path (Path path) {
        if (path.getShortestPath() == null) {
            shortestPath = null;
            return;
        }
        this.distance = path.getDistance();
        this.planetPath = path.hasPlanetInPath();
        Cell[] temp = path.getShortestPath();
        this.shortestPath = new Cell[temp.length];
        for (int i = 0; i < shortestPath.length; i++)
            if (temp[i] instanceof PlanetCell)
                this.shortestPath[i] = (Cell)(new PlanetCell((PlanetCell)temp[i]));
            else if (temp[i] instanceof WormHoleCell)
                this.shortestPath[i] = (Cell)(new WormHoleCell((WormHoleCell)temp[i]));
            else
                this.shortestPath[i] = new Cell(temp[i]);
    }
    
    public Path (Cell[] cells, int playerID) {
        if (cells == null) {
            shortestPath = null;
            return;
        }
        this.distance = cells.length;
        this.planetPath = false;
        this.shortestPath = new Cell[cells.length];
        for (int i = 0; i < cells.length; i++)
            if (cells[i] instanceof PlanetCell) {
                this.shortestPath[i] = (Cell)(new PlanetCell((PlanetCell)cells[i]));
                if (((PlanetCell)cells[i]).getClaimedBy() != playerID && i != cells.length-1)
                    this.planetPath = true;
            } else if (cells[i] instanceof WormHoleCell)
                this.shortestPath[i] = (Cell)(new WormHoleCell((WormHoleCell)cells[i]));
            else
                this.shortestPath[i] = new Cell(cells[i]);
    }
    
    public boolean hasPlanetInPath() {
        return this.planetPath;
    }

    public int getDistance() {
        return distance;
    }
    
    public Cell[] getShortestPath() {
        return shortestPath;
    }
    
}
