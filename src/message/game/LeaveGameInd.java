package message.game;

import java.io.Serializable;

import message.Constant;
import message.MessagePlayer;

public class LeaveGameInd extends MessagePlayer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7541816060440499467L;

	public LeaveGameInd(int clientId, int gameId, int playerId) {
		super(Constant.IND_LEAVE_GAME.getId(), clientId, gameId, playerId);
	}
}
