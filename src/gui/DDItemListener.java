package gui;

public interface DDItemListener {

    public void itemSelected(DDItem ddItem);
}
