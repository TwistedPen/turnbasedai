package message;

import java.io.Serializable;

public class MessageClient extends Message implements Serializable {
	
	/**
     * 
     */
    private static final long serialVersionUID = 8887452338117807649L;
    private final int clientId;
	
	public MessageClient(int messageId, int clientId) {
		super(messageId);
		this.clientId = clientId;
	}

	public int getClientId() {
		return clientId;
	}
	
	public boolean verify(int clientId) {
	    return (clientId == this.clientId);
	}
	
    public boolean verify(int clientId, int gameId) {
        return verify(clientId);
    }

    public boolean verify(int clientId, int gameId, int playerId) {
        return verify(clientId, gameId);
    }
    
	@Override
	public String toString() {
		return super.toString() + "\n" + "[clientId=" + clientId + "]";
	}
}
