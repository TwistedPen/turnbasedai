package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;


public class JoinMenuScreen extends GuiMenu {

    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
    private Button joinButton;
    public static String ip = "";
    
    public JoinMenuScreen() {
        super();

        joinButton = addButton(new Button(ButtonID.PERFORM_JOIN_ID, 3, (screenW - 128)/2, screenH / 4 + 120));
        addButton(new Button(ButtonID.CHANGE_PORT_ID, 8, (screenW - 128)/2, screenH / 4 + 160));
        addButton(new Button(ButtonID.CANCEL_ID, 4, (screenW - 128)/2, screenH / 4 + 200));
    }

    @Override
    public void render(Screen screen) {

        screen.clear(0);
        Font.draw(screen, "Enter IP of Host:", (screenW - Font.getStringWidth("Enter IP of Host:"))/2, screenH / 4);
        Font.draw(screen, ip + "-",  (screenW - Font.getStringWidth(ip + "-"))/2, screenH / 4 + 20);
        
        super.render(screen);
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_ENTER && ip.length() > 0) {
            joinButton.postClick();
        }
    }

    public void keyReleased(KeyEvent arg0) {
    }

    public void keyTyped(KeyEvent e) {

        if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE && ip.length() > 0) {
            ip = ip.substring(0, ip.length() - 1);
        } else if (Font.letters.indexOf(Character.toUpperCase(e.getKeyChar())) >= 0) {
            ip += e.getKeyChar();
        }
    }

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}
