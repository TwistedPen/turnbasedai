package message;

/* NOTE:
 * Please use these constants whenever they are applicable. This will make changes to the entire application much, much easier.
 * Should you need a constant which isn't listed then please add it.
 */

/* NOTE:
 * Only the id/ordinal/integer value of an enum is sent in messages!!!
 */

/* NOTE:
 * Constant.values()[x] returns the enum with the ordinal value x
 */

public enum Constant {
	
    /* DO NOT MOVE - must always be zero */
    NONE("NONE"),
	
	/* Message types */
	REQ_JOIN_SERVER("REQEST JOIN SERVER"),
	REQ_NEW_GAME("REQUEST NEW GAME"),
	RESP_CHOOSE_MAP("RESPONSE CHOOSE MAP"),
	REQ_JOIN_GAME("REQUEST JOIN GAME"),
	RESP_CHOOSE_HOME_PLANET("RESPONSE CHOSE HOME PLANET"),
    REQ_MOVE("REQUEST MOVE"),
    IND_LEAVE_GAME("INDICATION LEAVE GAME"),
    /* Only possible if requesting player is the only player left in game */
    REQ_CLOSE_GAME("REQUEST CLOSE GAME"),
    IND_CLIENT_SHUTDOWN("CLIENT HAS BEEN SHUT DOWN"),
    IND_WAIT("INDICATION WAIT"),
    /* Server may refuse due to too many clients */
    RESP_JOIN_SERVER("RESPONSE JOIN SERVER"),
    REQ_GAME_LIST("REQUEST GAME LIST"),
    RESP_GAME_LIST("RESPONSE GAME LIST"),
    RESP_NEW_GAME("RESPONSE NEW GAME"),
    /* Indicates that the last game map option has been sent - note: these are sent as IND_GAME_STATE */
    REQ_CHOOSE_MAP("REQUEST CHOOSE MAP"),
    RESP_JOIN_GAME("RESPONSE JOIN GAME"),
    REQ_CHOOSE_HOME_PLANET("REQUEST CHOOSE HOME PLANET"),
    /* Also used when choosing map and home planet */
    IND_GAME_STATE("INDICATION GAME STATE"), 
    /* Not used - as we are currently sending entire game state */
    IND_GAME_STATE_UPDATE("INDICATION GAME STATE UPDATE"),
    /* Players may make their moves */
    IND_TURN_STARTED("INDICATION TURN STARTED"),
    IND_TURN_ENDED("INDICATION TURN ENDED"),
    /* If sent after playing time expired or incorrect move - it may be nack */
    RESP_MOVE("RESPONSE MOVE"),
    /* Players may no longer make moves */
    IND_TIMEOUT("INDICATION TIME OUT"), 
    IND_GAME_ENDED("INDICATION GAME ENDED"),
    RESP_CLOSE_GAME("RESPONSE CLOSE GAME"),
    IND_SERVER_SHUTDOWN("SERVER HAS BEEN SHUT DOWN"),
    IND_ERROR("AN ERROR HAS OCCURRED"),
	
	/* Possible moves */
    MOVE_UP_RIGHT("UP RIGHT"),
    MOVE_RIGHT("RIGHT"),
    MOVE_DOWN_RIGHT("DOWN RIGHT"),
    MOVE_DOWN_LEFT("DOWN LEFT"),
    MOVE_LEFT("LEFT"),
    MOVE_UP_LEFT("UP LEFT"),
    MOVE_THROUGH_WORM("THROUGH WORMHOLE"),
    
    /* Player types */
    PLAYER_ACTIVE("ACTIVE"),
    PLAYER_INACTIVE("INACTIVE"),
    PLAYER_SPECTATOR("SPECTATOR"),
    
    /* Game states */
    GAME_PLAYER_LIST_NOT_FULL("PLAYER LIST IS NOT FULL"),
    GAME_PLAYER_LIST_FULL_NOT_STARTED("PLAYER LIST IS FULL - GAME HAS NOT STARTED"),
    GAME_STARTED("GAME HAS STARTED"),
    GAME_ENDED("GAME HAS ENDED"),

    /* Ack/nack codes from server */
    OK("OK"),
    
    ALREADY_JOINED("CLIENT HAS ALREADY JOINED SERVER"),
    TOO_MANY_CLIENTS("SERVER CANNOT ACCEPT ANY MORE CLIENTS"),
    SERVER_SHUT_DOWN("THE SERVER HAS BEEN SHUT DOWN"),
    CANNOT_NOT_CREATE_INPUTSTREAM("SERVER CANNOT CREATE INPUTSTREAM"),
    CONNECTION_PROBLEM("CONNECTION PROBLEM - CLOSING CONNECTION"),
    HAS_NOT_JOINED_GAME("CLIENT HAS NOT JOINED - THIS GAME"),
    TOO_MANY_GAMES("THE NUMBER OF GAMES ON THE SERVER HAS REACHED ITS MAXIMUM"),
    NO_MAPS_AVAILABLE("RELEVANT MAPS IS NOT AVAILABLE"),
    REQUESTED_PLAYER_COUNT_ERROR("CANNOT CREATE GAME WITH REQUESTED NUMBER OF PLAYERS"),
    PLAYER_LIST_FULL("THE GAME CANNOT ACCEPT ANOTHER PLAYER"),
    GAME_HAS_STARTED("THE GAME HAS ALREADY STARTED"),
    PLAYERS_IN_GAME("THE GAME HAS REGISTERED PLAYERS"),
    TURN_HAS_ENDED("CURRENT TURN HAS ENDED"),
    
    NO_FLEET_IN_CELL("PLAYER HAS NO FLEET IN THIS CELL"),
    CELL_FLEET_SIZE_TOO_SMALL("PLAYER'S FLEET IN RELEVANT CELL EXCEEDS MOVE REQUEST"),
    CELL_IS_NOT_WORMHOLE("CELL IS NOT A WORMHOLE"),
    
    CLIENT_ID_NOT_RECOGNIZED("SUPPLIED CLIENT ID IS NOT RECOGNIZED"),
    GAME_ID_NOT_RECOGNIZED("SUPPLIED GAME ID IS NOT RECOGNIZED"),
    PLAYER_ID_NOT_RECOGNIZED("SUPPLIED PLAYER ID IS NOT RECOGNIZED"),
    MESSAGE_NOT_EXPECTED("MESSAGE NOT EXPECTED"),
    
    /* Constant for wait screen in gui */
    
    WAIT_FOR_PLAYERS("WAIT FOR OTHER PLAYERS TO JOIN"),
    WAIT_FOR_PLAYER_TO_CHOOSE_MAP("WAIT FOR A MAP TO BE CHOSEN");

	private final String message;

	Constant(String message) {
		this.message = message;
	}

	public int getId() {
		return this.ordinal();
	}

	public String getMessage() {
		return message;
	}
	
}