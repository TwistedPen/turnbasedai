package message.application;

import java.io.Serializable;

import message.Constant;
import message.MessageGame;

public class NewGameResp extends MessageGame implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6494226201155541141L;
	
	private int response;
	
	public NewGameResp(int clientId, int gameId, int response) {
		super(Constant.RESP_NEW_GAME.getId(), clientId, gameId);
		this.response = response;
	}

	public int getResponse() {
		return response;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "NewGameResp [response=" + response + "]";
	}
}