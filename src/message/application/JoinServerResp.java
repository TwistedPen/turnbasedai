package message.application;

import java.io.Serializable;
import message.Constant;
import message.MessageClient;

public class JoinServerResp extends MessageClient implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4581175183011564002L;
	private int response;
	
	public JoinServerResp(int clientId, int response) {
		super(Constant.RESP_JOIN_SERVER.getId(), clientId);
		this.response = response;
	}

	public int getResponse() {
		return response;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "JoinServerResp" + "\n" + "[response=" + response + "]";
	}
}
