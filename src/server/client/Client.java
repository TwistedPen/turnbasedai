package server.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;

import server.game.Player;

import server.Debug;
import server.server.ServerAdmin;

import message.Constant;
import message.Message;
import message.application.CloseGameReq;
import message.application.GameListReq;
import message.application.JoinGameReq;
import message.application.JoinServerReq;
import message.application.NewGameReq;
import message.game.ChooseHomePlanetResp;
import message.game.ChooseMapResp;
import message.game.LeaveGameInd;
import message.game.MoveReq;

/* Communicates with the server and any players */

/* silently discard any messages - where the clientId does not match
 * - otherwise an opponent could just try all the ids until a match is found */

/* if last player removed from this client - and its state is shutdown - remove from server */

public class Client implements Runnable {
    
    public static final boolean DEBUG = false;

    private ServerAdmin serverAdmin;
    /* Socket used to communicate with this client */
    private Socket socket;
    /* Message stream from client */
    private ObjectInputStream inMessageStream = null;
    /* Message stream to client */
    private ObjectOutputStream outMessageStream = null;
    private int clientId;

    /* Should this thread continue reading messages from the client ? */
    private volatile boolean running = true;
    
    /* All the players associated with this client */
    private HashMap<Integer, HashMap<Integer, Player>> players;

    public Client(ServerAdmin serverAdmin, Socket socket) {
        this.serverAdmin = serverAdmin;
        this.socket = socket;
        players = new HashMap<Integer, HashMap<Integer, Player>>();
    }
    
    public void setId(int clientId) {
        this.clientId = clientId;
    }
    
    public int getId() {
        return this.clientId;
    }
    
    public Socket getSocket() {
        return this.socket;
    }
    
    public void addPlayer(Player player) {
        
        Integer gameId = new Integer(player.getGameId());
        Integer playerId = new Integer(player.getPlayerId());
        
        synchronized (players) {
            
            HashMap<Integer, Player> currPlayers = players.get(gameId);
            
            if (currPlayers == null) {
                currPlayers = new HashMap<Integer, Player>();
            }
            currPlayers.put(playerId, player);
            players.put(gameId, currPlayers);
        }
    }
    
    public void removePlayer(Player player) {

        Integer gameId = new Integer(player.getGameId());
        Integer playerId = new Integer(player.getPlayerId());

        synchronized (players) {

            HashMap<Integer, Player> currPlayers = players.get(gameId);

            if (currPlayers != null) {
                currPlayers.remove(playerId);
                if (currPlayers.isEmpty()) {
                    players.remove(gameId);
                }
            }
        }
        /* check if there are no more players and client is in shut down state */
    }

    /*
     * Shutdown this thread - should be performed when either server or client
     * issues a shutdown indication or there was a problem during initialization
     * of the connection the client
     */
    public void shutDown() {

        running = false;

        if (inMessageStream != null) {
            try {
                inMessageStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void sendMessage(Message sendToClient) {
        
        try {
            if (sendToClient != null) {
                if (DEBUG) {
                    System.out.println("Server SENDING:");
                    Debug.print(sendToClient);
                }
                synchronized (outMessageStream) {
                    /* as soon as we receive client shut down - shutdown this socket output */
                    outMessageStream.writeObject(sendToClient);
                    /* See: http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6525563 */
                    outMessageStream.reset();
                }
            }
        } catch (IOException e) {
            /* Try to retransmit ?? - or behave as if leave indication was issued */
            running = false;
            shutDown();
        }
    }

    public void run() {

        Message received = null;

        try {
            outMessageStream = new ObjectOutputStream(socket.getOutputStream());
            outMessageStream.flush();
        } catch (IOException e) {
            shutDown();
        }
        
        try {
            inMessageStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            shutDown();
        }
        
        /* Register with server as unregistered client -
         * the server should be able to shut down all threads */
        
        while (running) {
            
            try {

                received = (Message) inMessageStream.readObject();
                
                if (DEBUG) {
                    System.out.println("Server RECEIVED: ");
                    Debug.print(received);
                }
                
                /* This should be replaced with a state-dependent handler */
                switch (Constant.values()[received.getMessageId()]) {
                
                case REQ_JOIN_GAME:
                {
                    serverAdmin.handleJoinGameReq(this, (JoinGameReq) received);
                    break;
                }
                
                case REQ_GAME_LIST:
                {
                    serverAdmin.handleGameListReq(this, (GameListReq) received);
                    break;
                }
                
                case REQ_CLOSE_GAME: 
                {
                    serverAdmin.handleCloseGameReq(this, (CloseGameReq) received);
                    break;
                }
                
                case RESP_CHOOSE_MAP:
                {
                    ChooseMapResp resp = (ChooseMapResp) received;
                    HashMap<Integer, Player> current = players.get(resp.getGameId());
                    Player player = current.get(resp.getPlayerId());
                    player.handleChooseMapResp(resp);
                    break;
                }
                case RESP_CHOOSE_HOME_PLANET:
                {
                    ChooseHomePlanetResp resp = (ChooseHomePlanetResp) received;
                    HashMap<Integer, Player> current = players.get(resp.getGameId());
                    Player player = current.get(resp.getPlayerId());
                    player.handleChooseHomePlanetResp(resp);
                    break;
                }
                case IND_LEAVE_GAME:
                {
                    LeaveGameInd ind = (LeaveGameInd) received;
                    HashMap<Integer, Player> current = players.get(ind.getGameId());
                    Player player = current.get(ind.getPlayerId());
                    player.handleLeaveGameInd(ind);
                    break;
                }
                case REQ_MOVE:
                {
                    MoveReq req = (MoveReq) received;
                    HashMap<Integer, Player> current = players.get(req.getGameId());
                    Player player = current.get(req.getPlayerId());
                    player.handleMoveReq(req);
                    break;
                }

                case REQ_JOIN_SERVER:
                {
                    serverAdmin.handleJoinServerReq(this, (JoinServerReq) received);
                    break;
                }
                
                case REQ_NEW_GAME:
                {
                    serverAdmin.handleNewGameReq(this, (NewGameReq) received);
                    break;
                }

                case IND_ERROR: {
                    /* temporary debugging measure */
                    break;
                }
                
                case IND_CLIENT_SHUTDOWN:
                default: {
                    /* Object will still remain - until the last game has ended */
                    /* ENSURE neither server nor games can send messages to this client - it will throw an exception if that happens */
                    socket.shutdownOutput();
                    /* Remove from server */
                    /* Remove from all games */
                    /* Shutdown */
                    break;
                }
                }

            } catch (ClassNotFoundException e) {
                shutDown();
            } catch (IOException e) {
                shutDown();
            }
        }
        shutDown();
    }
}
