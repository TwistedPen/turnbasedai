package constants;

/* NOTE:
 * Please use this constants whenever they are applicable. This will make changes to the entire application much, much easier.
 * Should you need a constant which isn't listed then please add it.
 */

/* NOTE:
 * Only the id/ordinal/integer value of an enum is sent in messages!!!
 */

/* NOTE:
 * Constant.values()[x] returns the enum with the ordinal value x
 */

public enum GameConstant {
	
	MAX_ACTIVE_PLAYER_COUNT(10),
	MAX_PLAYER_COUNT(20), 
	MAX_MAP_WIDTH(100),
	MAX_MAP_HEIGHT(100);
	
	private final int value;

	GameConstant(int value) {
		this.value = value;
	}

	public int getId() {
		return this.ordinal();
	}
	
	public int getValue() {
		return this.value;
	}
}
