package message.game;

import java.io.Serializable;

import message.Constant;
import message.MessagePlayer;
import message.data.GameData;

public class ChooseMapReq extends MessagePlayer implements Serializable {
	
	/**
     * 
     */
    private static final long serialVersionUID = 8883084556012551806L;
    
    /* Timeout in seconds */
	private int timeOut;
	/* Number of map options */
	private int mapOptionsCount;
	/* Maps which may be chosen */
	private GameData[] maps;

	public ChooseMapReq(int clientId, int gameId, int playerId, int timeOut, int mapOptionsCount, GameData[] maps) {
		super(Constant.REQ_CHOOSE_MAP.getId(), clientId, gameId, playerId);
		this.timeOut = timeOut;
		this.mapOptionsCount = mapOptionsCount;
		this.maps = maps;
	}

	public int getTimeOut() {
		return timeOut;
	}
	
	public int getMapOptionsCount() {
		return mapOptionsCount;
	}
	
	public GameData[] getMaps() {
	    return maps;
	}

	@Override
	public String toString() {
	    
        String mapsStr = "";
        
        if(maps != null)
            for (GameData map : maps) {
                mapsStr += map.toString() + "\n";
            }
        
		return super.toString() + "\n" + "ChooseMapReq [timeOut=" + timeOut + ", mapOptionsCount="
				+ mapOptionsCount + " maps= " + mapsStr + "]";
	}
}
