package message.application;

import java.io.Serializable;

import message.Constant;
import message.MessageClient;

public class ClientShutDownInd extends MessageClient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3313695786658449219L;

	public ClientShutDownInd(int clientId) {
		super(Constant.IND_CLIENT_SHUTDOWN.getId(), clientId);
	}
}
