package message.data;

import java.io.Serializable;

public class PlanetData implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -8671353794191456715L;
    
    private int location;
	private int colonizedBy; /* May be colonized by NONE */
	private int requiredFleetSize;
	private int expendFleetSize;
	private int productionFleetSize;
	private int productionRate;
	private int visionRange;
	
	public PlanetData(int location, int colonizedBy, int requiredFleetSize,
			int expendFleetSize, int productionFleetSize, int productionRate,
			int visionRange) {
		
		this.location = location;
		this.colonizedBy = colonizedBy;
		this.requiredFleetSize = requiredFleetSize;
		this.expendFleetSize = expendFleetSize;
		this.productionFleetSize = productionFleetSize;
		this.productionRate = productionRate;
		this.visionRange = visionRange;
	}
	
	public PlanetData(PlanetData PD){
	    this.location = PD.location;
        this.colonizedBy = PD.colonizedBy;
        this.requiredFleetSize = PD.requiredFleetSize;
        this.expendFleetSize = PD.expendFleetSize;
        this.productionFleetSize = PD.productionFleetSize;
        this.productionRate = PD.productionRate;
        this.visionRange = PD.visionRange;
	}
	

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public int getColonizedBy() {
		return colonizedBy;
	}

	public void setColonizedBy(int colonizedBy) {
		this.colonizedBy = colonizedBy;
	}

	public int getRequiredFleetSize() {
		return requiredFleetSize;
	}

	public void setRequiredFleetSize(int requiredFleetSize) {
		this.requiredFleetSize = requiredFleetSize;
	}

	public int getExpendFleetSize() {
		return expendFleetSize;
	}

	public void setExpendFleetSize(int expendFleetSize) {
		this.expendFleetSize = expendFleetSize;
	}

	public int getProductionFleetSize() {
		return productionFleetSize;
	}

	public void setProductionFleetSize(int productionFleetSize) {
		this.productionFleetSize = productionFleetSize;
	}

	public int getProductionRate() {
		return productionRate;
	}

	public void setProductionRate(int productionRate) {
		this.productionRate = productionRate;
	}

	public int getVisionRange() {
		return visionRange;
	}

	public void setVisionRange(int visionRange) {
		this.visionRange = visionRange;
	}
	
	public boolean isClaimed(){
	    if(colonizedBy != 0)
	        return true;
	    return false;
	}

	@Override
	public String toString() {
		return "PlanetData [location=" + location + ", colonizedBy="
				+ colonizedBy + ", requiredFleetSize=" + requiredFleetSize
				+ ", expendFleetSize=" + expendFleetSize
				+ ", productionFleetSize=" + productionFleetSize
				+ ", productionRate=" + productionRate + ", visionRange="
				+ visionRange + "]";
	}
}
