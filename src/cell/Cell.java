package cell;

import client.Map;
import client.MouseButtons;
import client.actors.Actor;

import gui.Gui;
import gui.GuiComponent;
import gui.sprites.Art;
import gui.sprites.Screen;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

import message.Constant;

public class Cell extends GuiComponent implements Comparable<Object> {
	
	protected List<CellListener> listeners;
	
    public static final int HEIGHT = 60;
    public static final int WIDTH = 52;
    public static final int SCALE = Gui.SCALE;
   
    private int playerID; // the player occupying this cell.
	private int ship; //number of ships in the cell
	private int xpos, ypos; //position in gui
	private int x = 0, y = 0; // position in level
	
	private Polygon p;
	private Map map;
	private int id = 0;
	private boolean visible = false;
	
	private boolean performClick;
	
	// For AI pathfind
	private double gScore = 0;
    private double fScore = 0;
    private Cell parent = null;

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public Cell() {
		
	}
	
	public Map getMap() {
	    return map;
	}

	public Cell(Cell newC){
	    
	    this.fScore = newC.fScore;
	    this.gScore = newC.gScore;
	    this.id = newC.getID();
	    this.playerID = newC.getPlayerID();
	    this.x = newC.x; 
	    this.y = newC.y;
	    this.xpos = newC.getXpos(); 
	    this.ypos = newC.getYpos();
	    this.map = newC.getMap();
	}
	
	public Cell(int x,int y) {
		
		this.x = x;
		this.y = y;
	}
	
	public Cell(int x,int y,int xPos, int yPos) {
        
        this.x = x;
        this.y = y;
        this.xpos = xPos;
        this.ypos = yPos;
    }
	
	
	public int getShip() {
		return ship;
	}

	public void setShip(int ship) {
		this.ship = ship;
	}

	// Position on the screen
	public int getXpos() {
		return xpos;
	}
	
	// Position on the screen
	public void setXpos(int xpos) {
		this.xpos = xpos;
	}

	// Position on the screen
	public int getYpos() {
		return ypos;
	}

	// Position on the screen
	public void setYpos(int ypos) {
		this.ypos = ypos;
	}

	// Position in the level
	public int getX() {
		return x;
	}

	// Position in the level
	public void setX(int x) {
		this.x = x;
	}

	// Position in the level
	public int getY() {
		return y;
	}

	// Position in the level
	public void setY(int y) {
		this.y = y;
	}

	public int getPlayerID() {
		return playerID;
	}

	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}

	public Polygon getPolygon() {
		return p;
	}
	
	public void init(Map m, int xpos, int ypos, int x, int y, boolean visible){
		this.visible = visible;
		this.map = m;
		this.xpos = xpos;
		this.ypos = ypos;
		this.x = x;
		this.y = y;
		
		//Constructs the polygon
		p = new Polygon();
		
		
		//scaling for polygons
		int xpost = (xpos)*SCALE; 
		int ypost = (ypos)*SCALE;
		
		setPolygon(p,xpost, ypost);
				
	}
	
	private void setPolygon(Polygon p, int xpost, int ypost){
	    //Add points for the polygon
	    p.addPoint(xpost+Cell.WIDTH*SCALE/2, ypost);
        p.addPoint(xpost+Cell.WIDTH*SCALE, ypost+Cell.HEIGHT*SCALE/4);
        p.addPoint(xpost+Cell.WIDTH*SCALE, ypost+Cell.HEIGHT*SCALE*3/4);
        p.addPoint(xpost+Cell.WIDTH*SCALE/2, ypost+Cell.HEIGHT*SCALE);
        p.addPoint(xpost, ypost+Cell.HEIGHT*SCALE*3/4);
        p.addPoint(xpost, ypost+Cell.HEIGHT*SCALE/4);
	}

	public void update(MouseButtons mouseButtons, Actor p1, int offsetX, int offsetY, int mapW, int mapH){
		super.update(mouseButtons);

		int mx = 0;
		int my = 0;

		mx = (mouseButtons.getX()- offsetX);
		my = (mouseButtons.getY()- offsetY);

		if(p.contains(mx, my) && my <Gui.GAME_HEIGHT*SCALE - 50 - offsetY){
		    if (mouseButtons.isRelased(1)) {
		        postClick();
		    } 
		}

		if (performClick) {
		    if (listeners != null) {
		        for (CellListener listener : listeners) {
		            listener.cellSelected(this);
		        }
		    }
		    performClick = false;
		}

	}
	
	public void renderError(Screen screen){
	    screen.colorBlit(Art.cellTile[0][0], xpos , ypos,0xFFFF0000);
	}
	
	@Override
	public void render(Screen screen){
	    super.render(screen);

		if(visible){
			screen.blit(Art.cellTile[0][0], xpos , ypos);

			if(ship == 1)
				screen.blit(Art.ships[0][playerID], xpos, ypos);
			else if(ship == 2)
				screen.blit(Art.ships[1][playerID], xpos, ypos);
			else if(ship == 3)
				screen.blit(Art.ships[2][playerID], xpos, ypos);
			else if(ship > 3)
				screen.blit(Art.ships[3][playerID], xpos, ypos);
			
		} else
			screen.blit(Art.cellTile[1][0], xpos , ypos);
		
	}
	
	public void postClick() {
		performClick = true;
    }
	
	public void addListener(CellListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<CellListener>();
        }
        if(!listeners.contains(listener))
        	listeners.add(listener);
    }
	
	public void removeListener(CellListener listener){
		if (listeners != null) 
			listeners.remove(listener);
	}
	
	public Cell[] getNeighbours(){
		
		//upholds the warping of the map
		int up = y-1;
		if(up < 0)
			up = map.getHeight()-1;
		
		int left = x-1;
		if(left < 0)
			left = map.getWidth()-1;
		
		int down = y+1;
		if(down > map.getHeight()-1)
			down = 0;
		
		int right = x+1;
		if(right > map.getWidth()-1)
			right = 0;
		
		
		Cell NE,SE,SW,NW;
		
		//for even rows
		if (y % 2 == 0){
			NE = map.getCell(x, up);
			SE = map.getCell(x, down);
			SW = map.getCell(left, down);
			NW = map.getCell(left, up);
		}
		
		//for odd rows
		else {
			NE = map.getCell(right, up);
			SE = map.getCell(right, down);
			SW = map.getCell(x, down);
			NW = map.getCell(x, up);
		}
			
		Cell W 	= map.getCell(left, y);
		Cell E 	= map.getCell(right, y);
		
		if(this instanceof WormHoleCell){
		    return new Cell[] {NE,E,SE,SW,W,NW,((WormHoleCell)this).getConnectedTo()};
		} else{
		    return new Cell[] {NE,E,SE,SW,W,NW};
		}
		
	}
	
	public int getAction(Cell d){
	    
	    
	    
	    int action = Constant.MOVE_UP_RIGHT.getId(); // start in NW and goes clockwise
        for(Cell nCell : d.getNeighbours()){
            if (nCell.equals(this)){
                return action;
            }
            action++;
        }
        
        if(this instanceof WormHoleCell){
            return action++;
        }
	    
	    
        return 0;
	    
	}
	
	public boolean equals(Cell c) {
		
		if(this.x == c.x && this.y == c.y){
			return true;
		}
		
		return false;
	}
	
	public double getgScore() {
        return gScore;
    }

    public void setgScore(double gScore) {
        this.gScore = gScore;
    }

    public double getfScore() {
        return fScore;
    }

    public void setfScore(double d) {
        this.fScore = d;
    }

    public Cell getParent() {
        return parent;
    }

    public void setParent(Cell parent) {
        this.parent = parent;
    }
    
    public String toStrin(){
        return "(" + this.x + " : " + this.y + ")";
    }
    
    @Override
    public boolean equals(Object o) {

        if(o instanceof Cell)
        {
            if(((Cell) o).x == this.x && ((Cell) o).y == this.y)
                return true;
        }
        return super.equals(o);
    }

    @Override
    public int compareTo(Object o) {

        if(o instanceof Cell){
            if(this.fScore < ((Cell) o).fScore)
                return -1;
            else if(this.fScore > ((Cell) o).fScore)
                return 1;
        }
        return 0;
    }
	
}
