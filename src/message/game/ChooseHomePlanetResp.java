package message.game;

import java.io.Serializable;
import message.Constant;
import message.MessagePlayer;

public class ChooseHomePlanetResp extends MessagePlayer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6091861957448753305L;
	
	private int planetLocation;
	
	public ChooseHomePlanetResp(int clientId, int gameId, int playerId, int planetLocation) {
		super(Constant.RESP_CHOOSE_HOME_PLANET.getId(), clientId, gameId, playerId);
		this.planetLocation = planetLocation;
	}
	
	public int getPlanetLocation() {
		return planetLocation;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "ChooseHomePlanetResp [planetLocation=" + planetLocation + "]";
	}
}
