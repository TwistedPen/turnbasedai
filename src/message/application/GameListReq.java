package message.application;

import java.io.Serializable;

import message.Constant;
import message.MessageClient;

public class GameListReq extends MessageClient implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4145048475928222593L;

    public GameListReq(int clientId) {
        super(Constant.REQ_GAME_LIST.getId(), clientId);
    }
}
