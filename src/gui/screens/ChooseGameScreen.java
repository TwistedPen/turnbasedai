package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.SmallButton;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import message.Constant;
import message.data.GameListData;

public class ChooseGameScreen extends GuiMenu {

    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
	private Button joinGameButton;
	public String gameId = "";
	public DropDown DD;
	public ArrayList<GameListData> gameList = new ArrayList<GameListData>();
	
	int leftBP = screenW - 128-64;
	
	public ChooseGameScreen() {
        super();
        
        gameId = "";
        
        //dropdown object
        String[] item = {"Player","AI","Spectator"};
        DD = addDropDown(new DropDown(leftBP + Font.getStringWidth("Join as")+8, 160, item));
        
        
        buttonPlacement();
    }
	
	public ChooseGameScreen(ArrayList<GameListData> gameList2) {
		super();
		
		 //dropdown object
        String[] item = {"Player","AI","Spectator"};
        DD = addDropDown(new DropDown(leftBP + Font.getStringWidth("Join as")+8, 160, item));
		
		gameList = gameList2;
		
		buttonPlacement();
		
	}
	
	private void buttonPlacement(){
	    
	    
	    addButton(new Button(ButtonID.NEW_GAME_ID, 5, leftBP , 80));
        joinGameButton = addButton(new Button(ButtonID.PERFORM_JOIN_GAME_ID, 3, leftBP, 120));
        
        addButton(new SmallButton(ButtonID.UPDATE_GAME_LIST_ID, 2, leftBP-46-24, 80-24-10));
        addButton(new Button(ButtonID.DISCONNECT_ID, 9, leftBP, screenH - 64-64));
	}

	@Override
	public void render(Screen screen) {
	    
		screen.clear(0);
		
		screen.fill(54, 80, leftBP-100, screenH - 184, 0xff808080);
		
		Font.draw(screen, "Games", 64, 60);
		Font.draw(screen,"Join as",leftBP, 163);
		
		if(gameList.size() == 0){
			
			Font.draw(screen, "No games present ", 64, 90);
			
		} else {
			
			Font.draw(screen, "Enter game ID to join:", 64, screenH-64-64);
			Font.draw(screen, gameId + "-", 64 + Font.getStringWidth("Enter game ID to join:") + 8, screenH-64-64);
			
			for(int i = 0; i < gameList.size(); i++){
				Font.draw(screen, "Game ID: " + gameList.get(i).getGameId() + " Player size: " + gameList.get(i).getMaxPlayerCount() + 
				        " Players in game: " + gameList.get(i).getCurrPlayerCount() + " " + Constant.values()[gameList.get(i).getCurrentState()].getMessage() , 64, 90 + (i*20));
			}
		}
		
		super.render(screen);
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER && gameId.length() > 0) {
			joinGameButton.postClick();
		}
	}

	public void keyReleased(KeyEvent arg0) {
	}

	public void keyTyped(KeyEvent e) {

		if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE && gameId.length() > 0) {
			gameId = gameId.substring(0, gameId.length() - 1);
		} else if (Font.letters.indexOf(Character.toUpperCase(e.getKeyChar())) >= 0) {
			gameId += e.getKeyChar();
		}
	}

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}

