package message.game;

import java.io.Serializable;
import java.util.Arrays;

import message.Constant;
import message.MessagePlayer;

public class ChooseHomePlanetReq extends MessagePlayer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5567186481468760761L;
	
	/* Timeout in seconds */
	private int timeOut;
	/* Home planets which can be chosen by this player */
	private int availableHomePlanetLocations[];

	public ChooseHomePlanetReq(int clientId, int gameId, int playerId, int timeOut, int availableHomePlanetLocations[]) {
		super(Constant.REQ_CHOOSE_HOME_PLANET.getId(), clientId, gameId, playerId);
		this.timeOut = timeOut;
		this.availableHomePlanetLocations = availableHomePlanetLocations;
	}

	public int getTimeOut() {
		return timeOut;
	}

	public int[] getAvailableHomePlanetLocations() {
		return availableHomePlanetLocations;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "ChooseHomePlanetReq [timeOut=" + timeOut
				+ ", availableHomePlanetLocations="
				+ Arrays.toString(availableHomePlanetLocations) + "]";
	}
}
