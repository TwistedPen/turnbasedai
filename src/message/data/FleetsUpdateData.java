package message.data;

import java.io.Serializable;
import java.util.Arrays;

public class FleetsUpdateData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2186268496022998067L;
	
	private int playerId;
	private int[] removeFleetLocation;
	private int[] removeFleetSize;
	private int[] addFleetLocation;
	private int[] addFleetSize;
	
	public FleetsUpdateData(int playerId, int[] removeFleetLocation,
			int[] removeFleetSize, int[] addFleetLocation, int[] addFleetSize) {
		this.playerId = playerId;
		this.removeFleetLocation = removeFleetLocation;
		this.removeFleetSize = removeFleetSize;
		this.addFleetLocation = addFleetLocation;
		this.addFleetSize = addFleetSize;
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public int[] getRemoveFleetLocation() {
		return removeFleetLocation;
	}

	public void setRemoveFleetLocation(int[] removeFleetLocation) {
		this.removeFleetLocation = removeFleetLocation;
	}

	public int[] getRemoveFleetSize() {
		return removeFleetSize;
	}

	public void setRemoveFleetSize(int[] removeFleetSize) {
		this.removeFleetSize = removeFleetSize;
	}

	public int[] getAddFleetLocation() {
		return addFleetLocation;
	}

	public void setAddFleetLocation(int[] addFleetLocation) {
		this.addFleetLocation = addFleetLocation;
	}

	public int[] getAddFleetSize() {
		return addFleetSize;
	}

	public void setAddFleetSize(int[] addFleetSize) {
		this.addFleetSize = addFleetSize;
	}

	@Override
	public String toString() {
		return "FleetsUpdateData [playerId=" + playerId
				+ ", " + "\n" + "removeFleetLocation="
				+ Arrays.toString(removeFleetLocation) + ", " + "\n" + "removeFleetSize="
				+ Arrays.toString(removeFleetSize) + ", " + "\n" + "addFleetLocation="
				+ Arrays.toString(addFleetLocation) + "," + "\n" + "addFleetSize="
				+ Arrays.toString(addFleetSize) + "]";
	}
}
