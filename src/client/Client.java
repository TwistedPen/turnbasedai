package client;

import gui.Gui;
import gui.screens.ChooseGameScreen;
import gui.screens.ChooseMapScreen;
import gui.screens.ErrorScreen;
import gui.screens.NumPlayersScreen;
import gui.screens.ScoreScreen;
import gui.screens.WaitPlayerScreen;
import gui.sprites.Screen;

import message.*;
import message.application.*;
import message.data.*;
import message.game.*;

import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import client.actors.AI;
import client.actors.Actor;

import cell.Cell;
import cell.PlanetCell;
import cell.WormHoleCell;

public class Client implements Runnable {

    public static final boolean DEBUG = false;
    
	private int gameID = 0;
	boolean running = true;
	private boolean inGame = false;
	private Actor player;
	private AI AI;
	public ArrayList<MoveCommand> playerCommands = new ArrayList<MoveCommand>();
	public ArrayList<MoveCommand> aiCommands = new ArrayList<MoveCommand>();
	public ArrayList<ArrayList<Cell>> paths = new ArrayList<ArrayList<Cell>>();
	public ArrayList<ArrayList<Cell>> aiPaths = new ArrayList<ArrayList<Cell>>();
	public boolean sendData = false;
	public int turnNumber = 0;
	public int playerCount = 0;
	private boolean hasActiveAI;
	
	public static Connection server;
	private static Message fromServer;
	private static Gui gui;
	private static Map map;
	
	public Actor getActor() {
        return player;
    }

    public void setActor(Actor player) {
        this.player = player;
    }

    public AI getAI() {
        return AI;
    }

    public void setAI(AI aI) {
        AI = aI;
    }

    public boolean isHasActiveAI() {
        return hasActiveAI;
    }

    public void setHasActiveAI(boolean hasActiveAI) {
        this.hasActiveAI = hasActiveAI;
    }

    public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		Client.map = map;
	}

	public int getGameID() {
        return gameID;
    }

    public Gui getGui(){
		return Client.gui;
	}
    
    public void undoLastCommand(){
        if (!playerCommands.isEmpty() && map != null) {
            int lastCommand = playerCommands.size()-1;
            Cell cell = map.getCell(playerCommands.get(lastCommand).getCurrentLocationX(), playerCommands.get(lastCommand).getCurrentLocationY());
            cell.setShip(cell.getShip() + playerCommands.get(lastCommand).getShips());       
            
            playerCommands.remove(lastCommand);
        }
    }
    
    public void clearCommands(){
        
        while(!playerCommands.isEmpty()){
            undoLastCommand();
        }
        
    }

	/*
	 * Constructs a level to test on with a set height and width this will later
	 * be replaced so that we can construct a level based on the info received
	 * from the server.
	 */
	public void createMap(int height, int width) {
	    
		map = new Map(height, width,this);
		map.TestMap();
		
		//map.setListener(gui);
	}

	public void start() {
	    
		Thread thread = new Thread(this);
		thread.setPriority(Thread.MAX_PRIORITY);
		thread.start();

		Thread thread2 = new Thread(server);
		thread2.setPriority(Thread.NORM_PRIORITY);
		thread2.start();

	}

	private void update() {

		gui.update();

		if(server.isConnected())
			Protocol();

	}

    private void render(Graphics g) {
	    gui.render(g);
	}

	public void run() {

		// Variables for fps calculation
		long lastTimer1 = System.currentTimeMillis();
		int frames = 0;

		// variables for optim of when to render.
		long lastTime = System.nanoTime();
		double unprocessed = 0;
		int toUpdate = 0;
		long lastRenderTime = System.nanoTime();
		int min = 999999999;
		int max = 0;

		while (running) {

			// sets the time between each update
			double nsPerUpdate = 1000000000.0 / gui.framerate;
			boolean shouldRender = false;

			// counter for when to update
			while (unprocessed >= 1) {
				toUpdate++;
				unprocessed -= 1;
			}
			int updateCount = toUpdate;
			if (toUpdate > 0 && toUpdate < 3) {
				updateCount = 1;
			}
			if (toUpdate > 20) {
				toUpdate = 20;
			}

			// updates dependent on how large unprocessed is
			for (int i = 0; i < updateCount; i++) {

				toUpdate--;

				// part of a check to see how much time a update took
				 //long before = System.nanoTime();

				update();
				
				// part of a check to see how much time a update took
				 //long after = System.nanoTime();
				 //if(gui.activeTurn)
				 //System.out.println("Update time took " + (after - before) *
				 //100.0 / nsPerUpdate + "% of the max time");
				shouldRender = true;
			}

			// sets a buffer strategy, in this case we have 3 buffers to draw on
			BufferStrategy bs = gui.getBufferStrategy();
			
			if (bs == null) {
                gui.createBufferStrategy(3);
                continue;
            }

			if (shouldRender) {
			    
				// instantiates the graphics with a new buffer to draw on
				Graphics g = bs.getDrawGraphics();
				
				frames++;

				// Call render function in other classes
				render(g);
				g.dispose();
				// timepoint for render
				long renderTime = System.nanoTime();

				// calculates time passed from last render and sets max and min
				// render time.
				int timePassed = (int) (renderTime - lastRenderTime);
				if(gui.activeTurn)
				//System.out.println("render time took " + timePassed *
		        //         100.0 / nsPerUpdate + "% of the max time");
				if (timePassed < min) {
					min = timePassed;
				}
				if (timePassed > max) {
					max = timePassed;
				}
				lastRenderTime = renderTime;
				
			}

			try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
			
			// flips the buffer
            if (bs != null) {
                bs.show();
                Toolkit.getDefaultToolkit().sync();
            }
            
			// calculates the amount of time the update must be called
			long now = System.nanoTime();
			unprocessed += (now - lastTime) / nsPerUpdate;
			lastTime = now;

			// set the fps in gui
			if (System.currentTimeMillis() - lastTimer1 > 1000) {
				lastTimer1 += 1000;
				gui.setFps(frames);
				frames = 0;
			}
			
		}
	}


	public void setServerName(String input) {
		server.setServerName(input);

	}

	public void setPortNumber(String input) {
		server.setPortNumber(Integer.parseInt(input));

	}

	public void connect() {
		server.establishConnection();
	}
	
	public void abortConnect(){
	    server.abortConnection();
	}

	public void disconnect() {
		server.disconnect();
	}

	public void shutdown() {
		server.shutdown();
	}

	private void Protocol() {

		if (server.checkUpdate()) {
			fromServer = server.getUpdate();

			if (inGame) {

				switch (Constant.values()[fromServer.getMessageId()]) {

				case IND_GAME_STATE: {
				    
				    if(hasActiveAI)
				        AI.addMessage(fromServer);
				    
				    if(!gui.menuStack.isEmpty())
				        gui.clearMenus();
				    
					GameStateInd ind = (GameStateInd) fromServer;
					GameData data = ind.getGameData();
					turnNumber = ind.getTurnNumber();
					clearCommands();
					
					gui.setGrid(new Screen(data.getMapWidth()*Cell.WIDTH+Cell.WIDTH/2 ,
                            (data.getMapHeight()*Cell.HEIGHT*6/4 + ((data.getMapHeight()&1) == 0 ? Cell.HEIGHT/4 : Cell.HEIGHT ))));
					
					if(ind.getTurnNumber() == 1)
					    gui.initGame();
					
					playerCount = data.getPlayerCount();
					map = new Map(data.getMapWidth(),data.getMapHeight(),this);
					
					for (WormHoleData worm : data.getWormHoles()){
						map.setWormhole(worm.getLocation(),worm.getDestination(),false);
					}

					for (int debris : data.getDebrisLocations()){
						map.setDebris(debris,false);
					}
					
					    
					for (PlanetData planet : data.getPlanets()){
						map.setPlanet(planet.getColonizedBy(), 	// Planet Owner
								planet.getLocation(),			
								planet.getExpendFleetSize(),	// Cost
								planet.getRequiredFleetSize(),	// Requirement
								planet.getProductionFleetSize(),// Production
								planet.getProductionRate(),
								planet.getVisionRange(),false);
					}
					map.setHomePlanets(data.getHomePlanetLocations());
					
					for (FleetsData fleet : data.getFleets()) {
						int playerID = fleet.getPlayerId();
						int[] size = fleet.getFleetSizes();
						int[] location = fleet.getLocations();
						if(size != null){
						    for (int i = 0; i < size.length; i++){
						        map.setShip(playerID, location[i], size[i]);
						    }
						}
					}
					
					
					for (int vision : data.getEmptyLocations()) {
						map.setCellVisible(vision,true);
						map.setCellListener(vision, gui);
						Cell c = map.getCell((vision % map.getWidth()) , (vision / map.getWidth()));
						if(c.getID() == 2)
						    map.setCellListener(((WormHoleCell)c).getConnectedTo().getX() + ((WormHoleCell)c).getConnectedTo().getY()*map.getWidth(), getGui());
					}
					
					gui.activeTurn = true;
					gui.choosePlanet = false;
					break;

				} case IND_TIMEOUT: {
					
					gui.indTimeout();
					 
					break;
					
				} case REQ_CHOOSE_HOME_PLANET: {

				    if(hasActiveAI)
                        AI.addMessage(fromServer);
				    
					ChooseHomePlanetReq req = (ChooseHomePlanetReq) fromServer;
					
					int[] homePlanets = map.getHomePlanets();
					map.setHomePlanets(req.getAvailableHomePlanetLocations());
					gui.choosePlanet = true;
					
	                 //clears the menu so that the game is shown and remove buttons that sends messages to the server
					gui.clearMenus();
					gui.removeMessageButtons();
                    
					/* The for loop part is in order to overwrite the recently changed planets owner, the owner ID is irrelevant for this specific purpose, just needs to be unique */
					for (int i = 0; i < req.getAvailableHomePlanetLocations().length; i++) {
					    if (homePlanets[i] != req.getAvailableHomePlanetLocations()[i]) {
					        PlanetCell cell = (PlanetCell) map.getCell((homePlanets[i] % map.getWidth()) , (homePlanets[i] / map.getWidth()));
					        cell.setClaimedBy(homePlanets.length);
					        map.setCell((homePlanets[i] % map.getWidth()) , (homePlanets[i] / map.getWidth()), cell, true);
					        break;
					    } else if (req.getAvailableHomePlanetLocations().length != homePlanets.length && i == req.getAvailableHomePlanetLocations().length-1) {
					        PlanetCell cell = (PlanetCell) map.getCell((homePlanets[i+1] % map.getWidth()) , (homePlanets[i+1] / map.getWidth()));
                            cell.setClaimedBy(homePlanets.length);
                            map.setCell((homePlanets[i+1] % map.getWidth()) , (homePlanets[i+1] / map.getWidth()), cell, true);
					    }
					}
					
					if (req.getTimeOut() == Constant.IND_TIMEOUT.getId())
						gui.activeTurn = false;
					else{
						gui.activeTurn = true;
					    
					    //if this client is to choose a planet then add buttons
					    if(gui.choosePlanet && !hasActiveAI){
					        gui.initChoosePlanet();
					    }
					}
					
					break;
					
				} case RESP_GAME_LIST: {

                    GameListResp resp = (GameListResp) fromServer;
                    
                    if (gui.menuStack.peek() instanceof gui.screens.WaitPlayerScreen)
                        for (GameListData game : resp.getGames())
                            if (game.getGameId() == this.gameID) {
                                gui.menuStack.pop();
                                gui.addMenu(new WaitPlayerScreen(game.getMaxPlayerCount()-game.getCurrPlayerCount()));
                                
                            }

                    break;
					
				} case RESP_MOVE: {
				    
				    if(hasActiveAI)
                        AI.addMessage(fromServer);
				    
					MoveResp resp = (MoveResp) fromServer;
					if (resp.getResponse() != Constant.OK.getId()) {
						
						map.errorCells(resp.getResponses()); // Error messages will contain information telling what kind of error is is, and what action was wrong.
						gui.errorMove = true;
						gui.activeTurn = true;
						gui.startTimer();
						
					}
					break;
					
				} case REQ_CHOOSE_MAP: {

				    if(hasActiveAI){
                        AI.addMessage(fromServer);
				    }
				    
					ChooseMapReq req = (ChooseMapReq) fromServer;

					if (req.getTimeOut() == Constant.IND_TIMEOUT.getId()) {
					    
					    gui.menuStack.add(new WaitPlayerScreen());
					    
					    
					} else {
					    
					    /*
					     * This request can only be received if this client created the game so we go from NumPlayerScreen -> WaitPlayerScreen -> ChooseMapScreen
					     * When the client receives this message then the gui goes over to ChooseMapScreen where the possible maps
					     * is shown and a map is chosen the same way as for game atm.
					     */
					    
					    if (gui.menuStack.peek() instanceof gui.screens.WaitPlayerScreen) {

					        ChooseMapScreen cms = new ChooseMapScreen();
					        
					        for(GameData mapL : req.getMaps())
                                cms.mapList.add(mapL);
					        
					        gui.addMenu(cms);
	                        
					    }
					    
					}

					break;
					
				} case IND_GAME_ENDED: {
				    
				    setMap(null);
				    
				    if(hasActiveAI)
                        AI.addMessage(fromServer);
					
					GameEndedInd ind = (GameEndedInd) fromServer;
					ScoreData[] data = ind.getScoresData();
					
					int[] playerArray = new int[data.length];
					int[] scoreArray = new int[data.length];
					for (int i = 0;i < data.length;i++) {
						playerArray[i] = data[i].getPlayerId();
						scoreArray[i] = data[i].getPlayerScore();
					}
					
					gui.addMenu(new ScoreScreen(playerArray,scoreArray,player.getPlayerID(),turnNumber));
					inGame = false;
					
					break;
					
				} case IND_WAIT: {
				    
				    if(hasActiveAI)
                        AI.addMessage(fromServer);
                    
                    if(gui.menuStack.peek() instanceof NumPlayersScreen)
                        gui.menuStack.add(new WaitPlayerScreen(1000));
                    else if(gui.menuStack.peek() instanceof ChooseGameScreen)
                        gui.menuStack.add(new WaitPlayerScreen(1000));
                    
                        
                                        
                    break;
					
				} default: {
				
					
					
				}}

				} else {

					switch (Constant.values()[fromServer.getMessageId()]) {

					case RESP_GAME_LIST: {

						GameListResp resp = (GameListResp) fromServer;

						ArrayList<GameListData> gameList = new ArrayList<GameListData>();
						
						for (GameListData gml : resp.getGames())
							gameList.add(gml);

						if (gui.menuStack.peek() instanceof gui.screens.ChooseGameScreen) {

							ChooseGameScreen cgm = (ChooseGameScreen) gui.menuStack.peek();
							cgm.gameList = gameList;
						}

						break;
						
					} case RESP_NEW_GAME: {

						NewGameResp resp = (NewGameResp) fromServer;
						if (resp.getResponse() == Constant.OK.getId()) {

							gameID = resp.getGameId();
							
							sendMessageToServer(new JoinGameReq(server.getClientID(), gameID, Constant.PLAYER_ACTIVE.getId()));
							

							//gui.popMenu(); // back to joinMenu
							//gui.addMenu(new ChooseMapScreen());

						} else if (resp.getResponse() == Constant.TOO_MANY_GAMES.getId()) {

							gui.addMenu(new ErrorScreen(Constant.TOO_MANY_GAMES));

						} else if (resp.getResponse() == Constant.REQUESTED_PLAYER_COUNT_ERROR.getId()) {

							gui.addMenu(new ErrorScreen(Constant.REQUESTED_PLAYER_COUNT_ERROR));
							
						}

						break;

					} case RESP_JOIN_GAME: {

						JoinGameResp resp = (JoinGameResp) fromServer;

						if (resp.getResponse() == Constant.OK.getId()) {
						    
							gameID = resp.getGameId();
							player.setPlayerID(resp.getPlayerId());
							inGame = true;

						} else if (resp.getResponse() == Constant.PLAYER_LIST_FULL.getId()) {

							gui.addMenu(new ErrorScreen(Constant.PLAYER_LIST_FULL));

						} else if (resp.getResponse() == Constant.GAME_HAS_STARTED.getId()) {
						    
							gui.addMenu(new ErrorScreen(Constant.GAME_HAS_STARTED));

						}

						break;

					} case RESP_JOIN_SERVER: {

						JoinServerResp resp = (JoinServerResp) fromServer;

						if (resp.getResponse() == Constant.OK.getId()) {

							gui.popMenu();
							gui.addMenu(new ChooseGameScreen());
							server.setClientID(resp.getClientId());
							
							server.sendUpdate(new GameListReq(server.getClientID()));

						} else if (resp.getResponse() == Constant.TOO_MANY_CLIENTS.getId()) {

							gui.addMenu(new ErrorScreen(Constant.TOO_MANY_CLIENTS));
							server.disconnect();

						}
						
						break;

					} default: {
						
						

					}}

				}
		
	

			}
		}

	public void startAI(){
	    hasActiveAI = true;
	    (new Thread(AI, "AI")).start();
	}
	
	public void stopAI(){
	    hasActiveAI = false;
	    Message posion = new GameEndedInd(server.getClientID(), getGameID(), null);
	    ((AI)player).addMessage(posion);
	}
	
	public synchronized void sendMessageToServer(Message msg){
	    
	    server.sendUpdate(msg);
	}
	
	public synchronized void recieveCommandsFromAI(ArrayList<MoveCommand> MC){
	    aiCommands = MC;
	    aiCommands.trimToSize();
	}
	
	public synchronized void updateCommands(){
	    //updates the command list recieved from the AI
	    if(aiCommands != null){
	        playerCommands = aiCommands;
	        aiCommands = null;
	    }

	}

	public synchronized void recievePathsFromAI(ArrayList<ArrayList<Cell>> pathsList){
	    
	    aiPaths = new ArrayList<ArrayList<Cell>>();
	    
	    for(int i = 0; i < pathsList.size(); i++){
	        
	        aiPaths.add( new ArrayList<Cell>());
	        
	        for(int j = 0; j < pathsList.get(i).size(); j++){
	            
	            if(pathsList.get(i).get(j) != null)
	                aiPaths.get(i).add(new Cell(0,0,pathsList.get(i).get(j).getXpos(),pathsList.get(i).get(j).getYpos()));
	                
	        }
	    }
	    //aiPaths = pathsList;
	    aiPaths.trimToSize();
	}

	public synchronized void updatePaths(){
	    
	    //updates the command list recieved from the AI
	    if(aiPaths != null){
	        
	        paths = new ArrayList<ArrayList<Cell>>();
	        for(int i = 0; i < aiPaths.size(); i++){
	            
	            paths.add( new ArrayList<Cell>());
	            
	            for(int j = 0; j < aiPaths.get(i).size(); j++){
	                if(aiPaths.get(i).get(j) != null)
	                paths.get(i).add(new Cell(0,0,aiPaths.get(i).get(j).getXpos(),aiPaths.get(i).get(j).getYpos()));
	            }
	        }
	        aiPaths = null;
	    }

	}
	
	
		public static void main(String[] args) {
			Client c = new Client();

			server = new Connection();
		    
			gui = new Gui(c);
			gui.init();
			
			c.start();

		}
		
}