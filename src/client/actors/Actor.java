package client.actors;

import gui.Gui;
import gui.sprites.Screen;
import cell.Cell;
import cell.PlanetCell;
import client.Keys;

public abstract class Actor {
    
    public Cell selectedCell = null;
    private int playerID;
    private int tProductionR = 0;
    private int ships = 0;
    private int nPlanets = 0;
    private Gui gui;
    public Keys keys; 
    public PlanetCell[] planets;
    
    // position of where the player is looking in the gui.
    public int wx = 0;
    public int wy = 0;

    public Actor() {
        
    }
    
    public int gettProductionR() {
        return tProductionR;
    }

    public void settProductionR(int tProductionR) {
        this.tProductionR = tProductionR;
    }

    public int getShips() {
        return ships;
    }

    public void setShips(int ships) {
        this.ships = ships;
    }

    public int getnPlanets() {
        return nPlanets;
    }

    public void setnPlanets(int nPlanets) {
        this.nPlanets = nPlanets;
    }

    public Gui getGui() {
        return gui;
    }

    public void setGui(Gui gui) {
        this.gui = gui;
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int id){
        this.playerID = id;
    }
    
    public abstract int getID();
    
    public void addPlanet(PlanetCell p) {
        planets[nPlanets] = p;
        nPlanets++;
    }
    
    public void setKeys(Keys keys){
        this.keys = keys;
    }

    public void update() {
        
        keys.update();
        
        if (keys.up.isDown) {
            if(keys.shift.isDown)
                wy = wy-4;
            else
                wy = wy-2;
        }
        if (keys.down.isDown) {
            if(keys.shift.isDown)
                wy = wy+4;
            else
                wy = wy +2;
        }
        if (keys.left.isDown) {
            if(keys.shift.isDown)
                wx = wx-4;
            else
                wx = wx-2;
        }
        if (keys.right.isDown) {
            if(keys.shift.isDown)
                wx = wx+4;
            else
                wx = wx+2;
        }

    }
    
    public void setSelectedCell(Cell c) {
        selectedCell = c;
    }

    public void render() {
        
    }

    public void render(Screen grid) {
        
    }
}
