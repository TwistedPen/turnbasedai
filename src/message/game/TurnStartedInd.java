package message.game;

import message.Constant;
import message.MessagePlayer;

public class TurnStartedInd extends MessagePlayer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2488871731917965759L;
	
	private int turnNumber;

	public TurnStartedInd(int clientId, int gameId, int playerId, int turnNumber) {
		super(Constant.IND_TURN_STARTED.getId(), clientId, gameId, playerId);
		this.turnNumber = turnNumber;
	}

	public int getTurnNumber() {
		return turnNumber;
	}

	
	@Override
	public String toString() {
		return super.toString() + "\n" + "TurnStartedInd [turnNumber="
				+ turnNumber + "]";
	}
}
