package client;

import client.actors.Player;
import gui.Gui;
import cell.Cell;
import cell.CellListener;
import cell.DebrisCell;
import cell.PlanetCell;
import cell.WormHoleCell;

public class Map {

	private final int width, height;
	
	private int[] homePlanets;
    private Cell[] cellTiles;
    private Cell[] homePlanetCells;
	public Cell[] errorCells;
    
    private Client client;
    
    public Map(int width, int height, Client c) {
    	
    	this.client = c;
    	this.width = width;
    	this.height = height;
    	
    	cellTiles = new Cell[width * height];
    	for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
  
            		Cell cell = new Cell();
            		setCell(x, y, cell, false);
            }
        }
    
    }
    public Map(int width, int height) {
        
        this.width = width;
        this.height = height;
        
        cellTiles = new Cell[width * height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
  
                    Cell cell = new Cell();
                    setCell(x, y, cell, false);
            }
        }
    
    }
    
    public Cell[] getCellTiles() {
		return cellTiles;
	}

	public Cell[] getHomePlanetCells() {
		return homePlanetCells;
	}
    
    public Client getClient(){
    	return this.client;
    }
    
    public void setCell(int x, int y, Cell cell , boolean visible){
    	final int index = x + y * width;
        cellTiles[index] = cell;
        
        //Set Position for cell
        int xpos = x*Cell.WIDTH;
		int ypos = y*(Cell.HEIGHT-Cell.HEIGHT/4);
		
		//offset for every second line 
		if(y % 2 == 1)
			xpos = x*(Cell.WIDTH)+Cell.WIDTH/2;
		
        cell.init(this, xpos, ypos, x, y, visible);
    }
	
    public Cell getCell(int x, int y) {
        if (x < 0 || y < 0 || x >= width || y >= height) return null;
        return cellTiles[x + y * width];
    }
    
    public Cell getCell(int location){
        
        return getCell(location % width,location / width);
    }
    
    public int getHeight(){
    	return this.height;
    }
    
    public int getWidth(){
    	return this.width;
    }

    public void errorCells(int[] responses) {
		
    	errorCells = new Cell[responses.length];
    	for (int i = 0;i < responses.length;i++){
    	    System.out.println((responses[i] % width) + " : " + (responses[i] / width));
    	    if (responses[i] > -1)
    	        errorCells[i] = new Cell((responses[i] % width) , (responses[i] / width));
    	    else
    	        errorCells[i] = null;
    	}
    	
	}
    
    public int[] getHomePlanets() {
        return homePlanets;
    }
    
    
    /**
     * May only be called after planets are set 
     */
    public void setHomePlanets(int[] location) {
    	
        homePlanets = location;
    	homePlanetCells = new Cell[location.length];
    	for (int i = 0;i < location.length;i++){
    		homePlanetCells[i] = getCell((location[i] % width) , (location[i] / width));
    		if(homePlanetCells[i] instanceof PlanetCell)
    		    ((PlanetCell)homePlanetCells[i]).setHomePlanet(true);
    	}
    	
    	
    }
    
	public void setWormhole(int location, int destination, boolean visible) {
		
		WormHoleCell whc = new WormHoleCell();
		setCell( (location % width) , (location / width) , whc, visible);
		whc.setConnection( getCell((destination % width) , (destination / width)) );
		
	}

	public void setDebris(int debris, boolean visible) {
		
		setCell( (debris % width) , (debris / width) , new DebrisCell(), visible);
		
	}
	
	public void setCellVisible(int location, boolean visible){
		
		Cell c =  getCell((location % width) , (location / width));
		c.setVisible(visible);
		
	}
	
	public void clearVisible(Gui g){
	    for(int i = 0; i < this.width; i++){
	        for(int j = 0; j < this.height; j++){
	            Cell c = getCell(i,j);
	            c.setVisible(false);
	            c.removeListener(g);
	        }
	    }
	}
	
	public void setCellListener(int location, Gui g){
		
		Cell c =  getCell((location % width) , (location / width));
		c.addListener(g);
	}

	public void setShip(int playerID, int location, int size) {
		
		Cell c =  getCell((location % width) , (location / width));
		c.setPlayerID(playerID);
		c.setShip(size);
		c.setVisible(true);
		
	}

	public void setPlanet(int colonizedBy, int location, int expendFleetSize,
			int requiredFleetSize, int productionFleetSize, int productionRate,
			int visionRange, boolean visible) {
		
		PlanetCell planet = new PlanetCell(requiredFleetSize, expendFleetSize, productionFleetSize, productionRate, visionRange, false);
		planet.setClaimedBy(colonizedBy);
		if(colonizedBy > 0){
		    planet.setPlayerID(colonizedBy);
		}
		setCell((location % width) , (location / width) , planet, visible);
		
	}

	public void TestMap() {
		
	    Gui g = getClient().getGui();
	    
	    g.setActor(new Player(getClient().getGui()));
	    g.initGame();
	    
		PlanetCell planet = new PlanetCell(1, 1, 1, 1, 2,false);
		setCell(1,1,planet,true);
		setCellListener(1+this.width*1, this.client.getGui());
		
		WormHoleCell whc = new WormHoleCell();
		setCell(4,4,whc,true);
		whc.setConnection(getCell(2,1));
		setCellListener(4+this.width*4, this.client.getGui());
		
		DebrisCell dc1 = new DebrisCell();
		setCell(5,6,dc1,false);
		setCellListener(5+this.width*6, this.client.getGui());
		
		DebrisCell dc2 = new DebrisCell();
		setCell(6,7,dc2,false);
		setCellListener(6+this.width*7, this.client.getGui());
		
    	this.setShip(01,3+this.width*3, 2);
    	setCellListener(3+this.width*3, this.client.getGui());
    	
    	this.setShip(01, 3+this.width*2, 2);
    	setCellListener(3+this.width*2, this.client.getGui());
		
	}
	
	public void setListener(CellListener gui){
		
		for(Cell cell : cellTiles){
			cell.addListener(gui);
		}
		
	}

}

