package message.application;

import java.io.Serializable;

import message.Constant;
import message.Message;

public class JoinServerReq extends Message implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6123545861091367127L;

	public JoinServerReq() {
		super(Constant.REQ_JOIN_SERVER.getId());
	}
}
