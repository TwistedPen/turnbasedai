package gui;

import client.*;
import client.actors.AI;
import client.actors.Actor;
import client.actors.Player;
import client.actors.Spectator;

import gui.screens.*;
import gui.sprites.Art;
import gui.sprites.Screen;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JPanel;

import message.Constant;
import message.application.GameListReq;
import message.application.JoinGameReq;
import message.application.NewGameReq;
import message.data.MoveData;
import message.game.ChooseHomePlanetResp;
import message.game.ChooseMapResp;
import message.game.MoveReq;


import cell.Cell;
import cell.CellListener;
import cell.PlanetCell;
import cell.WormHoleCell;

public class Gui extends Canvas implements MouseListener, ButtonListener, CellListener,
		KeyListener, WindowListener {

	private static final long serialVersionUID = 1L;

	public static final int GAME_WIDTH = 512*2;
	public static final int GAME_HEIGHT = GAME_WIDTH * 10 / 16;
	public static final int SCALE = 1;
	private Client c;
	private Actor p;
	private int xScroll = 0;
	private int yScroll = 0;
	
	public List<Button> gameButtons = new ArrayList<Button>();
	public Stack<GuiMenu> menuStack = new Stack<GuiMenu>();

	private int shipsToMove = 0;
	private int selectedCellShip;
	private int fps;
	public double framerate = 60;
	int timeoutShowedTime = 0;
	private long errorTime;

	public static MouseButtons mouseButtons = new MouseButtons();
	public Screen interfaceScreen = new Screen(GAME_WIDTH, GAME_HEIGHT);
	private Screen gridMap;
	private Screen background = new Screen(GAME_WIDTH, GAME_HEIGHT);;
	public Keys keys = new Keys();
	private Button endTurnButton;
	private Button choosePlanetButton;
	private Button clearCommands, undoCommand, incB, decB;
	
	private boolean activeCell = true; // Boolean to prevent from the cell being active to soon
	private boolean toolTipShow = false;
	public boolean activeTurn = false;
	public boolean choosePlanet = false; // for when to choose homeplanet
	public boolean errorMove = false; // if there is an error move

    private Cell toolTipCell;

	public Gui(Client c) {
		this.c = c;
		this.setPreferredSize(new Dimension(GAME_WIDTH *SCALE - 10, GAME_HEIGHT * SCALE - 10));
		this.setMinimumSize(new Dimension(GAME_WIDTH * SCALE / 2, GAME_HEIGHT * SCALE / 2));
		this.setMaximumSize(new Dimension(GAME_WIDTH * SCALE * 2, GAME_HEIGHT * SCALE * 2));
		this.addMouseListener(this);
		this.addKeyListener(new InputHandler(keys));

		// add StartMenu
		StartMenuScreen menu = new StartMenuScreen();
		addMenu(menu);
		addKeyListener(this);
		
	}
	
	public Screen getGrid() {
        return gridMap;
    }
	
	public boolean hasActiveGame(){
	    return menuStack.isEmpty();
	}

    public void setGrid(Screen grid) {
        this.gridMap = grid;
    }

	public void setFps(int fps) {
		this.fps = fps;
	}

	public void setActor(Actor actor) {
		this.p = actor;
		this.p.setKeys(keys);
	}

	public void init() {
	    
	    //Initializing of the screen where the window is made and some button for the game screen.
		JFrame frame = new JFrame();
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(this);
		frame.setContentPane(panel);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.addWindowListener(this);
		this.setFont(this.getFont().deriveFont(Font.BOLD, 10));
		
	}

	
	public void update() {

		mouseButtons.setPosition(getMousePosition());
		
		if(p != null){
			p.update();
		}
		
		if (hasActiveGame()) {
            xScroll = (int) (p.wx - interfaceScreen.w / 4);
            yScroll = (int) (p.wy - (interfaceScreen.h - 24) / 8);
		    
		      // Updates for buttons in game
			if(activeTurn){
			    if(choosePlanet & choosePlanetButton != null){
	                choosePlanetButton.update(mouseButtons);
			    } else{

			        for (Button button : gameButtons) {
			            button.update(mouseButtons);
			        }
			    }
			}
           
			// Updates for the cell listener
			if(activeCell)
			    for(Cell cell :c.getMap().getCellTiles()){
			        if(cell.getXpos()-xScroll+Cell.WIDTH > 0 && cell.getXpos()-xScroll < GAME_WIDTH && cell.getYpos()-yScroll+Cell.HEIGHT > 0 && cell.getYpos()-yScroll < GAME_WIDTH)
			            cell.update(mouseButtons,p,-xScroll,-yScroll,gridMap.w,gridMap.h);
                }
			
			if((System.currentTimeMillis() - errorTime) > 3000){
	            errorMove = false;
	        }
		}

		// Updates for buttons in screens (not ingame)
		if (!hasActiveGame()) {
		    menuStack.peek().update(mouseButtons);

		}
		
		activeCell = true;
		
		mouseButtons.update();

	}

	public void render(Graphics g) {
	    
	    //clears the screens so that there aren't any left from last render
	    interfaceScreen.clear(0);
	    if(gridMap != null)
	        gridMap.clear(0);
	    
		//if there is a menu in the stack then render it
		if (!hasActiveGame()) {
			menuStack.peek().render(interfaceScreen);
		}
		
		// Sets a dark gray background if outside game else draw the background with the stars	
		if(hasActiveGame()){
		    
		    g.setColor(Color.BLACK);
		    g.fillRect(0, 0, getWidth(), getHeight());
		    //moves the background based on the position of the player screen
		    background.blit(Art.bg_game, 10 - p.wx/20, 10 -  p.wy/20);
		    
		} else{
		    
		    g.setColor(Color.DARK_GRAY);
            g.fillRect(0, 0, getWidth(), getHeight());
            
		}
		
		// if there aren't any menus then draw the game
		if (hasActiveGame()) {

			// render cells
			for(Cell cell :c.getMap().getCellTiles()){
			    if(cell.getXpos()-xScroll+Cell.WIDTH > 0 && cell.getXpos()-xScroll < GAME_WIDTH && cell.getYpos()-yScroll+Cell.HEIGHT > 0 && cell.getYpos()-yScroll < GAME_WIDTH)
			    cell.render(gridMap);
			    
			}
			
			//If player is to choose planet then render the planet with homeplanet indicators
			if(choosePlanet){
				for(Cell cell :c.getMap().getHomePlanetCells()){
					((PlanetCell)cell).renderHomePlanets(gridMap);
				}
			}
			
			//draw error moves that are recieved from the server
			if(errorMove && c.getMap().errorCells != null)
			    for(Cell errorCell : c.getMap().errorCells)
			        if (errorCell != null)
			                errorCell.renderError(gridMap);

			//call the render for the player
			p.render(gridMap);

			//if it is time to chose planet then write a message if it is this players turn to pick or if he has to play
			if(choosePlanet){
			    String msg;
			    if(activeTurn){
			        msg = "Select a homeplanet and click 'Done' when finsihed";
			    } else
			        msg = "Wait for a player to choose a homeplnaet";

			    gui.Font.draw(interfaceScreen, msg, ((GAME_WIDTH/2) - (gui.Font.getStringWidth(msg)/2)) , 60);

			}
			
			//draw the MoveCommand in a new form, useful if visual wrapping is active. still uses the old form if it is a WormHole
			for (MoveCommand mc : c.playerCommands) {

			    Cell cu = c.getMap().getCell(mc.getCurrentLocationX(),
			            mc.getCurrentLocationY());
			    if(!(cu instanceof WormHoleCell) && !c.isHasActiveAI())
			        gridMap.blit(Art.cellTile[mc.getAction() - Constant.MOVE_UP_RIGHT.getId()][1], cu.getXpos(), cu.getYpos());

			}

			// Draws the background and the map
			if(c.getMap() != null){
			    g.drawImage(background.image, 0, 0, GAME_WIDTH * SCALE, GAME_HEIGHT
			            * SCALE, null);

			    //draw main game screen
			    g.drawImage(gridMap.image, -xScroll, -yScroll, GAME_WIDTH, GAME_HEIGHT,
			            0,0,GAME_WIDTH - (-xScroll),GAME_HEIGHT - (-yScroll), 
			            null);
			    
			    //polygonTest(g);
			}

			//draw the moves for the player (also the AI if it is active);
			drawMoves(g);


			// draws tool tip
			if (toolTipCell != null)
			    if (toolTipShow)
			        ToolTip(g, toolTipCell,
			                ((int)(toolTipCell.getXpos() + Cell.WIDTH * 3 / 4) - xScroll) * SCALE,
			                ((int)(toolTipCell.getYpos() + Cell.WIDTH * 3 / 4) - yScroll) * SCALE);

			//draw the bottom box in the gui containing the buttons and text
			bottomBox(g);

		}
		
		// Draws the main screen that shows the different screens outside the game and the buttons and text inside the game
		if (!hasActiveGame() || c.getMap() != null){
		    g.drawImage(interfaceScreen.image, 0, 0, GAME_WIDTH * SCALE, GAME_HEIGHT
		            * SCALE, null);
		}

		// Draws the fps
		String msg = "FPS: " + fps;
		g.setColor(Color.LIGHT_GRAY);
		g.drawString(msg, 11, 11);
	}
	
	public void drawMoves(Graphics g){
	    
            
	    //Draw the current path that the AI is on for each fleet to it's target

	    if(c.isHasActiveAI()){
	        c.updateCommands();
	        c.updatePaths();

	        g.setColor(Color.RED);


	        for(int i = 0; i < c.paths.size(); i++){
	            if(c.paths.size() > 0){
	                ArrayList<Cell> temp = c.paths.get(i);
	                int tx = 0,ty = 0, dx = 0,dy = 0;
	                Cell newCell = null;
	                for(int j = 1; j < temp.size(); j++){
	                    newCell = temp.get(j);
	                    if(newCell != null){
	                        Cell cu = temp.get(j-1);
	                        Cell d = newCell;
	                        tx = ((cu.getXpos() + Cell.WIDTH / 2)+10 - xScroll) * SCALE;
	                        ty = ((cu.getYpos() + Cell.WIDTH / 2)+10 - yScroll) * SCALE;
	                        dx = ((d.getXpos() + Cell.WIDTH / 2)+10 - xScroll) * SCALE;
	                        dy = ((d.getYpos() + Cell.WIDTH / 2)+10 - yScroll) * SCALE;

	                        if(ty < (getHeight()-100) && dy < (getHeight()-50)){
	                            g.drawLine(tx,ty,dx,dy);

	                        }

	                        cu = d;
	                    }

	                }
	                g.fillOval(dx-3, dy-3, 6, 6);
	            }
	        }
	    }
        
        //if there aren't no active AI then use the new visual command else us the old 
        
        g.setColor(Color.GREEN);
        for (MoveCommand mc : c.playerCommands) {

            Cell cu = c.getMap().getCell(mc.getCurrentLocationX(),
                    mc.getCurrentLocationY());
            if((cu instanceof WormHoleCell)){
                Cell d = c.getMap().getCell(mc.getDestinationX(), mc.getDestinationY());

                int tx = ((cu.getXpos() + Cell.WIDTH / 2) - xScroll) * SCALE;
                int ty = ((cu.getYpos() + Cell.WIDTH / 2) - yScroll) * SCALE;
                int dx = ((d.getXpos() + Cell.WIDTH / 2) - xScroll) * SCALE;
                int dy = ((d.getYpos() + Cell.WIDTH / 2) - yScroll) * SCALE;

                g.drawLine(tx,ty,dx,dy);
                g.fillOval(dx-3, dy-3, 6, 6);
            } else if(c.isHasActiveAI()){
                Cell d = c.getMap().getCell(mc.getDestinationX(), mc.getDestinationY());

                int tx = ((cu.getXpos() + Cell.WIDTH / 2) - xScroll) * SCALE;
                int ty = ((cu.getYpos() + Cell.WIDTH / 2) - yScroll) * SCALE;
                int dx = ((d.getXpos() + Cell.WIDTH / 2) - xScroll) * SCALE;
                int dy = ((d.getYpos() + Cell.WIDTH / 2) - yScroll) * SCALE;

                g.drawLine(tx,ty,dx,dy);
                g.fillOval(dx-3, dy-3, 6, 6);
            }
        }
	}

	public void bottomBox(Graphics g){
	    
	    interfaceScreen.fill(0, GAME_HEIGHT-50, GAME_WIDTH, 50, 0xff505050);
	    
		// render the buttons
		for(Button button : gameButtons) {
			button.render(interfaceScreen);
		}

		if(c.getActor().getID() == Constant.PLAYER_ACTIVE.getId()){
		    String turn = "";
		    if(!choosePlanet){
		        String sMove = "Move " + shipsToMove + " of "+ selectedCellShip; 
                gui.Font.draw(interfaceScreen,  sMove , (GAME_WIDTH/2) - gui.Font.getStringWidth(sMove)/2, GAME_HEIGHT-15);
		        if(activeTurn){
		            turn = "Your Turn";
		        }else
		            turn = "Wait for responds";
		        
		        gui.Font.draw(interfaceScreen,turn, GAME_WIDTH-128, GAME_HEIGHT-35);
		    }
		}
		
		gui.Font.draw(interfaceScreen, "Turn: " + c.turnNumber, GAME_WIDTH-128, 10);

	}

	// Creates a tool tip in the gui when a cell is selected
	public void ToolTip(Graphics g, Cell c, int x, int y) {
	    
	    int mouseX = mouseButtons.getX();
	    int mouseY = mouseButtons.getY();
	    int verticalDirection = 1 , horizontalDirection = 1, textSpace = 10, startY = 15, startX = 10;
	    
		if(x > 0 && y > 0 && x < getWidth()){
		    
			g.setColor(Color.LIGHT_GRAY);
			// ToolTip for Cells
			if (c.getID() == 00){
			    
			    int ttWidth = 110;
			    int ttHeight = 35;
			    
			    if(!(y < (getHeight()-ttHeight))){
			        verticalDirection = -1;
			        
			        if(mouseX > x && mouseX < (x+ttWidth) && mouseY < y && mouseY > (y-ttHeight)){
	                    horizontalDirection = -1;
	                    g.fillRoundRect(x-ttWidth, y-ttHeight, ttWidth, ttHeight, 10, 10);
	                } else
	                    g.fillRoundRect(x, y-ttHeight, ttWidth, ttHeight, 10, 10);
	                    
			    }
			    else{
			        
			        if(mouseX > x && mouseX < (x+ttWidth) && mouseY > y && mouseY < (y+ttHeight)){
                        horizontalDirection = -1;
                        g.fillRoundRect(x-ttWidth, y, ttWidth, ttHeight, 10, 10);
                    } else
			            g.fillRoundRect(x, y, ttWidth, ttHeight, 10, 10);
			    }
			    
			    
				g.setColor(Color.BLACK);
				
				startX = horizontalDirection > 0 ? 10 : 10-ttWidth;
				startY = verticalDirection > 0 ? 15 : 15-ttHeight;
				
				String cellP = "Controlled by: " + c.getPlayerID();
				g.drawString(cellP, x + startX, y + (startY+textSpace*0));

				String cellS = "Ships: " + c.getShip();
				g.drawString(cellS, x + startX, y + (startY+textSpace*1));
			}

			// ToolTip for PlanetCells
			else if (c.getID() == 01){
			    
			    int ttWidth = 110;
                int ttHeight = 85;

                if(!(y < (getHeight()-ttHeight))) {
                    verticalDirection = -1;
                    if(mouseX > x && mouseX < (x+ttWidth) && mouseY < y && mouseY > (y-ttHeight)){
                        horizontalDirection = -1;
                        g.fillRoundRect(x-ttWidth, y-ttHeight, ttWidth, ttHeight, 10, 10);
                    } else
                        g.fillRoundRect(x, y-ttHeight, ttWidth, ttHeight, 10, 10);
                }
                else{

                    if(mouseX > x && mouseX < (x+ttWidth) && mouseY > y && mouseY < (y+ttHeight)){
                        horizontalDirection = -1;
                        g.fillRoundRect(x-ttWidth, y, ttWidth, ttHeight, 10, 10);
                    } else
                        g.fillRoundRect(x, y, ttWidth, ttHeight, 10, 10);
                }

                startX = horizontalDirection > 0 ? 10 : 10-ttWidth;
                startY = verticalDirection > 0 ? 15 : 15-ttHeight;

                PlanetCell cp = (PlanetCell) c;
                g.setColor(Color.BLACK);

                String planetPL;
                if (cp.getClaimedBy() == 0)
					planetPL = "Neutral";
				else
					planetPL = "Claimed by: " + cp.getClaimedBy();
				g.drawString(planetPL, x + startX, y + (startY+textSpace*0));

				String planetR = "Requirement: " + cp.getRequirement();
				g.drawString(planetR, x + startX, y + (startY+textSpace*1));

				String planetC = "Cost: " + cp.getCost();
				g.drawString(planetC, x + startX, y + (startY+textSpace*2));

				String planetP = "Production: " + cp.getProduction();
				g.drawString(planetP, x + startX, y + (startY+textSpace*3));

				String planetPR = "ProductionRate: " + cp.getProductionRate();
				g.drawString(planetPR, x + startX, y + (startY+textSpace*4));

				String planetV = "Vision: " + cp.getVision();
				g.drawString(planetV, x + startX, y + (startY+textSpace*5));
				
				String ships = "Ships: " + cp.getShip();
                g.drawString(ships, x + startX, y + (startY+textSpace*6));

			}

			// ToolTip for WormholeCell
			else if (c.getID() == 2 ) {
			    
			    int ttWidth = 110;
			    int ttHeight = 35;

			    if(!(y < (getHeight()-ttHeight))) {
			        verticalDirection = -1;
			        if(mouseX > x && mouseX < (x+ttWidth) && mouseY < y && mouseY > (y-ttHeight)){
			            horizontalDirection = -1;
			            g.fillRoundRect(x-ttWidth, y-ttHeight, ttWidth, ttHeight, 10, 10);
			        } else
			            g.fillRoundRect(x, y-ttHeight, ttWidth, ttHeight, 10, 10);
			    }
			    else{

			        if(mouseX > x && mouseX < (x+ttWidth) && mouseY > y && mouseY < (y+ttHeight)){
			            horizontalDirection = -1;
			            g.fillRoundRect(x-ttWidth, y, ttWidth, ttHeight, 10, 10);
			        } else
			            g.fillRoundRect(x, y, ttWidth, ttHeight, 10, 10);
			    }

			    startX = horizontalDirection > 0 ? 10 : 10-ttWidth;
                startY = verticalDirection > 0 ? 15 : 15-ttHeight;
			    
				WormHoleCell whc = (WormHoleCell) c;


				g.setColor(Color.BLACK);

				String cellP = "Controlled by: " + whc.getPlayerID();
				g.drawString(cellP, x + startX, y + (startY+textSpace*0));

				String cellS = "Ships: " + whc.getShip();
				g.drawString(cellS, x + startX, y + (startY+textSpace*1));

			}

			// ToolTip for Debris
			else if (c.getID() == 3) {

                int ttWidth = 140;
                int ttHeight = 35;

                if(!(y < (getHeight()-ttHeight))) {
                    verticalDirection = -1;
                    if(mouseX > x && mouseX < (x+ttWidth) && mouseY < y && mouseY > (y-ttHeight)){
                        horizontalDirection = -1;
                        g.fillRoundRect(x-ttWidth, y-ttHeight, ttWidth, ttHeight, 10, 10);
                    } else
                        g.fillRoundRect(x, y-ttHeight, ttWidth, ttHeight, 10, 10);
                }
                else{

                    if(mouseX > x && mouseX < (x+ttWidth) && mouseY > y && mouseY < (y+ttHeight)){
                        horizontalDirection = -1;
                        g.fillRoundRect(x-ttWidth, y, ttWidth, ttHeight, 10, 10);
                    } else
                        g.fillRoundRect(x, y, ttWidth, ttHeight, 10, 10);
                }

                startX = horizontalDirection > 0 ? 10 : 10-ttWidth;
                startY = verticalDirection > 0 ? 15 : 15-ttHeight;
			    
				g.setColor(Color.BLACK);

				String cellD1 = "Don't go here!!!";
				g.drawString(cellD1, x + startX, y + (startY+textSpace*0));

				String cellD2 = "Ships will be destroyed!!";
				g.drawString(cellD2, x + startX, y + (startY+textSpace*1));
			}
		}

	}
	
	public void indTimeout() {
		
	}

	public void clearMenus() {
		menuStack.clear();
	}

	public void addMenu(GuiMenu menu) {
		menuStack.add(menu);
		menu.addButtonListener(this);
	}

	public void popMenu() {
		if (!menuStack.isEmpty()) {
			menuStack.pop();
		}
	}

	// A test to see if the poylgons are positioned correct
	@SuppressWarnings("unused")
	private void polygonTest(Graphics g) {

		g.setColor(Color.WHITE);

		for(Cell cell : c.getMap().getCellTiles())
				g.drawPolygon(cell.getPolygon());

		
	}

	public void initChoosePlanet() {
	    removeMessageButtons();
	    choosePlanetButton = addButton(new Button(ButtonID.CHOOSE_PLANET_ID, 7, (GAME_WIDTH/2 - 64), GAME_HEIGHT-24));
	    
    }
	
	public void initGame(){
	    removeMessageButtons();
	    if(c.getActor().getID() == Constant.PLAYER_ACTIVE.getId()){
	        endTurnButton = addButton(new Button(ButtonID.END_TURN_ID, 2, (GAME_WIDTH - 128), GAME_HEIGHT-24));
            clearCommands = addButton(new Button(ButtonID.CLEAR_COMMANDS_ID, 11, 0, GAME_HEIGHT-24));
            undoCommand = addButton(new Button(ButtonID.UNDO_COMMAND_ID, 10, 0, GAME_HEIGHT-24-24-2));
            incB = addButton(new SmallButton(ButtonID.INC_SHIP_MOVE_ID, 0,  (GAME_WIDTH/2+100-24), GAME_HEIGHT-24));
            decB = addButton(new SmallButton(ButtonID.DEC_SHIP_MOVE_ID, 1,  (GAME_WIDTH/2-100), GAME_HEIGHT-24));
	    }else
	        removeControlButtons();
	}
	
	private void removeControlButtons() {
	    if(incB != null)
	        removeButton(incB);
	    if(decB != null)
            removeButton(decB);
	    if(undoCommand != null)
            removeButton(undoCommand);
	    if(clearCommands != null)
            removeButton(clearCommands);
        
    }

    public void removeMessageButtons(){

	    if(endTurnButton != null)
	        removeButton(endTurnButton);
	    if(choosePlanetButton != null)
	        removeButton(choosePlanetButton);
            
	}
    
    @Override
    public void cellSelected(Cell cell) {
        
        //Method for cellListener where it control what the cell should do when selected
        
        if (p.selectedCell == null && !toolTipShow) {
            
            toolTipShow = true;
            toolTipCell = cell;
            if(cell.getPlayerID() == p.getPlayerID()&& cell.getShip() > 0){
                p.selectedCell = cell;
                if(p instanceof Player){
                    selectedCellShip = p.selectedCell.getShip();
                    shipsToMove = p.selectedCell.getShip();
                }
            }else if(choosePlanet && cell instanceof PlanetCell){
                if(((PlanetCell)cell).isHomePlanet())
                    p.selectedCell = cell;
                    
            }
                

        } else if(p.selectedCell == null && toolTipShow){
            
            toolTipShow = false;
            toolTipCell = null;
                    
        }else {
        

            // removes the selected cell if the second cell
            // is the same or to far away to be apart of a
            // move command
            if (cell.equals(p.selectedCell)){
        
                p.selectedCell = null;
                selectedCellShip = 0;
                shipsToMove = 0;
                toolTipShow = false;
                toolTipCell = null;
                
            } else if (activeTurn) {

                // If the second cell that the player clicks
                // on is possible as a move command then
                // a move command is stored
                if(p.selectedCell.getShip() > 0){
                    int action = Constant.MOVE_UP_RIGHT.getId(); // start in NW and goes clockwise
                    for(Cell nCell : p.selectedCell.getNeighbours()){
                        if (nCell.equals(cell)){
                            p.selectedCell.setShip(p.selectedCell.getShip() - shipsToMove);
                            c.playerCommands.add(new MoveCommand(
                                    p.selectedCell.getX(),
                                    p.selectedCell.getY(), shipsToMove,cell.getX(), cell.getY(),
                                    action));
                        }
                        action++;
                    }

                }
            }
            
            p.selectedCell = null;
            selectedCellShip = 0;
            shipsToMove = 0;
            toolTipShow = false;
            toolTipCell = null;
        } 
        
    }
	
	@Override
	public void buttonPressed(Button button) {
	    
	    if(Client.DEBUG)
	        System.out.println("pressed button: " + button.getId().name());
	    
	    if(!menuStack.isEmpty())
	        
	        switch (ButtonID.values()[button.getId().ordinal()]) {
	        
	        case START_GAME_ID:{
	            /*clearMenus();

	            interfaceScreen.clear(0);
	            c.setActor(new Player(this));
	            c.getActor().setPlayerID(1);
	            //c.createMap(20, 20);
	            //setGrid(new Screen(20*Cell.WIDTH+Cell.WIDTH/2 ,
                 //       (10*Cell.HEIGHT*6/4 + ((10&1) == 0 ? Cell.HEIGHT/4 : Cell.HEIGHT ))));
	            activeTurn = true;
	            activeCell = false;
	            break;*/
	        }
	        
	        case JOIN_SERVER_ID: {
	            
	            addMenu(new JoinMenuScreen());
	            break;
	        }
	        
	        case CHANGE_PORT_ID: {
	            
	            addMenu(new ChangePortScreen());
	            break;
	        }
	        
	        case PERFORM_CHANGE_PORT_ID: {
	            
	            c.setPortNumber(ChangePortScreen.portNum);
	            popMenu();
	            break;
	        }
	        
	        case PERFORM_CHOOSE_MAP_ID : {

	            c.sendMessageToServer(new ChooseMapResp(Client.server.getClientID(),c.getGameID(),p.getPlayerID(),Integer.parseInt(ChooseMapScreen.mapId)));
	            break;
	        }
	        
	        case PERFORM_JOIN_GAME_ID: {
	            
	            ChooseGameScreen cgs = null;
	            
	            if(menuStack.peek() instanceof ChooseGameScreen){

	                cgs = (ChooseGameScreen) menuStack.peek();
	                
	                if(!cgs.gameId.isEmpty()){

	                    // Dependent on what the user has chosen on the screen then the playeType is set to either Player, AI or Spectator
	                    int playerType;
	                    playerType = Constant.PLAYER_ACTIVE.getId();

	                    String chosenItem = cgs.DD.getChosenItem(); 
	                    if(chosenItem.equals("Player")){
	                        playerType = Constant.PLAYER_ACTIVE.getId();
	                        c.setActor(new Player(this));
	                        setActor(c.getActor());
	                    } else if (chosenItem.equals("AI")){
	                        playerType = Constant.PLAYER_INACTIVE.getId();
	                        c.setActor(new Spectator());
	                        c.setAI(new AI(c));
	                        c.startAI();
	                        setActor(c.getActor());
	                    } else{
	                        playerType = Constant.PLAYER_SPECTATOR.getId();
	                        c.setActor(new Spectator());
	                        setActor(c.getActor());
	                    }

	                    c.sendMessageToServer(new JoinGameReq(Client.server.getClientID(),Integer.parseInt(cgs.gameId),playerType));

	                } 
	            }
	            break;
	        }
	        
	        case CANCEL_ID: {
	            
	            if(menuStack.peek() instanceof ErrorScreen)
	                popMenu();
	            else if(menuStack.peek() instanceof ConnectingScreen)
	                Client.server.abortConnection();
	            popMenu();
	            break;
	        }
	        
	        case DISCONNECT_ID: {
	            
	            popMenu();
	            c.disconnect();
	            break;
	        }
	        
	        case PERFORM_JOIN_ID: {
	            
	            if(!Client.server.isConnected()){
	                c.setServerName(JoinMenuScreen.ip);
	                c.connect();
	            }

	            addMenu(new ConnectingScreen(this));
	            break;
	        }
	        
	        case NEW_GAME_ID: {

	            if(menuStack.peek() instanceof ChooseGameScreen){

	                ChooseGameScreen cgs = (ChooseGameScreen) menuStack.peek();

	             // Dependent on what the user has chosen on the screen then the playeType is set to either Player, AI or Spectator
                    
	                String chosenItem = cgs.DD.getChosenItem(); 
                    if(chosenItem.equals("Player")){
                        c.setActor(new Player(this));
                        setActor(c.getActor());
                    } else if (chosenItem.equals("AI")){
                        c.setActor(new Spectator());
                        c.setAI(new AI(c));
                        c.startAI();
                        setActor(c.getActor());
                    } else{
                        c.setActor(new Spectator());
                        setActor(c.getActor());
                    }
	            }

	            addMenu(new NumPlayersScreen());
	            break;
	        }
	        
	        case NUM_FINISH_ID: {
	            
	            NumPlayersScreen NPS = (NumPlayersScreen) menuStack.peek();
	            if(NumPlayersScreen.numPlayers.length() > 0)
	                c.sendMessageToServer(new NewGameReq(Client.server.getClientID(), NPS.getNumPlayers()));
	            break;
	        }
	        
	        case LEAVE_GAME_SCORE_ID: {
	            
	            clearMenus();
	            c.setHasActiveAI(false);
	            c.setAI(null);
	            c.paths = null;
	            c.setActor(null);
	            activeTurn = false;
	            gameButtons.clear();
	            c.sendMessageToServer(new GameListReq(Client.server.getClientID()));
	            c.setMap(null);
	            addMenu(new ChooseGameScreen());
	            break;
	        } 
	        
	        case UPDATE_GAME_LIST_ID: {
	            
	            c.sendMessageToServer(new GameListReq(Client.server.getClientID()));
	            break;
	        }
	        
	        case EXIT_GAME_ID: {
	            
	            c.shutdown();
	            System.exit(0);
	            break;
	        }
	        }
	    
	    else{
	        
	        switch (ButtonID.values()[button.getId().ordinal()]) {
	        
	        case CHOOSE_PLANET_ID: {
	            
	            // set active to false and send a chooseplanet resp.
	            if(p.selectedCell != null){
	                activeTurn = false;
	                toolTipShow = false;
	                c.sendMessageToServer(new ChooseHomePlanetResp(Client.server.getClientID(),c.getGameID(),p.getPlayerID(), p.selectedCell.getX() + p.selectedCell.getY()*c.getMap().getWidth()));
	                p.setSelectedCell(null);
	            }
	            break;
	        }
	        
	        case END_TURN_ID: {
	            
	            activeTurn = false;
	            MoveData[] MD = new MoveData[c.playerCommands.size()];
	            for(int i = 0; i < MD.length; i++){
	                MD[i] = new MoveData(c.playerCommands.get(i).getCurrentLocationX() + c.getMap().getWidth()*c.playerCommands.get(i).getCurrentLocationY(),
	                                        c.playerCommands.get(i).getShips(),
	                                        c.playerCommands.get(i).getDestinationX() + c.getMap().getWidth()*c.playerCommands.get(i).getDestinationY()); 
	            }
	            c.sendMessageToServer(new MoveReq(Client.server.getClientID(), c.getGameID(), p.getPlayerID(), c.turnNumber, MD));
	            break;
	        }
	        
	        case INC_SHIP_MOVE_ID: {
	            
	            if(selectedCellShip > shipsToMove)
	                shipsToMove++;
	           
	            break;
	        }
	        
	        case DEC_SHIP_MOVE_ID: {
	            
	            if(shipsToMove > 0)
	                shipsToMove--;
	            break;
	        }
	        
	        case UNDO_COMMAND_ID: {
	            
	            c.undoLastCommand();
	            break;
	        }
	        
	        case CLEAR_COMMANDS_ID: {
	            c.clearCommands();
	            break;
	            
	        }
            
            }
	    }

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

	}
	
	public Button addButton(Button button) {
        gameButtons.add(button);
        button.addListener(this);
        return button;
    }
	
	public boolean removeButton(Button button){
	    if(button.removeListener(this))
	       return gameButtons.remove(button);
	    return false;
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent e) {
		mouseButtons.releaseAll();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		mouseButtons.setNextState(e.getButton(), true);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		mouseButtons.setNextState(e.getButton(), false);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (!menuStack.isEmpty()) {
			menuStack.peek().keyPressed(e);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (!menuStack.isEmpty()) {
			menuStack.peek().keyReleased(e);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (!menuStack.isEmpty()) {
			menuStack.peek().keyTyped(e);
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {
		c.shutdown();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowOpened(WindowEvent e) {}

    public void startTimer() {
        errorTime = System.currentTimeMillis();
    }    

}
