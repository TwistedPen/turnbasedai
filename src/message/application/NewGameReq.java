package message.application;

import java.io.Serializable;

import message.Constant;
import message.MessageClient;

public class NewGameReq extends MessageClient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3028143452135544202L;
	
	/* Desired number of active players in game */
	private int playerCount;

	public NewGameReq(int clientId, int playerCount) {
		super(Constant.REQ_NEW_GAME.getId(), clientId);
		this.playerCount = playerCount;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "NewGameReq [playerCount=" + playerCount + "]";
	}
}
