package message.data;

import java.io.Serializable;

import message.Constant;

public class GameListData implements Serializable {
	 
    /**
     * 
     */
    private static final long serialVersionUID = -6702788346918548780L;
    
    private int gameId;
	private int currPlayerCount;
	private int maxPlayerCount;
	private int currSpectatorCount;
	private int maxSpectatorCount = 5;
	private int currentState;
	
	public GameListData(int gameId, int maxPlayerCount) {
		this.gameId = gameId;
		this.currPlayerCount = 0; 
		this.currSpectatorCount = 0;
		this.maxPlayerCount = maxPlayerCount;
		this.currentState = Constant.GAME_PLAYER_LIST_NOT_FULL.getId();
	}
	
    public GameListData(int gameId, int currPlayerCount, int currSpectatorCount, int maxPlayerCount, int currentState) {
        this.gameId = gameId;
        this.currPlayerCount = 0;
        this.currSpectatorCount = 0;
        this.maxPlayerCount = maxPlayerCount;
        this.currentState = Constant.GAME_PLAYER_LIST_NOT_FULL.getId();
    }

	public void setCurrSpectatorCount(int currSpecCount) {
	    currSpectatorCount = currSpecCount;
	}
	
	public int getCurrSpectatorCount() {
	    return currSpectatorCount;
	}
	
	public int getMaxSpectatorCount() {
	    return maxSpectatorCount;
	}
	
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	
	public int getGameId() {
		return gameId;
	}

	public void setCurrPlayerCount(int currPlayerCount) {
		this.currPlayerCount = currPlayerCount;
	}
	
	public int getCurrPlayerCount() {
		return currPlayerCount;
	}
	
	public void addPlayer() {
		this.currPlayerCount++;
	}
	
	 public void subPlayer() {
	     this.currPlayerCount--;
	}
	
	public void addSpectator() {
	    this.currSpectatorCount++;
	}
	
	public void subSpectator() {
	    this.currSpectatorCount--;
	}
	
	public int getMaxPlayerCount() {
		return maxPlayerCount;
	}

	public void setCurrentState(int currentState) {
		this.currentState = currentState;
	}
	
	public int getCurrentState() {
		return currentState;
	}

	@Override
	public String toString() {
		return "GameData [gameId=" + gameId + ", playerCount=" + currPlayerCount
				+ ", currentState=" + currentState + "]";
	}

}
