package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;


public class ConnectingScreen extends GuiMenu {
    
    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
	public static String text = "";
	
    public ConnectingScreen(Gui g) {
        super();
        
       
        addButton(new Button(ButtonID.CANCEL_ID, 4, (screenW-128)/2, screenH/4+120));
        
    }
    
    @Override
    public void render(Screen screen) {

        screen.clear(0);
        Font.draw(screen, "Connecting to server", (screenW-Font.getStringWidth("Connecting to server"))/2, screenH/4);
        
        super.render(screen);
    }

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}
