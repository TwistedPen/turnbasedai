package server.game;

import server.client.Client;
import message.Message;
import message.game.ChooseHomePlanetResp;
import message.game.ChooseMapResp;
import message.game.LeaveGameInd;
import message.game.MoveReq;

abstract class PlayerState {
    
    public void handleChooseMapResp(Game game, Player player, ChooseMapResp resp) {
       game.serviceChooseMapResp(player, resp);
    }
    
    public void handleChooseHomePlanetResp(Game game, Player player, ChooseHomePlanetResp resp) {
        game.serviceChooseHomePlanetResp(player, resp);
    }
    
    public void handleMoveReq(Game game, Player player, MoveReq req) {
        game.serviceMoveReq(player, req);
    }
    
    public void handleLeaveGameInd(Game game, Player player, LeaveGameInd ind) {
        game.serviceLeaveGameInd(player, ind);
    }
    
    public void sendMessage(Client client, Message message) {
        client.sendMessage(message);
    }
}


/* Only accept leave */
class Waiting extends PlayerState {
    
}

/* Only accept map choice and leave */
class MapChoice extends PlayerState {
    
}

/* Only accept home planet choice and leave */
class HomePlanetChoice extends PlayerState {
    
}

/* Only accept move and leave */
class InTurn extends PlayerState {
    
}

/* Do not send any messages to this player */
class Left extends PlayerState {
    
}