package message.game;

import java.io.Serializable;
import java.util.Arrays;

import message.Constant;
import message.MessagePlayer;

public class MoveResp extends MessagePlayer implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6913891504784240173L;
    
    /* OK if all moves approved - otherwise appropriate error code */
	private int response;
	private int[] responses;

	public MoveResp(int messageId,int clientId, int gameId, int playerId, int response, int[] responses) {
		super(Constant.RESP_MOVE.getId(), clientId, gameId, playerId);
		this.responses = responses;
	}

	public int getResponse() {
		return response;
	}
	
	public int[] getResponses() {
		return responses;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "MoveResp [response= " + response + " responses=" + Arrays.toString(responses) + "]";
	}
}
