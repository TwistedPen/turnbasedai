package gui.screens;

import gui.Button;
import gui.ButtonID;
import gui.DropDown;
import gui.Font;
import gui.Gui;
import gui.sprites.Screen;

import java.awt.event.KeyEvent;


public class NumPlayersScreen extends GuiMenu {

    private int screenW = Gui.GAME_WIDTH;
    private int screenH = Gui.GAME_HEIGHT;
	public static String numPlayers = "";
	private Button numFinishButton;
	
	public NumPlayersScreen() {
        super();
        
        numPlayers = "";
        numFinishButton = addButton(new Button(ButtonID.NUM_FINISH_ID, 6, (screenW - 128)/2, screenH / 4 + 120));
        addButton(new Button(ButtonID.CANCEL_ID, 4, (screenW - 128)/2, screenH / 4 + 160));
        
    }
	
	@Override
    public void render(Screen screen) {

        screen.clear(0);
        
        Font.draw(screen, "Number of players", (screenW - Font.getStringWidth("Number of players"))/2, screenH / 4);
        Font.draw(screen, numPlayers + "-", (screenW - Font.getStringWidth(numPlayers + "-"))/2, screenH / 4 + 20);
        
        super.render(screen);
    }
	
	public int getNumPlayers(){
		return Integer.parseInt(numPlayers);
	}
	
	
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER && numPlayers.length() > 0) {
			numFinishButton.postClick();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	public void keyTyped(KeyEvent e) {

		if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE && numPlayers.length() > 0) {
			numPlayers = numPlayers.substring(0, numPlayers.length() - 1);
		} else if (Font.letters.indexOf(Character.toUpperCase(e.getKeyChar())) >= 0) {
			char key = e.getKeyChar();
			if((key == KeyEvent.VK_0 || key == KeyEvent.VK_1 || key == KeyEvent.VK_2 || key == KeyEvent.VK_3 || 
	        	key == KeyEvent.VK_4 || key == KeyEvent.VK_5 || key == KeyEvent.VK_6 || key == KeyEvent.VK_7 || 
	        	key == KeyEvent.VK_8 || key == KeyEvent.VK_9) && numPlayers.length() < 2)
				numPlayers += e.getKeyChar();
		}
	}

    @Override
    public void drop(DropDown dropDown) {
        // TODO Auto-generated method stub
        
    }

}
