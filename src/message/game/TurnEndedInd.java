package message.game;


import java.io.Serializable;
import message.Constant;
import message.MessagePlayer;

public class TurnEndedInd extends MessagePlayer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 838822532764154615L;
	
	private int turnNumber;

	public TurnEndedInd(int clientId, int gameId, int playerId, int turnNumber) {
		super(Constant.IND_TURN_ENDED.getId(), clientId, gameId, playerId);
		this.turnNumber = turnNumber;
	}

	public int getTurnNumber() {
		return turnNumber;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "TurnEndedInd [turnNumber=" + turnNumber + "]";
	}
}
